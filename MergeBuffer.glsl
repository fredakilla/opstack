#include "Uniforms.glsl"
#include "Samplers.glsl"
#include "Transform.glsl"
#include "ScreenPos.glsl"

varying vec2 vScreenPos;

uniform bool cFlipUV;

#ifdef COMPILEPS
uniform vec2 cBlendRatio;
uniform vec4 cClearColor;
uniform float cPower;


vec4 getPixel(vec2 uv, sampler2D tex, vec4 vp)
{
    if (uv.x < vp.x || uv.x > vp.z || uv.y < vp.y || uv.y > vp.w)
        return cClearColor;

    vec2 uv2 = vec2(uv.x-vp.x, uv.y-vp.y)/vec2(vp.z-vp.x, vp.w-vp.y);
    return texture2D(tex, uv2);
}
#endif

void VS()
{
    mat4 modelMatrix = iModelMatrix;
    vec3 worldPos = GetWorldPos(modelMatrix);
    gl_Position = GetClipPos(worldPos);
    vScreenPos = GetScreenPosPreDiv(gl_Position);

	if(cFlipUV)
  		vScreenPos.y = 1 - vScreenPos.y;
}


float blendAdd(float base, float blend) {
	return min(base+blend,1.0);
}

vec3 blendAdd(vec3 base, vec3 blend) {
	return min(base+blend,vec3(1.0));
}

vec3 blendAdd(vec3 base, vec3 blend, float opacity) {
	return (blendAdd(base, blend) * opacity + blend * (1.0 - opacity));
}

void PS()
{    

    vec4 vpSrc0 = vec4(0,0,1,1);
  	vec4 vpSrc1 = vec4(0,0,1,1);

	vec2 uv = vScreenPos;
	//if(cFlipUV)
    	//	uv.y = 1 - vScreenPos.y;
	

    vec4 base = getPixel(uv, sDiffMap, vpSrc0);
    vec4 blend = getPixel(uv, sNormalMap, vpSrc1);
    float ratio0 = cBlendRatio.x;
    float ratio1 = cBlendRatio.y;
	
    vec4 result = vec4(0,0,0,1);



#ifdef MERGE_MODE_NONE
    result = ratio0*base*(1.0f-base.a)+ratio1*blend;
#endif	
#ifdef MERGE_MODE_ADD
    result = ratio0*base + ratio1*blend;
#endif
#ifdef MERGE_MODE_SUB
    result = ratio0*base - ratio1*blend;
#endif
#ifdef MERGE_MODE_MUL
    result = ratio0*base * ratio1*blend;
#endif
#ifdef MERGE_MODE_DARKEN
    result = min( ratio1*blend, ratio0*base );
#endif
#ifdef MERGE_MODE_LIGHTEN
    result = max( ratio1*blend, ratio0*base );
#endif
#ifdef MERGE_MODE_DIFFERENCE
    result = abs( ratio0*base - ratio1*blend );
#endif
#ifdef MERGE_MODE_NEGATION
    result = vec4(1.0) - abs( vec4(1.0) - ratio0*base - ratio1*blend );
#endif
#ifdef MERGE_MODE_EXCLUSION
    result = ratio0*base + ratio1*blend - (2.0*ratio0*base*ratio1*blend);
#endif
#ifdef MERGE_MODE_DODGE
    result = (ratio0*base) / (vec4(1.0) - (ratio1*blend));
#endif
#ifdef MERGE_MODE_BURN
    result = (ratio1*blend) / (vec4(1.0) - (ratio0*base));
#endif

    gl_FragColor = result * cPower;
    
}

