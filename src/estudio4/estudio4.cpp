/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   This file is part of
 *       ______        _                             __ __
 *      / ____/____   (_)____ _ ____ ___   ____ _   / // /
 *     / __/  / __ \ / // __ `// __ `__ \ / __ `/  / // /_
 *    / /___ / / / // // /_/ // / / / / // /_/ /  /__  __/
 *   /_____//_/ /_//_/ \__, //_/ /_/ /_/ \__,_/     /_/.   
 *                    /____/                              
 *
 *   Copyright � 2003-2012 Brain Control, all rights reserved.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <QtWidgets/QApplication>
#include <QtCore/QProcess>
#include <QtCore/QtPlugin>
#include <QtCore/QFile>
#include <QtCore/QDir>
#include <QStyleFactory>

// use this when linking Qt statically
//Q_IMPORT_PLUGIN(QWindowsIntegrationPlugin)

#include "gui/mainwnd.hpp"
#include "../configinfo.hpp"
#include "../eshared/eshared.hpp"
#include "system/memorytrack.hpp"

#include "urhoobjects/uWz4Meshcomponent.hpp"
#include "urhoobjects/uAnimatorNodePRS.hpp"
#include "urhoobjects/uCameraController.hpp"


static void initApplication(eBool disableStyles)
{
    QApplication::setEffectEnabled(Qt::UI_FadeMenu, false);
    QApplication::setEffectEnabled(Qt::UI_AnimateMenu, false);
    QApplication::setApplicationName("OPSTACK Studio");
    QApplication::setApplicationVersion(eENIGMA4_VERSION);
    QApplication::setOrganizationName("Brain Control");
    QApplication::setOrganizationDomain("http://www.braincontrol.org");

    if (disableStyles)
        return;

#ifdef eRELEASE
    QFile cssFile(":/style/estudio4.css");
#else
    QFile cssFile(":/style/estudio4_debug.css");
#endif

    qApp->setStyle(QStyleFactory::create("Fusion"));

    QPalette darkPalette;
    darkPalette.setColor(QPalette::Window, QColor(53,53,53));
    darkPalette.setColor(QPalette::WindowText, Qt::white);
    darkPalette.setColor(QPalette::Base, QColor(25,25,25));
    darkPalette.setColor(QPalette::AlternateBase, QColor(53,53,53));
    darkPalette.setColor(QPalette::ToolTipBase, Qt::white);
    darkPalette.setColor(QPalette::ToolTipText, Qt::white);
    darkPalette.setColor(QPalette::Text, Qt::white);
    darkPalette.setColor(QPalette::Button, QColor(53,53,53));
    darkPalette.setColor(QPalette::ButtonText, Qt::white);
    darkPalette.setColor(QPalette::BrightText, Qt::red);
    darkPalette.setColor(QPalette::Link, QColor(42, 130, 218));
    darkPalette.setColor(QPalette::Highlight, QColor(42, 130, 218));
    darkPalette.setColor(QPalette::HighlightedText, Qt::black);
    qApp->setPalette(darkPalette);

    if (cssFile.open(QFile::ReadOnly))
        qApp->setStyleSheet(QString(cssFile.readAll()));
}

eInt RunQt(eInt argc, eChar **argv)
{
    // sets the current directory to Enigma Studio's
    // executable file path as we look for the shaders
    // relatively to that directory, not the project's
    // directory as set by the IDE
    QApplication app(argc, argv);
    QDir::setCurrent(app.applicationDirPath());

    eBool disableStyles = eFALSE;
    QString projectFile = "";
    const QStringList &args = app.arguments();

    for (eInt i=1; i<args.size(); i++)
    {
        if (args[i] == "--nostyle")
            disableStyles = eTRUE;
        else if (args[i] == "--novsync")
            eDevice::forceNoVSync(eTRUE);
        else
            projectFile == args[i];
    }

    initApplication(disableStyles);

    eMainWnd mainWnd(projectFile);
    eDevice* device = new eDevice(eWF_VSYNC, eSize(300, 200), (ePtr)mainWnd.m_renderView->winId());
    app.setActiveWindow(&mainWnd);
    mainWnd.show();

    // enter Qt event loop
    int ret = app.exec();

    delete device;
    return ret;
}

eInt main(eInt argc, eChar **argv)
{
    eSimdSetArithmeticFlags(eSAF_RTN|eSAF_FTZ);
    ePROFILER_ADD_THIS_THREAD("Main thread");
    qputenv("QT_HASH_SEED","303"); // avoid salting and with that XML randomization

    // create urho3d context
    eGraphics::createContext();

    // run Qt window
    int result = RunQt(argc, argv);

    // free Urho3D context
    eGraphics::freeContext();

    // memory leak report
#ifdef eDEFINE_MEMORYTRACK
    TrackDumpBlocks();
#endif

    return result;
}
