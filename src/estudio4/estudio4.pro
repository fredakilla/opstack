#--------------------------------------------------------------------
# 3rdparty libraries path
#--------------------------------------------------------------------

URHO3D = /home/fred/Documents/Urho3D/BUILD

#--------------------------------------------------------------------
# target
#--------------------------------------------------------------------

TEMPLATE = app
CONFIG -= console
CONFIG += windows
QT += widgets xml
CONFIG += c++11
CONFIG -= debug_and_release
CONFIG -= debug_and_release_target

#--------------------------------------------------------------------
# output directory
#--------------------------------------------------------------------

CONFIG(debug,debug|release) {
    DESTDIR = $$PWD/../../binary
} else {
    DESTDIR = $$PWD/../../binary
}

QMAKE_CLEAN += $$DESTDIR/$$TARGET

#--------------------------------------------------------------------
# compilation flags
#--------------------------------------------------------------------

unix:!macx: QMAKE_CFLAGS_WARN_ON -= -Wall
unix:!macx: QMAKE_CXXFLAGS_WARN_ON -= -Wall
unix:!macx: QMAKE_CXXFLAGS += -Wall
unix:!macx: QMAKE_CXXFLAGS += -Wno-comment
unix:!macx: QMAKE_CXXFLAGS += -Wno-ignored-qualifiers
unix:!macx: QMAKE_CXXFLAGS += -Wno-unused-parameter
unix:!macx: QMAKE_CXXFLAGS += -std=c++11

unix:!macx: QMAKE_CXXFLAGS += -fpermissive


CONFIG(debug,debug|release) {
#message( debug )
} else {
#message( release )
unix:!macx: QMAKE_CXXFLAGS += -Wno-strict-aliasing

win32:QMAKE_CXXFLAGS_RELEASE -= -Zc:strictStrings

# remove possible other optimization flags
QMAKE_CXXFLAGS_RELEASE -= -O
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2
# add the desired -O3 if not present
QMAKE_CXXFLAGS_RELEASE *= -O3
}

#--------------------------------------------------------------------
# pre-processor definitions
#--------------------------------------------------------------------

CONFIG(debug,debug|release) {
#debug
DEFINES +=  \
        URHO3D_ANGELSCRIPT \
        #WIN32 \
        _DEBUG \
        #_WINDOWS \
        eDEBUG \
        eEDITOR \
        eUSE_PROFILER \
        eUSE_MMX \
        eUSE_SSE \
        eAUTO_SHADERS \
        QT_NO_KEYWORDS \
        eDEFINE_MEMORYTRACK
} else {
# release
DEFINES +=  \
        #WIN32 \
        NDEBUG \
        _WINDOWS \
        eRELEASE \
        eUSE_PROFILER \
        eEDITOR \
        eUSE_MMX \
        eUSE_SSE \
        QT_NO_KEYWORDS \
        eDEFINE_MEMORYTRACK
}

DEFINES -= UNICODE
DEFINES += _MBCS

#--------------------------------------------------------------------
# libraries includes
#--------------------------------------------------------------------

INCLUDEPATH += $$PWD/gui
INCLUDEPATH += $$PWD/../eshared
INCLUDEPATH += $$PWD/../wz4lib
INCLUDEPATH += $${URHO3D}/include
INCLUDEPATH += $${URHO3D}/include/Urho3D/ThirdParty

#--------------------------------------------------------------------
# check libraries dependencies
#--------------------------------------------------------------------

CONFIG(debug,debug|release) {
# debug
win32: PRE_TARGETDEPS += $${URHO3D}/lib/Urho3D_d.lib
unix:!macx: PRE_TARGETDEPS += $${URHO3D}/lib/libUrho3D.a
} else {
# release
win32: PRE_TARGETDEPS += $${URHO3D}/lib/Urho3D.lib
unix:!macx: PRE_TARGETDEPS += $${URHO3D}/lib/libUrho3D.a
}

win32: PRE_TARGETDEPS += \
    $${DESTDIR}/wz4lib.lib \


#--------------------------------------------------------------------
# libraries link
#--------------------------------------------------------------------

LIBS += -L$${URHO3D}/lib -lUrho3D
LIBS += -L$${DESTDIR} -lwz4lib

win32:LIBS +=   kernel32.lib user32.lib gdi32.lib comdlg32.lib advapi32.lib shell32.lib \
                ole32.lib oleaut32.lib uuid.lib imm32.lib winmm.lib wsock32.lib opengl32.lib glu32.lib version.lib

unix:!macx: LIBS += -lXinerama -lXcursor -lXrandr -lXi -ldl -lXxf86vm  -lpthread -lGL -lGLU -lX11

#win32:LIBS += opengl32.lib glu32.lib winmm.lib dsound.lib dxgi.lib d3d11.lib d3dcompiler.lib imm32.lib msimg32.lib version.lib ws2_32.lib \
#qwindowsd.lib qt5platformsupportd.lib qt5guid.lib qt5cored.lib qt5xmld.lib qt5widgetsd.lib qtmaind.lib

#--------------------------------------------------------------------
# project files
#--------------------------------------------------------------------

HEADERS += \
    ../configinfo.hpp \   
    ../eshared/engine/demo.hpp \
    ../eshared/engine/path.hpp \
    ../eshared/engine/sequencer.hpp \
    ../eshared/math/aabb.hpp \
    ../eshared/math/math.hpp \
    ../eshared/math/matrix.hpp \
    ../eshared/math/plane.hpp \
    ../eshared/math/quat.hpp \
    ../eshared/math/ray.hpp \
    ../eshared/math/transform.hpp \
    ../eshared/math/vector.hpp \
    ../eshared/opstacking/demodata.hpp \
    ../eshared/opstacking/demoscript.hpp \
    ../eshared/opstacking/ioperator.hpp \
    ../eshared/opstacking/opmacros.hpp \
    ../eshared/opstacking/oppage.hpp \
    ../eshared/opstacking/opstacking.hpp \
    ../eshared/opstacking/parameter.hpp \
    ../eshared/opstacking/script.hpp \
    ../eshared/packing/arith.hpp \
    ../eshared/packing/bwt.hpp \
    ../eshared/packing/ipacker.hpp \
    ../eshared/packing/mtf.hpp \
    ../eshared/packing/packing.hpp \
    ../eshared/packing/rle.hpp \
    ../eshared/system/array.hpp \
    ../eshared/system/color.hpp \
    ../eshared/system/datastream.hpp \
    ../eshared/system/file.hpp \
    ../eshared/system/point.hpp \
    ../eshared/system/profiler.hpp \
    ../eshared/system/rect.hpp \
    ../eshared/system/runtime.hpp \
    ../eshared/system/simd.hpp \
    ../eshared/system/string.hpp \
    ../eshared/system/system.hpp \
    ../eshared/system/timer.hpp \
    ../eshared/system/types.hpp \
    ../eshared/system/threading.hpp \
    ../eshared/eshared.hpp \
    gui/addopdlg.hpp \
    gui/demoseqview.hpp \
    gui/findopdlg.hpp \
    gui/linkopdlg.hpp \
    gui/mainwnd.hpp \
    gui/pagetree.hpp \
    gui/pageview.hpp \
    gui/paramview.hpp \
    gui/paramwidgets.hpp \
    gui/pathview.hpp \
    gui/renderview.hpp \
    gui/timelineview.hpp \
    gui/trackedit.hpp \
    resources/resource.h \    
    ../eshared/system/compiler.hpp \
    ../eshared/system/memorytrack.hpp \
    ../eshared/engine/graphicsdef.hpp \
    ../eshared/engine/graphics.hpp \
    ../eshared/engine/device.hpp \
    ../eshared/baseoperators/irealnode.hpp \
    ../eshared/baseoperators/icomponent.hpp \
    ../eshared/baseoperators/inode.hpp \
    ../eshared/baseoperators/iviewport.hpp \
    ../eshared/baseoperators/iscene.hpp \
    ../eshared/baseoperators/imisc.hpp \
    ../eshared/baseoperators/isequencer.hpp \
    ../eshared/baseoperators/ibitmap.hpp \
    ../eshared/urhoobjects/uCameraController.hpp \
    ../eshared/opstacking/ioperatorrenderer.hpp \
    ../eshared/baseoperators/ieffect.hpp \
    ../eshared/baseoperators/imesh.hpp \
    ../eshared/urhoobjects/uAnimatorNodePRS.hpp \
    ../eshared/urhoobjects/uWz4Meshcomponent.hpp

SOURCES += \
    ../../src/eshared/engine/demo.cpp \
    ../eshared/engine/path.cpp \
    ../eshared/engine/sequencer.cpp \
    ../eshared/math/aabb.cpp \
    ../eshared/math/matrix.cpp \
    ../eshared/math/plane.cpp \
    ../eshared/math/quat.cpp \
    ../eshared/math/ray.cpp \
    ../eshared/math/transform.cpp \
    ../eshared/math/vector.cpp \
    ../eshared/opstacking/demodata.cpp \    
    ../eshared/opstacking/demoscript.cpp \
    ../eshared/opstacking/ioperator.cpp \
    ../eshared/opstacking/oppage.cpp \
    ../eshared/opstacking/parameter.cpp \
    ../eshared/opstacking/script.cpp \   
    ../eshared/packing/arith.cpp \
    ../eshared/packing/bwt.cpp \
    ../eshared/packing/mtf.cpp \
    ../eshared/packing/rle.cpp \
    ../eshared/system/array.cpp \
    ../eshared/system/color.cpp \
    ../eshared/system/datastream.cpp \
    ../eshared/system/file.cpp \
    ../eshared/system/point.cpp \
    ../eshared/system/profiler.cpp \
    ../eshared/system/rect.cpp \
    ../eshared/system/runtime.cpp \
    ../eshared/system/simd.cpp \
    ../eshared/system/string.cpp \
    ../eshared/system/timer.cpp \
    ../eshared/system/threading.cpp \
    gui/addopdlg.cpp \
    gui/demoseqview.cpp \
    gui/findopdlg.cpp \
    gui/linkopdlg.cpp \
    gui/mainwnd.cpp \
    gui/pagetree.cpp \
    gui/pageview.cpp \
    gui/paramview.cpp \
    gui/paramwidgets.cpp \
    gui/pathview.cpp \
    gui/renderview.cpp \
    gui/timelineview.cpp \
    gui/trackedit.cpp \
    estudio4.cpp \
    ../eshared/system/memorytrack.cpp \
    ../eshared/operators/meshops.cpp \
    ../eshared/operators/bitmapops.cpp \
    ../eshared/operators/componentops.cpp \
    ../eshared/operators/modelops.cpp \
    ../eshared/operators/effectops.cpp \
    ../eshared/operators/miscops.cpp \
    ../eshared/operators/sequencerops.cpp \
    ../eshared/engine/graphics.cpp \
    ../eshared/engine/device.cpp \
    ../eshared/baseoperators/ibitmap.cpp \
    ../eshared/opstacking/ioperatorrenderer.cpp \
    ../eshared/baseoperators/imesh.cpp \
    ../eshared/urhoobjects/uAnimatorNodePRS.cpp \
    ../eshared/urhoobjects/uWz4MeshComponent.cpp

   
RESOURCES += \
    resources/resource.qrc


OTHER_FILES += \
   

FORMS += \
    gui/findopdlg.ui \
    gui/linkopdlg.ui \
    gui/mainwnd.ui

