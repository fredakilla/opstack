/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   This file is part of
 *       ______        _                             __ __
 *      / ____/____   (_)____ _ ____ ___   ____ _   / // /
 *     / __/  / __ \ / // __ `// __ `__ \ / __ `/  / // /_
 *    / /___ / / / // // /_/ // / / / / // /_/ /  /__  __/
 *   /_____//_/ /_//_/ \__, //_/ /_/ /_/ \__,_/     /_/.   
 *                    /____/                              
 *
 *   Copyright � 2003-2012 Brain Control, all rights reserved.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <QtGui/QResizeEvent>
#include <QtGui/QImageWriter>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QApplication>
#include <QtWidgets/QColorDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>

#include "renderview.hpp"


#include <SDL/SDL.h>

const eF32   eRenderView::CAM_SPEED_ZOOM      = 1.0f;
const eF32   eRenderView::CAM_SPEED_ROTATE    = 1.0f;
const eF32   eRenderView::CAM_SPEED_MOVE_SLOW = 1.0f;
const eF32   eRenderView::CAM_SPEED_MOVE_FAST = 10.0f*eRenderView::CAM_SPEED_MOVE_SLOW;



//------------------------------------------------------------------------------------------------------
// key utilities to convert Qt key to SDL key
//------------------------------------------------------------------------------------------------------
static QMap<Qt::Key, SDL_Keycode> __keymap;
static void __initKeyMap();
static Uint16 __convertQtKeyModifierToSDL(Qt::KeyboardModifiers qtKeyModifiers);
static SDL_Keycode __convertQtKeyToSDL(Qt::Key qtKey);

//------------------------------------------------------------------------------------------------------
// map keys Qt/SDL
//------------------------------------------------------------------------------------------------------
void __initKeyMap()
{
    __keymap[Qt::Key_unknown]     = SDLK_UNKNOWN;
    __keymap[Qt::Key_Escape]      = SDLK_ESCAPE;
    __keymap[Qt::Key_Tab]         = SDLK_TAB;
    __keymap[Qt::Key_Backspace]   = SDLK_BACKSPACE;
    __keymap[Qt::Key_Return]      = SDLK_RETURN;
    __keymap[Qt::Key_Enter]       = SDLK_KP_ENTER;
    __keymap[Qt::Key_Insert]      = SDLK_INSERT;
    __keymap[Qt::Key_Delete]      = SDLK_DELETE;
    __keymap[Qt::Key_Pause]       = SDLK_PAUSE;
    __keymap[Qt::Key_Print]       = SDLK_PRINTSCREEN;
    __keymap[Qt::Key_SysReq]      = SDLK_SYSREQ;
    __keymap[Qt::Key_Home]        = SDLK_HOME;
    __keymap[Qt::Key_End]         = SDLK_END;
    __keymap[Qt::Key_Left]        = SDLK_LEFT;
    __keymap[Qt::Key_Right]       = SDLK_RIGHT;
    __keymap[Qt::Key_Up]          = SDLK_UP;
    __keymap[Qt::Key_Down]        = SDLK_DOWN;
    __keymap[Qt::Key_PageUp]      = SDLK_PAGEUP;
    __keymap[Qt::Key_PageDown]    = SDLK_PAGEDOWN;
    __keymap[Qt::Key_Shift]       = SDLK_LSHIFT;
    __keymap[Qt::Key_Control]     = SDLK_LCTRL;
    __keymap[Qt::Key_Alt]         = SDLK_LALT;
    __keymap[Qt::Key_CapsLock]    = SDLK_CAPSLOCK;
    __keymap[Qt::Key_NumLock]     = SDLK_NUMLOCKCLEAR;
    __keymap[Qt::Key_ScrollLock]  = SDLK_SCROLLLOCK;
    __keymap[Qt::Key_F1]          = SDLK_F1;
    __keymap[Qt::Key_F2]          = SDLK_F2;
    __keymap[Qt::Key_F3]          = SDLK_F3;
    __keymap[Qt::Key_F4]          = SDLK_F4;
    __keymap[Qt::Key_F5]          = SDLK_F5;
    __keymap[Qt::Key_F6]          = SDLK_F6;
    __keymap[Qt::Key_F7]          = SDLK_F7;
    __keymap[Qt::Key_F8]          = SDLK_F8;
    __keymap[Qt::Key_F9]          = SDLK_F9;
    __keymap[Qt::Key_F10]         = SDLK_F10;
    __keymap[Qt::Key_F11]         = SDLK_F11;
    __keymap[Qt::Key_F12]         = SDLK_F12;
    __keymap[Qt::Key_F13]         = SDLK_F13;
    __keymap[Qt::Key_F14]         = SDLK_F14;
    __keymap[Qt::Key_F15]         = SDLK_F15;
    __keymap[Qt::Key_Menu]        = SDLK_MENU;
    __keymap[Qt::Key_Help]        = SDLK_HELP;

    // A-Z
    for(int key='A'; key<='Z'; key++)
        __keymap[Qt::Key(key)] = key + 32;

    // 0-9
    for(int key='0'; key<='9'; key++)
        __keymap[Qt::Key(key)] = key;
}

//------------------------------------------------------------------------------------------------------
// get SDL key from Qt key
//------------------------------------------------------------------------------------------------------
SDL_Keycode __convertQtKeyToSDL(Qt::Key qtKey)
{
    SDL_Keycode sldKey = __keymap.value(Qt::Key(qtKey));

    if(sldKey == 0)
        ePRINT("Warning: Key %d not mapped", qtKey);

    return sldKey;
}

//------------------------------------------------------------------------------------------------------
// get SDL key modifier from Qt key modifier
//------------------------------------------------------------------------------------------------------
Uint16 __convertQtKeyModifierToSDL(Qt::KeyboardModifiers qtKeyModifiers)
{
    Uint16 sdlModifiers = KMOD_NONE;

    if(qtKeyModifiers.testFlag(Qt::ShiftModifier))
        sdlModifiers |= KMOD_LSHIFT | KMOD_RSHIFT;
    if(qtKeyModifiers.testFlag(Qt::ControlModifier))
        sdlModifiers |= KMOD_LCTRL | KMOD_RCTRL;
    if(qtKeyModifiers.testFlag(Qt::AltModifier))
        sdlModifiers |= KMOD_LALT | KMOD_RALT;

    return sdlModifiers;
}



eRenderView::eRenderView(QWidget *parent) : QWidget(parent), Object(eGraphics::m_gcontext),
    m_viewOp(nullptr),
    m_editOp(nullptr),
    m_lastCalcMs(0.0f),
    m_lastFrameMs(0.0f),
    m_opChanged(eFALSE),
    m_time(0.0f),
    //m_engine(eWF_VSYNC, eSize(300, 200), (ePtr)winId()),
    m_camLocked(eFALSE),
    m_actShowGrid(nullptr),
    m_actWireframe(nullptr),
    m_actShowNormals(nullptr),
    m_actShowBBoxes(nullptr),
    m_actShowTiled(nullptr),
    m_actVisualAlpha(nullptr),
    m_actScale(nullptr),
    m_actRotate(nullptr),
    m_actTrans(nullptr),
    m_lblStats((QWidget *)this),
    m_progressBar(this),
    m_progressStartTime(0)
{    
    // init keymap for Qt > SDL keys conversion
    __initKeyMap();

    setMouseTracking(true);

    setAttribute(Qt::WA_PaintOnScreen);
    setAttribute(Qt::WA_NativeWindow);

    //setUpdatesEnabled(false);
    setFocusPolicy(Qt::StrongFocus);

    _createActions();
    _initProgressBar();


    m_timerId = startTimer(0);

    SubscribeToEvent(E_LOGMESSAGE, URHO3D_HANDLER(eRenderView, _handleLogMessage));
}

eRenderView::~eRenderView()
{
    killTimer(m_timerId);


    
    // has to be shutdown before profiler
    // (because of thread)

    eOpStacking::shutdown();
    eProfiler::shutdown();

    // free all registered opRenderer
    for(eU32 i=0; i<m_opRenderers.size(); i++)
        delete m_opRenderers[i];
}

void eRenderView::processOperators()
{
    if(m_viewOp)
    {
        m_viewOp->setChanged();
        if (m_viewOp->process(m_time, _progressCallback, this) == eOPR_CHANGES)
        {
            m_opChanged  = eTRUE;

#ifdef eEDITOR
            // mark m_viewOp as currently shown
            m_viewOp->setCurrentShownOp(eTRUE);
#endif
        }
    }
}

void eRenderView::setViewOp(eIOperator *viewOp)
{
    if (m_viewOp == viewOp)
        return;

    m_viewOp = viewOp;
    m_opChanged = eTRUE;

    // clear menu and actions
    for (eInt i=actions().size()-1; i>=0; i--)
        removeAction(actions().at(i));

    m_menu.clear();

    // use default menu if operator does not exists
    if (!viewOp)
    {
        addActions(m_defMenu.actions());
        m_menu.addActions(m_defMenu.actions());
        return;
    }

    // use menu for operator category
    const eOpClass opc = viewOp->getResultClass();

    switch (opc)
    {
    case eOC_BMP:
        addActions(m_bmpMenu.actions());
        m_menu.addActions(m_bmpMenu.actions());
        break;
    case eOC_MESH:
        addActions(m_meshMenu.actions());
        m_menu.addActions(m_meshMenu.actions());
        break;
    case eOC_NODE:
        addActions(m_modelMenu.actions());
        m_menu.addActions(m_modelMenu.actions());
        break;
    case eOC_FX:
        addActions(m_fxMenu.actions());
        m_menu.addActions(m_fxMenu.actions());
        break;
    default:
        addActions(m_defMenu.actions());
        m_menu.addActions(m_defMenu.actions());
        break;
    }   
}

void eRenderView::setEditOp(eIOperator *editOp)
{
    m_editOp = (editOp ? editOp : m_viewOp);
}

void eRenderView::setTime(eF32 time)
{
    eASSERT(time >= 0.0f);
    m_time = time;
}

eIOperator * eRenderView::getViewOp() const
{
    return m_viewOp;
}

eIOperator * eRenderView::getEditOp() const
{
    return m_editOp;
}


eF32 eRenderView::getLastCalcMs() const
{
    return m_lastCalcMs;
}

eF32 eRenderView::getLastFrameMs() const
{
    return m_lastFrameMs;
}

eF32 eRenderView::getAvgFrameMs() const
{
    return m_frameTimer.getAvgElapsedMs();
}



QSize eRenderView::sizeHint() const
{
    return QSize(120, 500);
}

// needed because we're painting with Direct3D 
// on a widget normally controlled by QT
QPaintEngine * eRenderView::paintEngine() const
{
    return nullptr;
}

void eRenderView::_renderOperator()
{
    // operator invalid?
    if (!m_viewOp || m_viewOp->getError() != eOE_OK)
    {
        //_renderNone();
        return;
    }

    // compute timestep for manually updating urho scene
    static float lasttime = 0.0f;
    float step = lasttime - m_time;
    lasttime = m_time;


    // process operator
    eTimer timer;
    //if (m_viewOp->process(m_time, _progressCallback, this) == eOPR_CHANGES)
    //    m_opChanged  = eTRUE;

    m_lastCalcMs = timer.getElapsedMs();



    eIOperatorRenderer* opRenderer = getOperatorRenderer(m_viewOp);

    // if operator get changed, render operator
    if(m_opChanged || m_viewOp->getResultClass() == eOC_DEMO )
    {
        //eIOperatorRenderer* opRenderer = getOperatorRenderer(m_viewOp);
        //opRenderer->setReady(true);
        if(opRenderer)
        {
            // remove all nodes in scene and set renderer camera
            //opRenderer->clearScene();
            //opRenderer->getDefaultScene()->RemoveChildren(true,true,false);
            opRenderer->getRootNode()->RemoveChildren(true, true, false);


            // set viewport from operator renderer
            Renderer* urhoRender = GetSubsystem<Renderer>();
            urhoRender->SetViewport(0, opRenderer->getViewport());

            // clear any existing UI
            GetSubsystem<UI>()->Clear();

            // set new UI from operator renderer
            if(opRenderer->getRootUI())
                GetSubsystem<UI>()->GetRoot()->AddChild(opRenderer->getRootUI());


            if(m_viewOp->getResultClass() == eOC_DEMO)
            {
                eIOpDemoRenderer* demoRenderer = (eIOpDemoRenderer*)opRenderer;
                demoRenderer->setTimeLine(m_time);
            }

            // render operator
            opRenderer->operatorUpdated(m_viewOp);


        }
        else
        {
            ePRINT("Operator %s doesn't have any renderer", m_viewOp->getMetaInfos().className.c_str() );
        }

    }
    //else
    {



        m_viewOp->processAnimation(m_time, _progressCallback, this);
    }














    // manually update urho scene
    if(opRenderer)
    {        
        opRenderer->getDefaultScene()->Update(step);

        /*static float lasttime = 0.0f;
        float step = lasttime - m_time;
        opRenderer->getDefaultScene()->Update(step);
        lasttime = m_time;*/
    }



    // render operator
    /*switch (m_viewOp->getResultClass())
    {
    case eOC_BMP:
        _renderBitmapOp((eIBitmapOp *)m_viewOp);
        break;
    case eOC_MESH:
        _renderMeshOp((eIMeshOp *)m_viewOp);
        break;
    case eOC_MODEL:
        _renderModelOp((eIModelOp *)m_viewOp);
        break;
    case eOC_SEQ:
        _renderSequencerOp((eISequencerOp *)m_viewOp);
        break;
    case eOC_FX:
        _renderEffectOp((eIEffectOp *)m_viewOp);
        break;
    case eOC_DEMO:
        _renderDemoOp((eIDemoOp *)m_viewOp);
        break;
    case eOC_MAT:
        _renderMaterialOp((eIMaterialOp *)m_viewOp);
        break;
    case eOC_R2T:
        _renderRenderToTextureOp((eIRenderToTextureOp *)m_viewOp);
        break;
    default:
        _renderNone();
        break;
    }*/

    m_opChanged = eFALSE;
}




// percent is in {0,...,100}
void eRenderView::_updateProgress(eU32 percent)
{
    eASSERT(percent <= 100);

    if (percent == 0)
        m_progressStartTime = eTimer::getTimeMs();

    if (eTimer::getTimeMs()-m_progressStartTime >= PB_WAIT_TIL_SHOW)
    {
        m_progressBar.show();
        m_progressBar.setValue(percent);

        // only set cursor once cause override
        // cursors are pushed to a stack
        static eBool cursorSet = eFALSE;
        if (!cursorSet)
        {
            QApplication::setOverrideCursor(Qt::WaitCursor);
            cursorSet = eTRUE;
        }

        if (percent == 100)
        {
            m_progressBar.hide();
            QApplication::restoreOverrideCursor();
            cursorSet = eFALSE;
        }
    }
}

eBool eRenderView::_progressCallback(eU32 processed, eU32 total, ePtr param)
{
    eASSERT(processed <= total);
    eASSERT(total > 0);
    ((eRenderView *)param)->_updateProgress((eU32)eLerp(0.0f, 100.0f, (eF32)processed/(eF32)total));
    return eTRUE;
}

/*void eRenderView::_updateKdMesh(const eSceneData &sd)
{
    m_kdMesh.clear();
    eCuller kdTree(sd);

	for (eU32 i=0; i<kdTree.m_calculatedRecords.size(); i++)
    {
        eVector3 corners[8];
		kdTree.m_calculatedRecords[i].m_aabb.getCorners(corners);
        m_kdMesh.addWireCube(corners, COL_AABB_LEAF);
    }

    m_kdMesh.finishLoading(eMT_DYNAMIC);
}*/

/*void eRenderView::_updateBoundingBoxMesh(const eAABB &bbox)
{
    eVector3 corners[8];
    bbox.getCorners(corners);

    m_bboxMesh.clear();
    m_bboxMesh.addWireCube(corners, COL_AABB_LEAF);
    m_bboxMesh.finishLoading(eMT_DYNAMIC);
}*/

void eRenderView::_createActions()
{
    // create default actions
    QAction *actToggleFs = m_defMenu.addAction("Toggle fullscreen", this, SIGNAL(onToggleFullscreen()));
    actToggleFs->setCheckable(true);
    actToggleFs->setShortcut(QKeySequence("tab"));
    actToggleFs->setShortcutContext(Qt::WidgetShortcut);

#ifdef OLD_ENIGMA

    // create bitmap actions
    m_bmpMenu.addAction(actToggleFs);
    
    QAction *actResetVp = m_bmpMenu.addAction("Reset viewport", this, SLOT(_onResetViewport()));
    actResetVp->setShortcut(QKeySequence("r"));
    actResetVp->setShortcutContext(Qt::WidgetShortcut);

    m_bmpMenu.addAction("Save bitmap as", this, SLOT(_onSaveBitmapAs()));
    m_bmpMenu.addSeparator();

    m_actShowTiled = m_bmpMenu.addAction("Show tiled", this, SLOT(_onBmpTiling()));
    m_actShowTiled->setShortcut(QKeySequence("t"));
    m_actShowTiled->setShortcutContext(Qt::WidgetShortcut);
    m_actShowTiled->setCheckable(true);

    m_actVisualAlpha = m_bmpMenu.addAction("Visualize alpha", this, SLOT(_onBmpVisualizeAlpha()));
    m_actVisualAlpha->setShortcut(QKeySequence("a"));
    m_actVisualAlpha->setShortcutContext(Qt::WidgetShortcut);
    m_actVisualAlpha->setCheckable(true);
    m_actVisualAlpha->setChecked(true);

    QAction *actZoomIn = m_bmpMenu.addAction("Zoom in", this, SLOT(_onTextureZoomIn()));
    actZoomIn->setShortcut(QKeySequence("+"));
    actZoomIn->setShortcutContext(Qt::WidgetShortcut);

    QAction *actZoomOut = m_bmpMenu.addAction("Zoom out", this, SLOT(_onTextureZoomOut()));
    actZoomOut->setShortcut(QKeySequence("-"));
    actZoomOut->setShortcutContext(Qt::WidgetShortcut);

    // create R2T menu
    m_r2tMenu.addAction(actToggleFs);
    m_r2tMenu.addAction(actResetVp);
    
    QAction *actScreenShot = m_r2tMenu.addAction("Save screenshot", this, SLOT(_onSaveScreenShot()));
    
    m_r2tMenu.addSeparator();
    m_r2tMenu.addAction(actZoomIn);
    m_r2tMenu.addAction(actZoomOut);

    // create mesh actions
    m_meshMenu.addAction(actToggleFs);
    m_meshMenu.addAction(actResetVp);
    m_meshMenu.addAction(actScreenShot);
    m_meshMenu.addSeparator();

    m_actShowGrid = m_meshMenu.addAction("Show grid", this, SLOT(_onToggleShowGrid()));
    m_actShowGrid->setShortcut(QKeySequence("g"));
    m_actShowGrid->setShortcutContext(Qt::WidgetShortcut);
    m_actShowGrid->setCheckable(true);
    m_actShowGrid->setChecked(true);

    m_actWireframe = m_meshMenu.addAction("Render wireframe", this, SLOT(_onToggleWireframe()));
    m_actWireframe->setShortcut(QKeySequence("f"));
    m_actWireframe->setShortcutContext(Qt::WidgetShortcut);
    m_actWireframe->setCheckable(true);

    m_actShowNormals = m_meshMenu.addAction("Show normals", this, SLOT(_onToggleShowNormals()));
    m_actShowNormals->setShortcut(QKeySequence("n"));
    m_actShowNormals->setShortcutContext(Qt::WidgetShortcut);
    m_actShowNormals->setCheckable(true);

    m_actShowBBoxes = m_meshMenu.addAction("Show bounding boxes", this, SLOT(_onToggleBoundingBoxes()));
    m_actShowBBoxes->setShortcut(QKeySequence("b"));
    m_actShowBBoxes->setShortcutContext(Qt::WidgetShortcut);
    m_actShowBBoxes->setCheckable(true);

    m_meshMenu.addSeparator();
    m_actScale = m_meshMenu.addAction("Scale widget", this, SLOT(_onWidgetScale()));
    m_actScale->setShortcut(QKeySequence("y"));
    m_actScale->setShortcutContext(Qt::WidgetShortcut);
    m_actScale->setCheckable(true);

    m_actRotate = m_meshMenu.addAction("Rotate widget", this, SLOT(_onWidgetRotate()));
    m_actRotate->setShortcut(QKeySequence("x"));
    m_actRotate->setShortcutContext(Qt::WidgetShortcut);
    m_actRotate->setCheckable(true);

    m_actTrans = m_meshMenu.addAction("Translate widget", this, SLOT(_onWidgetTranslate()));
    m_actTrans->setShortcut(QKeySequence("c"));
    m_actTrans->setShortcutContext(Qt::WidgetShortcut);
    m_actTrans->setCheckable(true);
    m_actTrans->setChecked(true);

    QActionGroup *ac = new QActionGroup(this);
    ac->addAction(m_actScale);
    ac->addAction(m_actRotate);
    ac->addAction(m_actTrans);

    // create model actions
    m_modelMenu.addAction(actToggleFs);
    m_modelMenu.addAction(actResetVp);
    m_modelMenu.addAction(actScreenShot);
    m_modelMenu.addSeparator();
    m_modelMenu.addAction(m_actShowGrid);
    m_modelMenu.addAction(m_actWireframe);
    m_modelMenu.addAction(m_actShowBBoxes);
    m_modelMenu.addSeparator();
    m_modelMenu.addAction(m_actScale);
    m_modelMenu.addAction(m_actRotate);
    m_modelMenu.addAction(m_actTrans);

    // create effect actions
    m_fxMenu.addAction(actToggleFs);
    m_fxMenu.addAction(actResetVp);
    m_fxMenu.addAction(actScreenShot);
    m_fxMenu.addSeparator();

    m_actFixedCam = m_meshMenu.addAction("Fixed camera", this, SLOT(_onFxCameraFixed()));
    m_actFixedCam->setShortcut(QKeySequence("t"));
    m_actFixedCam->setShortcutContext(Qt::WidgetShortcut);
    m_actFixedCam->setCheckable(true);
    m_actFixedCam->setChecked(true);

    m_actFreeCam = m_meshMenu.addAction("Free camera", this, SLOT(_onFxCameraFree()));
    m_actFreeCam->setShortcut(QKeySequence("z"));
    m_actFreeCam->setShortcutContext(Qt::WidgetShortcut);
    m_actFreeCam->setCheckable(true);

    m_actLinkCam = m_meshMenu.addAction("Link camera", this, SLOT(_onFxCameraLink()));
    m_actLinkCam->setShortcut(QKeySequence("u"));
    m_actLinkCam->setShortcutContext(Qt::WidgetShortcut);
    m_actLinkCam->setCheckable(true);

    m_fxMenu.addAction(m_actFixedCam);
    m_fxMenu.addAction(m_actFreeCam);
    m_fxMenu.addAction(m_actLinkCam);

    ac = new QActionGroup(this);
    ac->addAction(m_actFixedCam);
    ac->addAction(m_actFreeCam);
    ac->addAction(m_actLinkCam);

    // use default menu actions in the beginning
    addActions(m_defMenu.actions());
    m_menu.addActions(m_defMenu.actions());

#endif
}

void eRenderView::_initProgressBar()
{
    m_progressBar.hide();
    m_progressBar.setTextVisible(false); // important as text rendering does not work on render-view
    QVBoxLayout *vbl = new QVBoxLayout(this);
    vbl->addWidget(&m_progressBar, 1);
}

eUserInput eRenderView::_getUserInput() const
{
    const QPoint mousePos = mapFromGlobal(QCursor::pos());
    const QPoint mouseDelta = m_mouseDownPos-mousePos;
    const Qt::MouseButtons mb = QApplication::mouseButtons();

    eUserInput input;
    input.mouseDelta.set(mouseDelta.x(), mouseDelta.y());
    input.mousePos.set(mousePos.x(), mousePos.y());
    input.mouseBtns = (underMouse() ? (mb&Qt::LeftButton ? eMB_LEFT : 0)|(mb&Qt::RightButton ? eMB_RIGHT : 0)|(mb&Qt::MidButton ? eMB_MIDDLE : 0) : eMB_NONE);
    return input;
}

eBool eRenderView::_isKeyDown(Qt::Key key) const
{
    return (m_keysDown.contains(key) ? eTRUE : eFALSE);
}

void eRenderView::_saveImageAs(eU32 width, eU32 height, const eArray<eColor> &data, eInt quality)
{
    // swap R and B channel
    eArray<eColor> swapped = data;
    for (eU32 i=0; i<data.size(); i++)
        eSwap(swapped[i].r, swapped[i].b);

    // create filter of supported formats for dialog
    const QList<QByteArray> &formats = QImageWriter::supportedImageFormats();
    QString filter;

    for (eInt i=0; i<formats.size(); i++)
    {
        if (i > 0)
            filter += ";;";
        
        filter += formats[i].toUpper();
        filter += " (";
        filter += "*.";
        filter += formats[i];
        filter += ")";
    }

    eASSERT(!formats.empty());

    // ask for file name
    QString selFilter = "PNG (*.png)";
    const QString fileName = QFileDialog::getSaveFileName(this, "", "", filter, &selFilter);

    if (fileName != "")
    {
        // find correct format
        QString format;
        for (eInt i=0; i<formats.size(); i++)
            if (fileName.endsWith(formats[i]))
                format = formats[i];

        if (format == "")
        {
            const QStringList list = selFilter.split(" ");
            eASSERT(!list.empty());
            format = list[0];
        }

        // save image to file
        QApplication::setOverrideCursor(Qt::WaitCursor);
        QImage img(width, height, QImage::Format_ARGB32);
        eMemCopy(img.bits(), &swapped[0], swapped.size()*sizeof(eColor));
        img.save(fileName, format.toLatin1(), quality);
        QApplication::restoreOverrideCursor();
    }
}

void eRenderView::_copy1stPovCameraToLocal()
{
    const eIOpPtrArray povOps = _getOpsOfTypeInStack(m_editOp, eOP_TYPE("Misc", "POV"));
    if (!povOps.isEmpty())
    {
        m_camEyePos = povOps[0]->getParameter(8).getAnimValue().fxyz;
        m_camLookAt = povOps[0]->getParameter(9).getAnimValue().fxyz;
    }
}

eIOpPtrArray eRenderView::_getOpsOfTypeInStack(eIOperator *op, eU32 opType) const
{
    eIOpPtrArray ops;
    m_editOp->getOpsInStack(ops);

    for (eInt i=(eInt)ops.size()-1; i>=0; i--)
        if (ops[i]->getMetaInfos().type != opType)
            ops.removeSwap(i);

    return ops;
}

void eRenderView::contextMenuEvent(QContextMenuEvent *ce)
{
    QWidget::contextMenuEvent(ce);

    // show context menu if mouse was not
    // moved too much
    if ((m_mouseDownPos-ce->pos()).manhattanLength() < 4)
        m_menu.exec(QCursor::pos());
}

void eRenderView::mousePressEvent(QMouseEvent *me)
{
    QWidget::mousePressEvent(me);

    m_lastMousePos = me->pos();
    m_mouseDownPos = me->pos();

    if (me->buttons() & Qt::RightButton)
        setContextMenuPolicy(Qt::PreventContextMenu);


    SDL_Event sdlEvent;
    sdlEvent.type = SDL_MOUSEBUTTONDOWN;
    sdlEvent.button.state = SDL_PRESSED;
    if(me->buttons() & Qt::LeftButton)
        sdlEvent.button.button = SDL_BUTTON_LMASK;
    if(me->buttons() & Qt::RightButton)
        sdlEvent.button.button = SDL_BUTTON_RMASK;
    if(me->buttons() & Qt::MiddleButton)
        sdlEvent.button.button = SDL_BUTTON_LMASK;
    QPoint position = me->pos();
    sdlEvent.button.x = position.x();
    sdlEvent.button.y = position.y();
    SDL_PushEvent(&sdlEvent);


    const eBool leftBtn = (me->buttons() & Qt::LeftButton);
    if(leftBtn)
    {
        QPoint glob = mapToGlobal(QPoint(width()/2,height()/2));
        QCursor::setPos(glob);
        SDL_SetRelativeMouseMode(SDL_TRUE);
    }
}

void eRenderView::mouseReleaseEvent(QMouseEvent *me)
{
    QWidget::mouseReleaseEvent(me);

    if (me->button() == Qt::RightButton)
    {
        QContextMenuEvent ce(QContextMenuEvent::Mouse, me->pos());
        setContextMenuPolicy(Qt::DefaultContextMenu);
        contextMenuEvent(&ce);
    }

    SDL_Event sdlEvent;
    sdlEvent.type = SDL_MOUSEBUTTONUP;
    sdlEvent.button.state = SDL_RELEASED;
    if(me->buttons() & Qt::LeftButton)
        sdlEvent.button.button = SDL_BUTTON_LMASK;
    if(me->buttons() & Qt::RightButton)
        sdlEvent.button.button = SDL_BUTTON_RMASK;
    if(me->buttons() & Qt::MiddleButton)
        sdlEvent.button.button = SDL_BUTTON_LMASK;
    QPoint position = me->pos();
    sdlEvent.button.x = position.x();
    sdlEvent.button.y = position.y();
    SDL_PushEvent(&sdlEvent);


    if(SDL_GetRelativeMouseMode())
    {
        SDL_SetRelativeMouseMode(SDL_FALSE);
        QPoint glob = mapToGlobal(QPoint(width()/2,height()/2));
        QCursor::setPos(glob);
    }
}

void eRenderView::mouseMoveEvent(QMouseEvent *me)
{
#if 0
    QWidget::mouseMoveEvent(me);
    
    const eBool leftBtn = (me->buttons() & Qt::LeftButton);
    const QPoint move = m_lastMousePos-me->pos();
    m_lastMousePos = me->pos();

    /*SDL_Event sdlEvent;
    sdlEvent.type = SDL_MOUSEMOTION;
    Qt::MouseButton button = me->button();
    if( (button & Qt::LeftButton))
        sdlEvent.motion.state |= SDL_BUTTON_LMASK;
    if( (button & Qt::RightButton))
        sdlEvent.motion.state |= SDL_BUTTON_RMASK;
    if( (button & Qt::MidButton))
        sdlEvent.motion.state |= SDL_BUTTON_MMASK;
    sdlEvent.motion.xrel = move.x();
    sdlEvent.motion.yrel = move.y();
    SDL_PushEvent(&sdlEvent);*/


    /*Graphics* graphics = GetSubsystem<Graphics>();
    SDL_Window * win = (SDL_Window*)graphics->GetImpl()->GetWindow();*/



   /* SDL_Event event;
    event.motion.type = SDL_MOUSEMOTION;
    event.motion.windowID = 1; //mouse->focus ? mouse->focus->id : 0;
    event.motion.which = 0; //mouseID;
    event.motion.state |= SDL_BUTTON_LMASK;
    event.motion.x = me->pos().x();
    event.motion.y = me->pos().y();
    event.motion.xrel = me->pos().x();
    event.motion.yrel = me->pos().y();
    SDL_PushEvent(&event);*/


    int x = me->pos().x();
    int y = me->pos().y();

    Graphics* graphics = GetSubsystem<Graphics>();
    SDL_Window * win = (SDL_Window*)graphics->GetImpl()->GetWindow();



    SDL_SetRelativeMouseMode(SDL_TRUE);
    SDL_WarpMouseInWindow(win,x,y);
    //ePRINT("Mouse = %d %d", move.x(), move.y());
    //SDL_sen


    QPoint glob = mapToGlobal(QPoint(width()/2,height()/2));
    QCursor::setPos(glob);


#endif


}

void eRenderView::keyReleaseEvent(QKeyEvent *ke)
{
    QWidget::keyReleaseEvent(ke);

    // remove key from held keys list
    const eInt index = m_keysDown.indexOf(ke->key());
    if (index != -1 && !ke->isAutoRepeat())
        m_keysDown.removeAt(index);

    // Transmit key release event to SDL
    SDL_Event sdlEvent;
    sdlEvent.type = SDL_KEYUP;
    sdlEvent.key.keysym.sym = __convertQtKeyToSDL( Qt::Key(ke->key()) );
    sdlEvent.key.keysym.mod = __convertQtKeyModifierToSDL(ke->modifiers());
    SDL_PushEvent(&sdlEvent);
}

void eRenderView::keyPressEvent(QKeyEvent *ke)
{
    QWidget::keyPressEvent(ke);

    // add key to held keys list
    if (!m_keysDown.contains(ke->key()) && !ke->isAutoRepeat())
        m_keysDown.append(ke->key());

    // Transmit key press event to SDL
    SDL_Event sdlEvent;
    sdlEvent.type = SDL_KEYDOWN;
    sdlEvent.key.keysym.sym = __convertQtKeyToSDL( Qt::Key(ke->key()) );
    sdlEvent.key.keysym.mod = __convertQtKeyModifierToSDL(ke->modifiers());
    SDL_PushEvent(&sdlEvent);
}

// zoom in/out using mouse-wheel
void eRenderView::wheelEvent(QWheelEvent *we)
{
    QWidget::wheelEvent(we);

    const eInt wheelDeg = we->delta()/8; // degrees wheel was turned
    const eInt wheelSteps = wheelDeg/15; // number of steps wheel was turned
}

void eRenderView::resizeEvent(QResizeEvent *re)
{
    QWidget::resizeEvent(re);

    int width = re->size().width();
    int height = re->size().height();

    Graphics* graphics = GetSubsystem<Graphics>();

    SDL_Window * win = (SDL_Window*)graphics->GetWindow();
    SDL_SetWindowSize(win, width, height);

    if(m_viewOp)
    {
        eIOperatorRenderer* opRenderer = getOperatorRenderer(m_viewOp);
        opRenderer->resize(width, height);
    }
}

void eRenderView::timerEvent(QTimerEvent *te)
{
    QWidget::timerEvent(te);
    QApplication::processEvents();
    //update();

    ePROFILER_BEGIN_THREAD_FRAME();
    {
        ePROFILER_FUNC();
        m_lastFrameMs = m_frameTimer.restart();
        eGfx->beginFrame();
        _renderOperator();
        eGfx->endFrame();
    }
    ePROFILER_END_THREAD_FRAME();
}

void eRenderView::paintEvent(QPaintEvent *pe)
{
    /*ePROFILER_BEGIN_THREAD_FRAME();
    {
        ePROFILER_FUNC();
        m_lastFrameMs = m_frameTimer.restart();
        eGfx->beginFrame();
        _renderOperator();
        eGfx->endFrame();
    }
    ePROFILER_END_THREAD_FRAME();*/

    QWidget::paintEvent(pe);
}

void eRenderView::_handleLogMessage(StringHash eventType, VariantMap& eventData)
{
    using namespace LogMessage;

    // remove date from log message
    String error = eventData[P_MESSAGE].GetString();
    unsigned bracketPos = error.Find(']');
    if (bracketPos != String::NPOS)
        error = error.Substring(bracketPos + 2);

    switch(eventData[P_LEVEL].GetInt())
    {
    case LOG_INFO:
        ePRINT("[Urho3D] %s", error.CString());
        break;
    case LOG_DEBUG:
        ePRINT("[Urho3D] %s", error.CString());
        break;
    case LOG_WARNING:
        ePRINT("[Urho3D] %s", error.CString());
        break;
    case LOG_ERROR:
        ePRINT("[Urho3D] %s", error.CString());
        break;
    default:
        ePRINT("[Urho3D] %s", error.CString());
    }
}
