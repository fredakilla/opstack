/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   This file is part of
 *       ______        _                             __ __
 *      / ____/____   (_)____ _ ____ ___   ____ _   / // /
 *     / __/  / __ \ / // __ `// __ `__ \ / __ `/  / // /_
 *    / /___ / / / // // /_/ // / / / / // /_/ /  /__  __/
 *   /_____//_/ /_//_/ \__, //_/ /_/ /_/ \__,_/     /_/.   
 *                    /____/                              
 *
 *   Copyright � 2003-2012 Brain Control, all rights reserved.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef RENDER_VIEW_HPP
#define RENDER_VIEW_HPP

#include <QtWidgets/QProgressBar>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMenu>

#include "../../eshared/eshared.hpp"


// viewport widget for rendering.
// all kinds of operators are displayed in this widget.

class eRenderView : public QWidget, public Object
{
    Q_OBJECT
    URHO3D_OBJECT(eRenderView, Object)


private:
    enum CameraType
    {
        CT_FIRST_PERSON,
        CT_ORIGIN_CENTER,
    };

public:
    eRenderView(QWidget *parent);
    virtual ~eRenderView();

    void                    processOperators();
    void                    setViewOp(eIOperator *viewOp);
    void                    setEditOp(eIOperator *editOp);
    void                    setTime(eF32 time);

    eIOperator *            getViewOp() const;
    eIOperator *            getEditOp() const;
    eF32                    getLastCalcMs() const;
    eF32                    getLastFrameMs() const;
    eF32                    getAvgFrameMs() const;

public:
    virtual QSize           sizeHint() const;
    virtual QPaintEngine *  paintEngine() const;

Q_SIGNALS:
    void                    onToggleFullscreen();

private:
    void                    _handleLogMessage(StringHash eventType, VariantMap& eventData);
    void                    _renderOperator();
    void                    _createActions();

    void                    _initProgressBar();
    void                    _updateProgress(eU32 percent);
    static eBool            _progressCallback(eU32 processed, eU32 total, ePtr param);

    eUserInput              _getUserInput() const;
    void                    _resetCamera();
    void                    _doCameraMovement();
    void                    _doCameraRotation(const QPoint &move, CameraType camMode);
    eBool                   _isKeyDown(Qt::Key key) const;
    void                    _saveImageAs(eU32 width, eU32 height, const eArray<eColor> &data, eInt quality);
    void                    _copy1stPovCameraToLocal();
    eIOpPtrArray            _getOpsOfTypeInStack(eIOperator *op, eU32 opType) const;

private:
    virtual void            contextMenuEvent(QContextMenuEvent *ce);
    virtual void            mousePressEvent(QMouseEvent *me);
    virtual void            mouseReleaseEvent(QMouseEvent *me);
    virtual void            mouseMoveEvent(QMouseEvent *me);
    virtual void            keyReleaseEvent(QKeyEvent *ke);
    virtual void            keyPressEvent(QKeyEvent *ke);
    virtual void            wheelEvent(QWheelEvent *we);
    virtual void            resizeEvent(QResizeEvent *re);
    virtual void            timerEvent(QTimerEvent *te);
    virtual void            paintEvent(QPaintEvent *pe);

private:
    static const eF32       CAM_SPEED_ZOOM;
    static const eF32       CAM_SPEED_ROTATE;
    static const eF32       CAM_SPEED_MOVE_SLOW;
    static const eF32       CAM_SPEED_MOVE_FAST;
    static const eU32       PB_WAIT_TIL_SHOW = 250;

private:
    //eDevice                 m_engine;
    QLabel                  m_lblStats;
    eTimer                  m_frameTimer;
    eIOperator *            m_viewOp;               // viewed operator (double click / press 's')
    eIOperator *            m_editOp;               // edited operator (selected one)
    eInt                    m_timerId;
    eF32                    m_lastCalcMs;
    eF32                    m_lastFrameMs;
    eBool                   m_opChanged;
    QPoint                  m_lastMousePos;
    QPoint                  m_mouseDownPos;
    eF32                    m_time;

    // mesh and model mode variables
    eVector3                m_camUpVec;
    eVector3                m_camEyePos;
    eVector3                m_camLookAt;
    eBool                   m_camLocked;
    eOpInteractionInfos     m_oii;

    // stuff used by GUI
    QList<eInt>             m_keysDown;
    QMenu                   m_menu;
    QMenu                   m_defMenu;
    QMenu                   m_bmpMenu;
    QMenu                   m_r2tMenu;
    QMenu                   m_meshMenu;
    QMenu                   m_modelMenu;
    QMenu                   m_fxMenu;
    QAction *               m_actShowGrid;
    QAction *               m_actWireframe;
    QAction *               m_actShowNormals;
    QAction *               m_actShowBBoxes;
    QAction *               m_actShowTiled;
    QAction *               m_actVisualAlpha;
    QAction *               m_actScale;
    QAction *               m_actRotate;
    QAction *               m_actTrans;
    QAction *               m_actFixedCam;
    QAction *               m_actFreeCam;
    QAction *               m_actLinkCam;

    // progress bar variables
    QProgressBar            m_progressBar;
    eU32                    m_progressStartTime;


    ////////////////

    struct SOpClassRenderer
    {
        eOpClass                opClass;
        eIOperatorRenderer*     opRenderer;
    };

    typedef eArray<SOpClassRenderer*> nOpRendersMap;
    nOpRendersMap m_opRenderers;


    eIOperatorRenderer* getOperatorRenderer(eIOperator* op)
    {
        eOpClass operatorClass = op->getResultClass();

        for(int i=0; i<m_opRenderers.size(); i++)
        {
            if(m_opRenderers[i]->opClass == operatorClass)
                return m_opRenderers[i]->opRenderer;
        }

        SOpClassRenderer * opClassRenderer = new SOpClassRenderer;
        opClassRenderer->opClass = operatorClass;
        opClassRenderer->opRenderer = op->createOperatorRenderer();

        m_opRenderers.append(opClassRenderer);

        return opClassRenderer->opRenderer;
    }


    /*typedef std::unordered_map<eOpClass, eIOperatorRenderer*> nOpRendersMap;
    nOpRendersMap m_opRenderers;

    eIOperatorRenderer* getOperatorRenderer(eIOperator* op)
    {
        eIOperatorRenderer* opRenderer = 0;
        eOpClass operatorClass = op->getResultClass();

        nOpRendersMap::iterator it = m_opRenderers.find(operatorClass);
        if(it != m_opRenderers.end())
        {
            opRenderer = m_opRenderers[operatorClass];
        }
        else
        {
            opRenderer = op->createOperatorRenderer();
            if(opRenderer)
                m_opRenderers[operatorClass] = opRenderer;
        }

        return opRenderer;
    }*/

};

#endif // RENDER_VIEW_HPP
