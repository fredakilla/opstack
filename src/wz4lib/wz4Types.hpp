#ifndef WZTYPES_H
#define WZTYPES_H

//-----------------------------------------------------------------------
// find out used platform
#if defined(_WIN32)
    #define sCONFIG_SYSTEM_WINDOWS 1
    #define sPLATFORM sPLAT_WINDOWS
    #define sCONFIG_FRAMEMEM_MT 1
#else
    #ifdef sCONFIG_SYSTEM_IOS
        #define sCONFIG_SYSTEM_IOS 1
        #define sPLATFORM sPLAT_IOS
    #else
        #define sCONFIG_SYSTEM_LINUX 1
        #define sPLATFORM sPLAT_LINUX
        #define sCONFIG_FRAMEMEM_MT 1
    #endif
#endif
//-----------------------------------------------------------------------

//-----------------------------------------------------------------------
// platform dependent settings
#if sCONFIG_SYSTEM_WINDOWS
    #define _INC_MALLOC
    #include "xmmintrin.h"
    #include "emmintrin.h"
    #undef _INC_MALLOC
    #define sCONFIG_SSE               __m128
    #define sSTDCALL                  __stdcall
#endif

#if sCONFIG_SYSTEM_LINUX
    #define sSTDCALL

    #ifndef __SSE__
        #define sCONFIG_SSE               void // mainly for initial build
    #else
        #include "xmmintrin.h"
        #define sCONFIG_SSE               __m128
    #endif
#endif

#define sCONFIG_LE 1
#define sCONFIG_BE 0
#define sCONFIG_UNALIGNEDCRASH 0

#if sCONFIG_SYSTEM_IOS
    #define sSTDCALL
    #define sCONFIG_SSE               void
    #define sCONFIG_GUID              { 0 }
#endif
//-----------------------------------------------------------------------

//-----------------------------------------------------------------------
// include dependant compiler
#ifdef _MSC_VER                                 // microsoft C++
    #include <math.h>
    #include <stdlib.h>
    #include <string.h>
    #define sCONFIG_COMPILER_MSC      1         // compiled with Microsoft Visuacl C++
    #define sCONFIG_COMPILER_GCC      0         // compiled with Gnu C++
    #define sCONFIG_COMPILER_CWCC     0         // compiled with CodeWarrior C++
    #define sCONFIG_COMPILER_SNC      0         // compiled with SN Systems C++
    #define sCONFIG_COMPILER_ARM      0         // compiled with ARM RVCT (3ds)
#endif

#if (defined(__GNUC__) || defined(__GCC__)) && !defined(__SNC__) && !defined(NN_COMPILER_RVCT) // GNU C++
    #include <stddef.h>
    #include <stdlib.h>
    #define sCONFIG_COMPILER_MSC      0
    #define sCONFIG_COMPILER_GCC      1
    #define sCONFIG_COMPILER_CWCC     0
    #define sCONFIG_COMPILER_SNC      0
    #define sCONFIG_COMPILER_ARM      0
#endif

#if sCONFIG_COMPILER_GCC
    #include <math.h>
#endif
//-----------------------------------------------------------------------


//#ifdef nDEFINE_DEBUG
    #undef NDEBUG
    #include <assert.h>
    #define sVERIFY(expr) assert(expr)
    #define sVERIFYFALSE assert(0)
    //#define sVERIFY2(x,desc) { (void)sizeof(x); }
//#else
//    #define sVERIFY(expr)
//#endif

//#define sVERIFY(x) {(void)sizeof(x); }
//#define sVERIFY2(x,desc) { (void)sizeof(x); }
//#define sVERIFYFALSE {;}


/****************************************************************************/
/****************************************************************************/

namespace wz4 {

#ifdef _MSC_VER                             // microsoft C++

// configuration switching

#define sCONFIG_COMPILER_MSC      1         // compiled with Microsoft Visuacl C++
#define sCONFIG_COMPILER_GCC      0         // compiled with Gnu C++
#define sCONFIG_COMPILER_CWCC     0         // compiled with CodeWarrior C++
#define sCONFIG_COMPILER_SNC      0         // compiled with SN Systems C++
#define sCONFIG_COMPILER_ARM      0         // compiled with ARM RVCT (3ds)
#define sCONFIG_32BIT             1         // Pointers are 32 bit
#define sCONFIG_64BIT             0         // Pointers are 64 bit
#define sCONFIG_ASM_MSC_IA32      1         // use 80386 instruction set with MSC syntax
#define sCONFIG_ASM_87            1         // when using IA32, use 8087 fpu
#define sCONFIG_ASM_SSE           0         // when using IA32, use SSE fpu

// type switching

#define sCONFIG_NATIVEINT         int _w64                // sDInt: an int of the same size as a pointer
#define sCONFIG_INT64             __int64                 // sS64, sU64: a 64 bit int
#define sCONFIG_UNICODETYPE       wchar_t                 // sChar: an unicode character
#define sCONFIG_SIZET             size_t
#define sINLINE                   __forceinline           // use this to inline
#define sNOINLINE                 __declspec(noinline)    // forbid inlining. usefull to hint compiler errors
#define sNORETURN                 __declspec(noreturn)    // use this for dead end funtions
#define sDEBUGBREAK               __debugbreak();
#define sCOMPILEERROR(x)          y y y y x
#define sCDECL                    __cdecl
#define sOBSOLETE                 __declspec(deprecated)
#define sALIGNED(type, name, al)  __declspec( align (al) ) type name
#define sRESTRICT                 __restrict
#define sUNUSED                                           // possibly unused variable
#define sOVERRIDE                 override
#define sALIGNOF                  __alignof

// compiler dependent settings

#pragma warning (disable : 4244)
//void __debugbreak();
#pragma intrinsic (__debugbreak)
// variations

#if sCONFIG_OPTION_XSI
#undef sCONFIG_UNICODETYPE
#define sCONFIG_UNICODETYPE       unsigned short
#endif

#ifdef _WIN64
#undef sCONFIG_NATIVEINT
#undef sCONFIG_INT64
#undef sCONFIG_SIZET
#undef sCONFIG_32BIT
#undef sCONFIG_64BIT
#undef sCONFIG_ASM_MSC_IA32
#undef sCONFIG_ASM_87
#undef sCONFIG_ASM_SSE
#define sCONFIG_NATIVEINT         __int64 _w64
#define sCONFIG_INT64             __int64 _w64
#define sCONFIG_SIZET             unsigned __int64
#define sCONFIG_32BIT             0
#define sCONFIG_64BIT             1
#define sCONFIG_ASM_MSC_IA32      0
#define sCONFIG_ASM_87            0
#define sCONFIG_ASM_SSE           0
#endif

#define sCONFIG_ALLOCA(a)         _alloca(a)
extern "C" void *_alloca(sCONFIG_SIZET size);
#pragma intrinsic (_alloca)

// done

#endif

//-----------------------------------------------------------------------
#if (defined(__GNUC__) || defined(__GCC__)) && !defined(__SNC__) && !defined(NN_COMPILER_RVCT) // GNU C++

// configuration switching

#define sCONFIG_COMPILER_MSC      0
#define sCONFIG_COMPILER_GCC      1
#define sCONFIG_COMPILER_CWCC     0
#define sCONFIG_COMPILER_SNC      0
#define sCONFIG_COMPILER_ARM      0

#ifndef __LP64__

#define sCONFIG_32BIT             1
#define sCONFIG_64BIT             0
#define sCONFIG_NATIVEINT         int
#define sCONFIG_PTHREAD_T_SIZE    (sizeof(sU32))

#else

#define sCONFIG_32BIT             0
#define sCONFIG_64BIT             1
#define sCONFIG_NATIVEINT         long long
#define sCONFIG_PTHREAD_T_SIZE    (sizeof(sU64))

#endif

// type switching

#define sCONFIG_INT64             long long
#define sCONFIG_UNICODETYPE       wchar_t
#define sCONFIG_SIZET             size_t
#define sINLINE                   inline
#define sNOINLINE
#define sNORETURN
#define sDEBUGBREAK               {};//__as__("int $3");
#define sCOMPILEERROR(x)          y y y y x
#define sCDECL
#define sOBSOLETE                  __attribute__ ((deprecated))
//#define sOBSOLETE
#define sALIGNED(type, name, al)  type name __attribute__ ((aligned (al)))
#define sRESTRICT                 __restrict
#define sCONFIG_ALLOCA(a)         __builtin_alloca(a)
#define sUNUSED                   __attribute__((unused))    // possibly unused variable
#define sOVERRIDE
#define sALIGNOF                  __alignof

template<unsigned int LINE> class Unreachable_At_Line {};
#define __builtin_unreachable() throw Unreachable_At_Line<__LINE__>()
#define __assume(cond) do { if (!(cond)) __builtin_unreachable(); } while (0)

// compiler dependent settings

void __debugbreak();


// done

#endif
//-----------------------------------------------------------------------





/****************************************************************************/
/***                                                                      ***/
/***   Basic Types and Functions                                          ***/
/***                                                                      ***/
/****************************************************************************/

typedef unsigned char             sU8;      // for packed arrays
typedef unsigned short            sU16;     // for packed arrays
typedef unsigned int              sU32;     // for packed arrays and bitfields
typedef unsigned sCONFIG_INT64    sU64;     // use as needed
typedef signed char               sS8;      // for packed arrays
typedef short                     sS16;     // for packed arrays
typedef int                       sS32;     // for packed arrays
typedef signed sCONFIG_INT64      sS64;     // use as needed
typedef float                     sF32;     // basic floatingpoint
typedef double                    sF64;     // use as needed
typedef int                       sInt;     // use this most!
typedef char                      sChar;    // type for strings
typedef wchar_t                   sWChar;   // type for unicode strings
typedef void*                     sPtr;     // not so usefull, don't use
typedef const void*               sCPtr;    // not so usefull, don't use
typedef signed sCONFIG_NATIVEINT  sDInt;    // type for pointer diff
typedef int                       sBool;    // use for boolean function results




#if sCONFIG_COMPILER_MSC

//#include <math.h>

extern "C"
{
  int __cdecl abs(int);
  void * __cdecl memset( void *dest, int c, sCONFIG_SIZET count );
  //void * __cdecl memcpy( void *dest, const void *src, sCONFIG_SIZET count );
  int __cdecl memcmp( const void *buf1, const void *buf2, sCONFIG_SIZET count );
  sCONFIG_SIZET __cdecl strlen( const char *string );
  __int64 __cdecl __emul(int a,int b);
  unsigned __int64 __cdecl __emulu(unsigned int a,unsigned int b);
}



#pragma intrinsic (abs,__emul,__emulu)                        // int intrinsic
#pragma intrinsic (memset,memcpy,memcmp,strlen)               // memory intrinsic
#pragma intrinsic (fabs)                                      // float intrinsic

sINLINE sInt sAbs(sInt i)                                  { return abs(i); }
sINLINE void sSetMem(void *dd,sInt s,sInt c)               { memset(dd,s,c); }
sINLINE void sCopyMem(void *dd,const void *ss,sInt c)      { memcpy(dd,ss,c); }
sINLINE sInt sCmpMem(const void *dd,const void *ss,sInt c) { return (sInt)memcmp(dd,ss,c); }
template <class Type> void sClear(Type &p) { sSetMem(&p,0,sizeof(p)); }

sINLINE sInt sMulDiv(sInt a,sInt b,sInt c)      { return sInt(__emul(a,b)/c); }
sINLINE sU32 sMulDivU(sU32 a,sU32 b,sU32 c)     { return sU32(__emulu(a,b)/c); }
sINLINE sInt sMulShift(sInt a,sInt b)           { return sInt(__emul(a,b)>>16); }
sINLINE sU32 sMulShiftU(sU32 a,sU32 b)           { return sU32(__emulu(a,b)>>16); }

sINLINE sInt sDivShift(sInt a,sInt b)           { return sInt((sS64(a)<<16)/b); }
sINLINE sF32 sAbs(sF32 f)                       { return fabs(f); }

#define sHASINTRINSIC_ABSI
#define sHASINTRINSIC_SETMEM
#define sHASINTRINSIC_COPYMEM
#define sHASINTRINSIC_CMPMEM
#define sHASINTRINSIC_MULDIV
#define sHASINTRINSIC_MULDIVU
#define sHASINTRINSIC_MULSHIFT
#define sHASINTRINSIC_MULSHIFTU
#define sHASINTRINSIC_DIVSHIFT
#define sHASINTRINSIC_ABSF

#pragma intrinsic (atan,atan2,cos,exp,log,log10,sin,sqrt,tan)

sINLINE sF32 sATan(sF32 f)                      { return (sF32)atan(f); }
sINLINE sF32 sATan2(sF32 over,sF32 under)       { return (sF32)atan2(over,under); }
sINLINE sF32 sCos(sF32 f)                       { return (sF32)cos(f); }
sINLINE sF32 sExp(sF32 f)                       { return (sF32)exp(f); }
sINLINE sF32 sLog(sF32 f)                       { return (sF32)log(f); }
sINLINE sF32 sLog10(sF32 f)                     { return (sF32)log10(f); }
sINLINE sF32 sSin(sF32 f)                       { return (sF32)sin(f); }
sINLINE sF32 sSqrt(sF32 f)                      { return (sF32)sqrtf(f); }
sINLINE sF32 sRSqrt(sF32 f)                     { return 1.0f/(sF32)sqrt(f); }
sINLINE sF32 sTan(sF32 f)                       { return (sF32)tan(f); }

sINLINE sF32 sFATan(sF32 f)                      { return (sF32)atan(f); }
sINLINE sF32 sFATan2(sF32 over,sF32 under)       { return (sF32)atan2(over,under); }
sINLINE sF32 sFCos(sF32 f)                       { return (sF32)cos(f); }
sINLINE sF32 sFExp(sF32 f)                       { return (sF32)exp(f); }
sINLINE sF32 sFLog(sF32 f)                       { return (sF32)log(f); }
sINLINE sF32 sFLog10(sF32 f)                     { return (sF32)log10(f); }
sINLINE sF32 sFSin(sF32 f)                       { return (sF32)sin(f); }
sINLINE sF32 sFTan(sF32 f)                       { return (sF32)tan(f); }

// Intel SSE seems to be too unpreceise which leads to jittering errors in animation code.
#if sRELEASE && 0
//sINLINE sF32 sFSqrt(sF32 f)                      { sF32 x; _m_store_ss(&x,_m_sqrt_ss(_m_load_ss(&f))); return x; }
//sINLINE sF32 sFRSqrt(sF32 f)                     { sF32 x; _m_store_ss(&x,_m_rsqrt_ss(_m_load_ss(&f))); return x; }
#else
// VS2005 has a bug where SSE intrinsics break debugging now and then. therefore...
sINLINE sF32 sFSqrt(sF32 f)                      { return (sF32)sqrtf(f); }
sINLINE sF32 sFRSqrt(sF32 f)                     { return 1.0f/(sF32)sqrt(f); }
#endif

#define sHASINTRINSIC_ATAN
#define sHASINTRINSIC_ATAN2
#define sHASINTRINSIC_COS
#define sHASINTRINSIC_EXP
#define sHASINTRINSIC_LOG
#define sHASINTRINSIC_LOG10
#define sHASINTRINSIC_SIN
#define sHASINTRINSIC_SQRT
#define sHASINTRINSIC_RSQRT
#define sHASINTRINSIC_TAN

#define sHASINTRINSIC_FATAN
#define sHASINTRINSIC_FATAN2
#define sHASINTRINSIC_FCOS
#define sHASINTRINSIC_FEXP
#define sHASINTRINSIC_FLOG
#define sHASINTRINSIC_FLOG10
#define sHASINTRINSIC_FSIN
#define sHASINTRINSIC_FSQRT
#define sHASINTRINSIC_FRSQRT
#define sHASINTRINSIC_FTAN

#endif    // compiler



#if sCONFIG_COMPILER_GCC

sINLINE sInt sAbs(sInt i)                                   { return __builtin_abs(i); }
sINLINE void sSetMem(void *dd,sInt s,sInt c)                { __builtin_memset(dd,s,c); }
sINLINE void sCopyMem(void *dd,const void *ss,sInt c)       { __builtin_memcpy(dd,ss,c); }
sINLINE sInt sCmpMem(const void *aa,const void *bb,sInt c)  { return __builtin_memcmp(aa,bb,c); }
sINLINE sInt sMulDiv(sInt a,sInt b,sInt c)                  { return sS64(a)*b/c; }
sINLINE sU32 sMulDivU(sU32 a,sU32 b,sU32 c)                 { return sU64(a)*b/c; }
sINLINE sInt sMulShift(sInt a,sInt b)                       { return (sS64(a)*b)>>16; }
sINLINE sU32 sMulShiftU(sU32 a,sU32 b)                      { return (sU64(a)*b)>>16; }
sINLINE sInt sDivShift(sInt a,sInt b)                       { return (sS64(a)<<16)/b; }
sINLINE sF32 sAbs(sF32 f)                                   { return __builtin_fabsf(f); }

#define sHASINTRINSIC_ABSI
#define sHASINTRINSIC_SETMEM
#define sHASINTRINSIC_COPYMEM
#define sHASINTRINSIC_CMPMEM
#define sHASINTRINSIC_MULDIV
#define sHASINTRINSIC_MULDIVU
#define sHASINTRINSIC_MULSHIFT
#define sHASINTRINSIC_MULSHIFTU
#define sHASINTRINSIC_DIVSHIFT
#define sHASINTRINSIC_ABSF

sINLINE sF32 sCos(sF32 f)                       { return __builtin_cosf(f); }
sINLINE sF32 sSin(sF32 f)                       { return __builtin_sinf(f); }
sINLINE sF32 sSqrt(sF32 f)                      { return __builtin_sqrtf(f); }
sINLINE sF32 sFSqrt(sF32 f)                     { return __builtin_sqrtf(f); }
sINLINE sF32 sRSqrt(sF32 f)                     { return 1.0f/__builtin_sqrtf(f); }
sINLINE sF32 sFRSqrt(sF32 f)                    { return 1.0f/__builtin_sqrtf(f); }

#define sHASINTRINSIC_COS
#define sHASINTRINSIC_SIN
#define sHASINTRINSIC_SQRT
#define sHASINTRINSIC_FSQRT
#define sHASINTRINSIC_RSQRT
#define sHASINTRINSIC_FRSQRT

sINLINE sF32 sLog(sF32 x)                       { return __builtin_logf(x); }
sINLINE sF32 sExp(sF32 x)                       { return __builtin_expf(x); }

sINLINE sF32 sFLog(sF32 x)                      { return __builtin_logf(x); }
sINLINE sF32 sFExp(sF32 x)                      { return __builtin_expf(x); }

#define sHASINTRINSIC_LOG
#define sHASINTRINSIC_EXP

#define sHASINTRINSIC_FLOG
#define sHASINTRINSIC_FEXP

#endif

#define sTRUE   (!0)
#define sFALSE  0
#define sNULL   0
#define sPI     3.1415926535897932384626433832795
#define sPI2    6.28318530717958647692528676655901
#define sPIF    3.1415926535897932384626433832795f
#define sPI2F   6.28318530717958647692528676655901f
#define sSQRT2  1.4142135623730950488016887242097
#define sSQRT2F 1.4142135623730950488016887242097f

// degrees to radians conversion
#define sRAD2DEG(x) ((x)*180.0f/sPIF)
#define sDEG2RAD(x) ((x)*sPIF/180.0f)

/****************************************************************************/

template <class Type> sINLINE Type sMin(Type a,Type b)              {return (a<b) ? a : b;}
template <class Type> sINLINE Type sMax(Type a,Type b)              {return (a>b) ? a : b;}
template <class Type> sINLINE Type sMin3(Type a,Type b,Type c)      {return sMin(sMin(a,b),c);}
template <class Type> sINLINE Type sMax3(Type a,Type b,Type c)      {return sMax(sMax(a,b),c);}
template <class Type> sINLINE Type sSign(Type a)                    {return (a==0) ? Type(0) : (a>0) ? Type(1) : Type(-1);}
template <class Type> sINLINE Type sCompare(Type a,Type b)          {return (a>b) ? 1 : ((a==b) ? 0 : -1); }
template <class Type> sINLINE Type sClamp(Type a,Type min,Type max) {return (a>=max) ? max : (a<=min) ? min : a;}
//template <class Type> sINLINE sBool sIsInRangeIncl(Type a,Type min,Type max) {return (min<=a) && (a<=max);}
//template <class Type> sINLINE sBool sIsInRangeExcl(Type a,Type min,Type max) {return (min<=a) && (a<max);}
template <class Type> sINLINE void sSwap(Type &a,Type &b)           {Type s=a; a=b; b=s;}
//template <class Type> sINLINE Type sAlign(Type a,sInt b)            {typedef sInt check[(sizeof(Type)<=sizeof(sDInt))?1:-1]; return (Type)((((sDInt)a)+b-1)&(~(b-1)));} // doesn't work correctly if sizeof(sDInt)<sizeof(Type)
sINLINE sS64 sAlign(sS64 a,sInt b)                                  {return ((a+b-1)&(~(b-1)));}
sINLINE sU64 sAlign(sU64 a,sInt b)                                  {return ((a+b-1)&(~(b-1)));}


template <class Type> sINLINE Type sSquare(Type a)                  {return a*a;}
sInt sFindLowerPower(sInt x);
sInt sFindHigherPower(sInt x);
sU32 sMakeMask(sU32 max);
sU64 sMakeMask(sU64 max);
inline sBool sIsPower2(sInt x)                                      {return (x&(x-1)) == 0;}
inline sInt sDivDown(sInt a,sInt b)                                 {return a>0?a/b:-(-a/b);}

template <class Type> sINLINE void sDelete(Type &a)                 {if(a) delete a; a=0;}
template <class Type> sINLINE void sDeleteArray(Type &a)            {if(a) delete[] a; a=0;}
template <class Type> sINLINE void sRelease(Type &a)                {if(a) a->Release(); a=0;}
template <class Type> sINLINE void sAddRef(Type &a)                 {if(a) a->AddRef();}

template <class Type> sINLINE Type sFade(sF32 f,Type a,Type b)      {return (Type)(a+(b-a)*f);}
template <class Type> sINLINE Type sFadeBilin(sF32 u,sF32 v,Type x00,Type x01,Type x10,Type x11) { return sFade(v,sFade(u,x00,x01),sFade(u,x10,x11)); }



/****************************************************************************/
/***                                                                      ***/
/***   Arithmetic Functions                                               ***/
/***                                                                      ***/
/****************************************************************************/

// int

sInt sMulDiv(sInt a,sInt b,sInt c);
sU32 sMulDivU(sU32 a,sU32 b,sU32 c);
sInt sMulShift(sInt var_a,sInt var_b);
sU32 sMulShiftU(sU32 var_a,sU32 var_b);
sInt sDivShift(sInt var_a,sInt var_b);
//sInt sDivMod(sInt over,sInt under,sInt &div,sInt &mod); // |over| = div*under+mod

// float fiddling

sF32 sAbs(sF32);                            // specialization for float
sF32 sMinF(sF32,sF32);                      // specialization for float
sF32 sMaxF(sF32,sF32);                      // specialization for float
void sDivMod(sF32 over,sF32 under,sF32 &div,sF32 &mod); // |over| = div*under+mod
sF32 sMod(sF32 over,sF32 under);
sF32 sAbsMod(sF32 over,sF32 under);         // values below zero will be modded to 0>=x<under as well.
sInt sAbsMod(sInt over,sInt under);         // values below zero will be modded to 0>=x<under as well.
void sSetFloat();                           // set floating point unit into default precision/rounding
void sFrac(sF32 val,sF32 &frac,sF32 &full); // val = frac+full; 0>=frac>1; full is int
void sFrac(sF32 val,sF32 &frac,sInt &full);
sF32 sFrac(sF32);
sF32 sRoundDown(sF32);                      // round towards minus infinity
sF32 sRoundUp(sF32);                        // round towards plus infinity
sF32 sRoundZero(sF32);                      // round towards zero
sF32 sRoundNear(sF32);                      // round towards nearest integer, in case of tie round to nearest even integer
sInt sRoundDowint(sF32);
sInt sRoundUpInt(sF32);
sInt sRoundZeroInt(sF32);
sInt sRoundNearInt(sF32);
sF32 sSelectEQ(sF32 a,sF32 b,sF32 t,sF32 f);// if (a==b) then x=t else x=f;
sF32 sSelectNE(sF32 a,sF32 b,sF32 t,sF32 f);// if (a!=b) then x=t else x=f;
sF32 sSelectGT(sF32 a,sF32 b,sF32 t,sF32 f);// if (a>=b) then x=t else x=f;
sF32 sSelectGE(sF32 a,sF32 b,sF32 t,sF32 f);// if (a> b) then x=t else x=f;
sF32 sSelectLT(sF32 a,sF32 b,sF32 t,sF32 f);// if (a<=b) then x=t else x=f;
sF32 sSelectLE(sF32 a,sF32 b,sF32 t,sF32 f);// if (a< b) then x=t else x=f;
sF32 sSelectEQ(sF32 a,sF32 b,sF32 t);       // if (a==b) then x=t else x=0;
sF32 sSelectNE(sF32 a,sF32 b,sF32 t);       // if (a!=b) then x=t else x=0;
sF32 sSelectGT(sF32 a,sF32 b,sF32 t);       // if (a>=b) then x=t else x=0;
sF32 sSelectGE(sF32 a,sF32 b,sF32 t);       // if (a> b) then x=t else x=0;
sF32 sSelectLT(sF32 a,sF32 b,sF32 t);       // if (a<=b) then x=t else x=0;
sF32 sSelectLE(sF32 a,sF32 b,sF32 t);       // if (a< b) then x=t else x=0;

// non-trivial float

sF32 sSqrt(sF32);
sF32 sRSqrt(sF32);
sF32 sLog(sF32);
sF32 sLog2(sF32);
sF32 sLog10(sF32);
sF32 sExp(sF32);
sF32 sPow(sF32,sF32);

sF32 sSin(sF32);
sF32 sCos(sF32);
sF32 sTan(sF32);
void sSinCos(sF32,sF32 &s,sF32 &c);
sF32 sASin(sF32);
sF32 sACos(sF32);
sF32 sATan(sF32);
sF32 sATan2(sF32 over,sF32 under);

// non-trivial float, fast

sF32 sFSqrt(sF32);
sF32 sFRSqrt(sF32);
sF32 sFLog(sF32);
sF32 sFLog2(sF32);
sF32 sFLog10(sF32);
sF32 sFExp(sF32);
sF32 sFPow(sF32,sF32);

sF32 sFSin(sF32);
sF32 sFCos(sF32);
sF32 sFTan(sF32);
void sFSinCos(sF32,sF32 &s,sF32 &c);
sF32 sFASin(sF32);
sF32 sFACos(sF32);
sF32 sFATan(sF32);
sF32 sFATan2(sF32 over,sF32 under);


sINLINE sF32 /*sOBSOLETE*/ sFMod(sF32 a,sF32 b)     { return sMod(a,b); }
sINLINE sF32 /*sOBSOLETE*/ sFFloor(sF32 a)          { return sRoundDown(a); }
sINLINE sF32 /*sOBSOLETE*/ sFCeil(sF32 a)           { return sRoundUp(a); }
sINLINE sF32 /*sOBSOLETE*/ sFAbs(sF32 a)            { return sAbs(a); }
sINLINE sF32 /*sOBSOLETE*/ sFInvSqrt(sF32 a)        { return sRSqrt(a); }


#if sCONFIG_COMPILER_GCC
  #if __GNUC__ >= 4
    #define sOFFSET(TYPE, MEMBER) __builtin_offsetof(TYPE,MEMBER)
  #else
    #define sOFFSET(TYPE, MEMBER)					\
    (__offsetof__ (reinterpret_cast <sDInt>			\
                 (&reinterpret_cast <const volatile char &>	\
                  (static_cast<TYPE *> (0)->MEMBER))))
  #endif
#else
  #define sOFFSET(t,m) (((sDInt)(&((t*)64)->m))-64)
#endif

#define sOFFSETO(o,m) ((sDInt)(((sU8 *)(&(o)->m))-((sU8 *)(o))))    // offset from object
#define sOFFSETOMP(o,m) ((sDInt)(((sU8 *)(&((o)->*(m))))-((sU8 *)(o))))  // offset from object, member pointer

#if sCONFIG_COMPILER_CWCC
#define sVERIFYSTATIC(x) { bool STATIC_ASSERTION_[(x)?1:-1]; }
#elif sCONFIG_COMPILER_SNC
#define sVERIFYSTATIC(x) {  }
#else
#define sVERIFYSTATIC(x) { struct STATIC_ASSERTION {bool STATIC_ASSERTION_[(x)?1:-1]; }; }
#endif

#define sCOUNTOF(x) sDInt(sizeof(x)/sizeof(*(x)))    // #elements of array, usefull for unicode strings

//-----------------------------------------------------------------------------------------
// Linear congruential generator (mul add)
//-----------------------------------------------------------------------------------------
class sRandom
{
private:
  sU32 kern;
  sU32 Step();
public:
  sRandom() { Seed(0); }
  sRandom(sU32 seed) { Seed(seed); }
  void Init() { Seed(0); }
  void Seed(sU32 seed);
  sInt Int(sInt max);
  sInt Int16() { return Step()&0xffff; }
  sU32 Int32();
  sF32 Float(sF32 max);
  inline sF32 FloatSigned(sF32 max) { return Float(2.0f*max)-max; }
};

//-----------------------------------------------------------------------------------------
//  Mersenne twister (pretty good and fast)
//-----------------------------------------------------------------------------------------
class sRandomMT
{
private:
  enum Enums
  {
    Count = 624,
    Period = 397,
  };
  sU32 State[Count];
  sInt Index;

  sU32 Step();
  void Reload();
public:
  sRandomMT() { Seed(0); }
  sRandomMT(sU32 seed) { Seed(seed); }
  void Init() { Seed(0); }
  void Seed(sU32 seed);
  sInt Int(sInt max);
  sInt Int16() { return Step()&0xffff; }
  sU32 Int32() { return Step(); }
  sF32 Float(sF32 max);
  inline sF32 FloatSigned(sF32 max) { return Float(2.0f*max)-max; }
};


sInt sCmpString(const sChar *a,const sChar *b);



uint sColorFade (uint a, uint b, float f); // fade=0.0f..1.0f



} // end namespace wz4


#endif // WZTYPES_H
