#ifndef WZ4COMMON_HPP
#define WZ4COMMON_HPP


#include <string>

#include "wz4Types.hpp"


namespace wz4 {



struct sVertexFormatHandle
{

};






enum Wz4MtrlFlags
{
    sRF_TARGET_MAIN     = 0x0001,   // render to screen
    sRF_TARGET_ZONLY    = 0x0002,   // render to depth texture (through red)
    sRF_TARGET_ZNORMAL  = 0x0003,   // render to depth texture (through red)
    sRF_TARGET_WIRE     = 0x0004,   // wireframe
    sRF_TARGET_DIST     = 0x0005,   // render distance to depth texture (through red)
    sRF_TARGET_MASK     = 0x0007,

    sRF_MATRIX_ONE      = 0x0008,   // normal painting
    sRF_MATRIX_BONE     = 0x0010,   // provide weight & index in geometriy stream[0]
    // set vertex constants manually before call
    sRF_MATRIX_INSTANCE = 0x0018,   // provide matrices in geometry stream[1]
    sRF_MATRIX_BONEINST = 0x0020,   // bone + instances. for swarms of animated objects
    sRF_MATRIX_INSTPLUS = 0x0028,   // provide matrices + sVF_F4 vector in geometry stream[1]
    sRF_MATRIX_MASK     = 0x0038,

    sRF_TOTAL           = 0x003f,

    sRF_HINT_DIRSHADOW  = 0x0100,   // directional shadow map: used by mandelbulb culling
    sRF_HINT_POINTSHADOW= 0x0200,   // pointlight shadow map
};

/****************************************************************************/
/***                                                                      ***/
/***   Vertex Formats                                                     ***/
/***                                                                      ***/
/****************************************************************************/

enum sVertexFlags                 // experimental way of specifying vertex formats
{                                 // (x) <- minimum number of dimensions
    sVF_POSITION      = 0x0001,     // (3) position (always set)
    sVF_NORMAL        = 0x0002,     // (3) normal
    sVF_TANGENT       = 0x0003,     // (3) tangent
    sVF_BONEINDEX     = 0x0004,     // (4) 4 indices to matrices
    sVF_BONEWEIGHT    = 0x0005,     // (4) 4 weighting factors to matrices
    sVF_BINORMAL      = 0x0006,     // (3) binormal, for certain consoles only, do not use!

    sVF_COLOR0        = 0x0008,     // (4) color
    sVF_COLOR1        = 0x0009,     // (4) color
    sVF_COLOR2        = 0x000a,     // (4) color
    sVF_COLOR3        = 0x000b,     // (4) color
    sVF_UV0           = 0x000c,     // (2) texture uv's
    sVF_UV1           = 0x000d,     // (2) texture uv's
    sVF_UV2           = 0x000e,     // (2) texture uv's
    sVF_UV3           = 0x000f,     // (2) texture uv's
    sVF_UV4           = 0x0010,     // (2) texture uv's
    sVF_UV5           = 0x0011,     // (2) texture uv's
    sVF_UV6           = 0x0012,     // (2) texture uv's
    sVF_UV7           = 0x0013,     // (2) texture uv's
    sVF_NOP           = 0x001f,

    sVF_USEMASK       = 0x001f,     // these flags are also stored in a bitmask, so it's limited to 32.

    sVF_STREAM0       = 0x0000,     // stream descriptor
    sVF_STREAM1       = 0x0040,
    sVF_STREAM2       = 0x0080,
    sVF_STREAM3       = 0x00c0,
    sVF_STREAMMASK    = 0x00c0,     // reserve for 4 streams
    sVF_STREAMMAX     = 4,          // max number of streams
    sVF_STREAMSHIFT   = 6,          // shift to extract sream

    // optional: specify datatype. if missing, defaults are provided.
    // always available
    sVF_F2            = 0x0100,     // 2 x sF32
    sVF_F3            = 0x0200,     // 3 x sF32
    sVF_F4            = 0x0300,     // 4 x sF32
    sVF_C4            = 0x0400,     // 4 x sU8, scaled to 0..1 range
    // sometimes available
    sVF_I4            = 0x0500,     // 4 x sU8, not scaled. for bone-indices
    sVF_S2            = 0x0600,     // 2 x sS16, scaled to -1..1 range
    sVF_S4            = 0x0700,     // 4 x sS16, scaled to -1..1 range
    sVF_H2            = 0x0800,     // 2 x sF16
    sVF_H4            = 0x0900,     // 4 x sF16
    sVF_F1            = 0x0a00,     // 1 x sF32

    sVF_H3            = 0x0b00,     // 3 x sF16 (for optimized 3-vectors)
    //                  0x0c00,     // unused
    sVF_SN_11_11_10   = 0x0d00,     // Normalized, 3D signed 11 11 10 format expanded to (value/1023.0, value/1023.0, value/511.0, 1)

    sVF_TYPEMASK      = 0xff00,

    sVF_INSTANCEDATA  = 0x00010000, // DX11 needs a marker for instanced streams

    sVF_END           = 0,          // endmarker.
};


enum sGeomtryFlags                // flags for initializing geometries
{
    sGF_TRILIST           = 0x1000, // geometry types
    sGF_TRISTRIP          = 0x2000,
    sGF_LINELIST          = 0x3000,
    sGF_LINESTRIP         = 0x4000,
    sGF_QUADLIST          = 0x5000,
    sGF_SPRITELIST        = 0x6000,
    sGF_LINELISTADJ       = 0x7000, // .. with adjacency
    sGF_TRILISTADJ        = 0x8000,
    sGF_PATCHLIST         = 0x9000, // .. for tesselation
    sGF_QUADRICS          = 0xf000, // .. special primitive mode (quad/grid)
    sGF_PRIMMASK          = 0xf000,

    sGF_PATCHMASK         = 0x003f, // patchlist count, 1..32

    sGF_INDEXOFF      = 0x00000000, // no index buffer (avoid)
    sGF_INDEX32       = 0x00010000, // use 32 bit index buffers (avoid)
    sGF_INDEX16       = 0x00020000, // use 16 bit index buffers (do!)
    sGF_INDEXMASK     = 0x00030000, // mask index buffer bits

    sGF_INSTANCES     = 0x00040000, // use instancing API
    sGF_CPU_MEM       = 0x00080000, // use cache cpu mem for vertex and index buffers
};


enum sGeometryDuration            // state of a buffer
{
    sGD_NONE = 0,                   // uninitialized slot
    sGD_STATIC,                     // forever, unchangable
    sGD_FRAME,                      // flushed after end of frame
    sGD_STREAM,                     // flushed immediately
    sGD_CSBUFFER,                   // written by compute shaders, read by input assembly
    sGD_DYNAMIC,                    // used internally for sGeomentry::InitDyn()
};



enum E_VERTEX_FORMAT
{
    EVF_P,      // nVertex_P
    EVF_pCT,    // nVertex_pCT
    EVF_PC,     // nVertex_PC
    EVF_PCT,     // nVertex_PCT
    EVF_PT,      // nVertex_PT
    EVF_WIRE
};


struct nVertex_P
{
    sF32 px,py,pz;    // 3 pos
};

struct nVertex_pCT
{
    sF32 px,py;       // 2 pos
    sF32 cr,cg,cb;    // 3 color
    sF32 u0,v0;       // 2 uv
};

struct nVertex_PCT
{
    sF32 px,py,pz;    // 3 pos
    sF32 cr,cg,cb;    // 3 color
    sF32 u0,v0;       // 2 uv
};


struct nVertex_PC
{
    sF32 px,py,pz;    // 3 pos
    sF32 cr,cg,cb;    // 3 color
};

struct nVertex_PT
{
    sF32 px,py,pz;    // 3 pos
    sF32 u0,v0;       // 2 uv
};

struct nVertex_WIRE
{
    /*sVector31 Pos;
    sVector30 Normal;
    sU32 Color;*/


    sF32 px,py,pz;
    sF32 nx,ny,nz;


    // sVector31 Pos;
    //sVector30 Normal;
    //sU32 Color;
    sF32 cx,cy,cz;

    //void Init(const sVector31 &pos,sU32 col) { Pos = pos; Normal.Init(0,0,0); Color = col; }
    //void Init(const sVector31 &pos,const sVector30 &norm,sU32 col) { Pos = pos; Normal = norm; Color = col; }*/

};


class sGeometry
{
public:

    virtual ~sGeometry() {}

    sU32            _vertexSize;    // vertex size in bytes
    E_VERTEX_FORMAT _vertexFormat; // vertex element type

    void * _vbo;               // vertex buffer object
    void * _ebo;               // index buffer object

    sCONFIG_SIZET _vboSize;    // sizeof vertex buffer
    sCONFIG_SIZET _eboSize;    // sizeof index buffer

    sU32 _vertexCount;         // vertex count inside vbo
    sU32 _indexCount;          // index count inside ebo

    //sCONFIG_SIZET _vertexSize; // sizeof of each vertex element in vbo
    //sCONFIG_SIZET _indexSize;  // sizeof of each index element in ebo

    sInt _indexType;           // sGF_INDEX16 or sGF_INDEX32
    sInt _flags;               // Init flags


    //virtual void Init(sInt flags,sVertexFormatHandle *) {}
    virtual void Init(sInt flags, E_VERTEX_FORMAT vertexFormat, const sU32* vertexDesc)
    {
        _vertexSize = GetVertexSizeFromDesc(vertexDesc);
        _vertexFormat = vertexFormat;
        _vertexCount = 0U;
        _indexCount = 0U;
        _vbo = sNULL;
        _ebo = sNULL;
        _vboSize = 0;
        _eboSize = 0;
        _flags = flags;

        switch(_flags & sGF_INDEXMASK)
        {
        case sGF_INDEX16:  _indexType = 16; break;
        case sGF_INDEX32:  _indexType = 32; break;
        default: _indexType = 0;
        }
    }


    void BeginLoadIB(sInt ic,sGeometryDuration duration,void **ip)
    {
        switch(_flags & sGF_INDEXMASK)
        {
        case sGF_INDEX16:
        {
            _ebo = new sU16[ic];
            _eboSize = ic * sizeof(sU16);
        }
            break;

        case sGF_INDEX32:
        {
            _ebo = new sU32[ic];
            _eboSize = ic * sizeof(sU32);
        }
            break;

        default:
            // undefined index type
            sVERIFYFALSE;
        }

        *ip = _ebo;
        _indexCount = ic;


        /*_ebo = new sU16[ic]; //malloc(sizeof(sU32)*vc);

        *ip = _ebo;
        _eboSize = ic * sizeof(sU16);

        _indexCount = ic;*/
    }

    virtual void BeginLoadVB(sInt vc,sGeometryDuration duration,void **vp,sInt stream=0)
    {
        _vboSize = _vertexSize * vc;
        _vbo = new sU8[_vboSize];




        /*switch(_vertexFormat)
        {
        case EVF_P:
            _vbo = new nVertex_P[vc];
            _vboSize = sizeof(nVertex_P) * vc;
            break;

        case EVF_pCT:
            _vbo = new nVertex_pCT[vc];
            _vboSize = sizeof(nVertex_pCT) * vc;
            break;

        case EVF_PCT:
            _vbo = new nVertex_PCT[vc];
            _vboSize = sizeof(nVertex_PCT) * vc;
            break;

        case EVF_PC:
            _vbo = new nVertex_PC[vc];
            _vboSize = sizeof(nVertex_PC) * vc;
            break;

        case EVF_PT:
            _vbo = new nVertex_PT[vc];
            _vboSize = sizeof(nVertex_PT) * vc;
            break;

        case EVF_WIRE:
            _vbo = new nVertex_WIRE[vc];
            _vboSize = sizeof(nVertex_WIRE) * vc;
            break;
        }*/


        *vp = _vbo;
        _vertexCount = vc;

    }

    virtual void BeginLoad(sVertexFormatHandle *,sInt flags,sGeometryDuration duration,sInt vc,sInt ic,void **vp,void **ip) {}

    template<typename T> void BeginLoadIB(sInt ic,sGeometryDuration duration,T **ip)
    { void **ptr = (void**)ip; BeginLoadIB(ic,duration,ptr);}
    template<typename T> void BeginLoadVB(sInt vc,sGeometryDuration duration,T **vp,sInt stream=0)
    { void **ptr = (void**)vp; BeginLoadVB(vc,duration,ptr,stream);  }
    template<typename V,typename I> void BeginLoad(sVertexFormatHandle *vh,sInt flags,sGeometryDuration duration,sInt vc,sInt ic,V **vp,I **ip)
    { void **vptr = (void**)vp; void **iptr = (void**)ip; BeginLoad(vh,flags,duration,vc,ic,vptr,iptr);  }

    virtual void EndLoadIB(sInt ic=-1) {}
    virtual void EndLoadVB(sInt vc=-1,sInt stream=0) {}
    virtual void EndLoad(sInt vc=-1,sInt ic=-1) {}


private:

    sU32 GetVertexSizeFromDesc(const sU32* vertexDescription)
    {
        sU32 vs = 0U;

        const sU32 *desc = vertexDescription;
        while(*desc)
        {
            const sU32 desctype = *desc & sVF_TYPEMASK;
            switch(desctype)
            {
            case sVF_F1:
                vs += 1U * sizeof(sF32); break;
            case sVF_F2:
                vs += 2U * sizeof(sF32); break;
            case sVF_F3:
                vs += 3U * sizeof(sF32); break;
            case sVF_F4:
                vs += 4U * sizeof(sF32); break;
            case sVF_C4:
            case sVF_I4:
                vs += 4U * sizeof(sU8); break;
            default:
                sVERIFYFALSE; // unknown vertex format
            }

            desc++;
        }

        return vs;
    }
};













class sWriter
{

};

class sReader
{

};



class wObject
{
protected:
    virtual ~wObject() {}
public:
    wObject() { /*Type = 0;*/ RefCount = 1; CallId = 0; }
    //wType *Type;
    sInt RefCount;

    sInt CallId;


    void AddRef()    { if(this) RefCount++; }
    void Release()   { if(this) { if(--RefCount<=0) delete this; } }
    //sBool IsType(wType *type) { return Type->IsType(type); }   // output->IsType(input). obj type is of type, or type is parent of obj type.
    virtual void Reuse()  { /*sFatal(L"this class can not be used for weak linking.");*/ }
    virtual wObject *Copy()  { return 0; }
};


} // end namespace wz4

#endif // WZ4COMMON_HPP

