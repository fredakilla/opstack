#ifndef WZ4TYPE2_HPP
#define WZ4TYPE2_HPP

#include "wz4Types.hpp"

#include <stdio.h>
#include <stdarg.h>
#include <iostream>

namespace wz4 {

template<typename T>
struct sRemovePtrType { typedef T Type; };
template<typename T>
struct sRemovePtrType<T*> { typedef T Type; };
template<typename T>
struct sRemovePtrType<T**> { typedef T Type; };


/************************************************************************/ /*!

\ingroup altona_base_types_arrays

Array that will not grow automatically.
- call HintSize() to set initial size before using
- You can also use the constructor to set initial size
- You may resize an sStaticArray manually after calling Reset().

*/ /*************************************************************************/

template <class Type> class sStaticArray
{
protected:

    Type *Data;
    sInt Used;
    sInt Alloc;

    // prepare tracking of array-usage
#if TRACK_ARRAY_USAGE
    sInt UsedMaximum;
    void UsageTrackingInit()   { UsedMaximum = Used; }
    void UsageTrackingUpdate() { if (UsedMaximum<Used) UsedMaximum = Used; }
public:
    sInt GetMaximumUsed() const { return UsedMaximum; }
#else
    void UsageTrackingInit()   {}
    void UsageTrackingUpdate() {}
public:
    sInt GetMaximumUsed() const { return -1; }
#endif
protected:

    virtual void ReAlloc(sInt max)          {
        if(max>Alloc)
        {
#if sDEBUG
            if (Data)
                sDPrintF(L"ReAlloc: Alloc=%d, max=%d, sizeof(Type)=%d\n", Alloc, max, sInt(sizeof(Type)));
#endif
            sVERIFY(!Data);
            Data = new Type[max];
            Alloc= Data?max:0;
        }
    }

    virtual void Grow(sInt add)             {
#if sDEBUG
        if(Used+add>Alloc)
            sFatal(L"Overflow! Alloc: %d Used: %d Added: %d\n",Alloc, Used, add);
#endif
        //sVERIFYRELEASE(Used+add<=Alloc);
    }

public:
    typedef Type BaseType;
    typedef typename sRemovePtrType<Type>::Type ElementType;
    typedef sInt Iterator;

    //! Construct and allocate some space. The array will be considered empty.
    explicit sStaticArray(sInt max)       { Data = 0; Used = 0; Alloc = 0; UsageTrackingInit(); /*sTagMemLast();*/ ReAlloc(max); }
    //! Construct with no space allocated
    sStaticArray()                        { Data = 0; Used = 0; Alloc = 0; UsageTrackingInit(); }
    //! Construct and initialize with preallocated space. The array will be considered empty.
    sStaticArray(Type *data, sInt max)    { Data = data; Used = 0; Alloc = max; UsageTrackingInit(); }
    //! Deallocate memory.
    virtual ~sStaticArray()               { delete[] Data;
                                            //// generate a message if memory is wasted
                                            //{
                                            //  sInt mu = GetMaximumUsed();
                                            //  if (mu>0)
                                            //  {
                                            //    sInt wastedBytes = (GetSize()-mu) * sizeof(BaseType);
                                            //    if (wastedBytes>100*1024)
                                            //      sLogF(L"sys", L"sStaticArray destroyed. %d bytes of mem never used.\n", wastedBytes);
                                            //  }
                                            //}
                                          }
    //! Create a copy of all elements.
    void Copy(const sStaticArray &a)      { Resize(a.GetCount()); for(sInt i=0;i<a.GetCount();i++) Data[i] = a.Data[i]; }
    //! Create a copy of all elements.
    sStaticArray(const sStaticArray &a)   { Data = 0; Used = 0; Alloc = 0; UsageTrackingInit(); Copy(a); }
    //! Create a copy of all elements.
    sStaticArray &operator=(const sStaticArray &a)  { if (this != &a) Copy(a); return *this; }
    //! reset deallocates memory and resets container,
    //! - use Clear instead if you want to reuse your container afterwards
    //! - this is the only way to Resize an sStaticArray
    virtual void Reset()                  { sDeleteArray(Data); Used = 0; Alloc = 0;}

    //! return number of elements used
    sInt GetCount() const                 { return Used; }
    //! return number of elements allocated (the potential size of the array)
    sInt GetSize() const                  { return Alloc; }
    //! get pointer to the actual array. Be aware that some classes derived from sStaticArray may change this pointer as the array grows
    Type *GetData() const                 { return Data; }
    //! return true if the used counter is zero
    sBool IsEmpty() const                 { return Used==0; }
    //! return true if potential size is used up. Some classes derived from sStaticArray can grow automatically.
    sBool IsFull() const                  { return Used==Alloc; }
    //! return true if the index provided will index a element.
    sBool IsIndexValid(sInt p) const      { return ((unsigned) p) < ((unsigned)Used); }
    //! Hints the array to allow grow to at least the specified size.
    //! - sStaticArray can only be initialized once and will not grow automatically
    void HintSize(sInt max)               { ReAlloc(max); }
    void HintSize_(sInt max,const char *file,sInt line)  { /*sTagMem(file,line);*/ ReAlloc(max); }

    //! Set the array count. Grow array if required.
    Type *Resize(sInt count)              { if (count>Alloc) ReAlloc(count); Used = count; UsageTrackingUpdate(); return Data; }
    //! Set the array count to 0 without actually deallocating the memory.
    void Clear()                          { Used = 0; }

    //  void Add(const Type &e)               { sVERIFY(&e<Data || &e>=Data+Alloc); Grow(1); Data[Used++]=e; }
    //  Type *Add()                           { Grow(1); return &Data[Used++]; }
    //! Add all elements from another array
    void Add(const sStaticArray<Type> &a) { Grow(a.Used); for(sInt i=0;i<a.Used;i++) Data[Used++]=a.Data[i]; UsageTrackingUpdate(); }
    //! Advance the Used counter, increasing the array count. The returned pointer points to the first of the new elements
    Type *AddMany(sInt count)             { Grow(count); Type *r=Data+Used; Used+=count; UsageTrackingUpdate(); return r; }
    Type *AddManyInit(sInt count, const Type *prototype = sNULL)  { Type *r=AddMany(count);
                                                                    if (prototype) for (sInt i=0;i<count;i++) r[i] = *prototype;
                                                                    else           for (sInt i=0;i<count;i++) r[i] = Type();
                                                                                                                                return r; }
    Type *AddFull()                       { return AddMany(GetSize()-GetCount()); }
    Type *AddFullInit(const Type *prototype = sNULL) { return AddManyInit(GetSize()-GetCount(), prototype); }
    //! Insert element into array, preserving order (requires copying elements around)
    void AddBefore(const Type &e,sInt p)  { sVERIFY(&e<Data || &e>=Data+Alloc); sVERIFY(p>=0 && p<=Used); Grow(1); for(sInt i=Used;i>p;i--) Data[i]=Data[i-1]; Data[p]=e; Used++; UsageTrackingUpdate(); }
    //! Insert element into array, preserving order (requires copying elements around)
    void AddAfter(const Type &e,sInt p)   { sVERIFY(&e<Data || &e>=Data+Alloc); AddBefore(e,p+1); UsageTrackingUpdate(); }
    //! Insert element into array, preserving order (requires copying elements around)
    void AddHead(const Type &e)           { sVERIFY(&e<Data || &e>=Data+Alloc); AddBefore(e,0); UsageTrackingUpdate(); }
    //! Insert element into array, preserving order (which is trivial)
    void AddTail(const Type &e)           { sVERIFY(&e<Data || &e>=Data+Alloc); Grow(1); Data[Used++]=e; UsageTrackingUpdate(); }

    Type &GetTail() const                 { sVERIFY(!IsEmpty()); return Data[Used-1]; }

    //! Removing element, NOT preserving order
    void RemAt(sInt p)                    { sVERIFY(IsIndexValid(p)); Data[p] = Data[--Used]; }
    //! Removing element, preserving order (requires copying elements around)
    void RemAtOrder(sInt p)               { sVERIFY(IsIndexValid(p)); Used--; while(p<Used) {Data[p]=Data[p+1]; p++;} }
    //! Removing last element, preserving order (which is trivial)
    Type RemTail()                        { sVERIFY(Used>0); Used--; return Data[Used]; }
    //! Removing element, NOT preserving order (requires a linear search for the element)
    sBool Rem(const Type e)               { sBool result = sFALSE; for(sInt i=0;i<Used;) if(Data[i]==e) { result = sTRUE; RemAt(i); } else i++; return result; }
    //! Removing element, preserving order (requires a linear search for the element) (requires copying elements around)
    sBool RemOrder(const Type e)          { sBool result = sFALSE; for(sInt i=0;i<Used;) if(Data[i]==e) { result = sTRUE; RemAtOrder(i); } else i++; return result; }

    //! Indexing
    sINLINE const Type &operator[](sInt p) const  { sVERIFY(IsIndexValid(p)); return Data[p]; }
    //! Indexing
    sINLINE Type &operator[](sInt p)              { sVERIFY(IsIndexValid(p)); return Data[p]; }

    //! Swap the whole array with another one. This is quick.
    void Swap(sStaticArray &a)            { sSwap(Data,a.Data); sSwap(Used,a.Used); sSwap(Alloc,a.Alloc); }
    //! Swap two elements of this array
    void Swap(sInt i,sInt j)              { sSwap(Data[i],Data[j]); }

    //! Fisher Yates Shuffle ; works in-place, time complexity: O(n)
    template <class RandomClass> void Shuffle(RandomClass &rnd_gen) { if (Used==0) return; for (sInt i=Used-1; i>0; i--) Swap(rnd_gen.Int(i+1), i); }

    // Don't use IndexOf, use sFindIndex instead!
    sOBSOLETE sInt IndexOf(const Type &e)            { for(sInt i = 0; i < GetCount(); i++) if (Data[i] == e) return i; return -1; }
};





/****************************************************************************/
/***                                                                      ***/
/***   Containers                                                         ***/
/***                                                                      ***/
/****************************************************************************/

/************************************************************************/ /*!
\ingroup altona_base_types_arrays

sArray is an automatically resizing version of sStaticArray.

- You should use HintSize() to set the initial potential size of sArray
  to avoid the overhead of growing.
- to grow the array, you need to call one of the AddXXX() functions, like
  AddTail() or AddMany(). If you index past the current count of the array
  it will assert.

*/ /*************************************************************************/

template <class Type> class sArray : public sStaticArray<Type>
{
    void ReAlloc(sInt max)          { if(max>=this->Used && max!=this->Alloc) { Type *n=new Type[max];
            if (n) { for(sInt i=0;i<this->Used;i++) n[i]=this->Data[i];
                delete[] this->Data; this->Data=n; this->Alloc=max; } } }

public:
    void Grow(sInt add)             { if(this->Used+add>this->Alloc) ReAlloc(sMax(this->Used+add,this->Alloc*2)); }
    void GrowTo(sInt size)             { if(size>this->Alloc) ReAlloc(sMax(size,this->Alloc*2)); }
    void Swap(sArray<Type> &a)      { sStaticArray<Type>::Swap(a); }
    void Swap(sInt i,sInt j)        { sStaticArray<Type>::Swap(i,j); }
};

template <class Type> class sAutoArray : public sArray<Type>
{
public:
    ~sAutoArray<Type>() { sDeleteAll(*this); }
};

// sFixedArray is a static array that preallocates items for its maximum size
template <class Type> class sFixedArray : public sStaticArray<Type>
{
public:
    sFixedArray(sInt count)         { sStaticArray<Type>::HintSize(count); sStaticArray<Type>::AddMany(count); }
};







/****************************************************************************/
/***                                                                      ***/
/***   FORALL macro                                                       ***/
/***                                                                      ***/
/****************************************************************************/

// adapter functions

template <template <typename> class ArrayType,class BaseType>
sINLINE BaseType *sGetPtr(ArrayType<BaseType *> &a,sInt i)
{
    return a[i];
}

template <template <typename> class ArrayType,class BaseType>
sINLINE BaseType *sGetPtr(ArrayType<BaseType> &a,sInt i)
{
    return &a[i];
}

template <template <typename> class ArrayType,class BaseType>
sINLINE const BaseType *sGetPtr(const ArrayType<BaseType *> &a,sInt i)
{
    return a[i];
}

template <template <typename> class ArrayType,class BaseType>
sINLINE const BaseType *sGetPtr(const ArrayType<BaseType> &a,sInt i)
{
    return &a[i];
}

// GCC needs these versions for sStackArray<class type,int max>

template <template <typename,int> class ArrayType,class BaseType,int count>
sINLINE BaseType *sGetPtr(ArrayType<BaseType *,count> &a,sInt i)
{
    return a[i];
}

template <template <typename,int> class ArrayType,class BaseType,int count>
sINLINE BaseType *sGetPtr(ArrayType<BaseType,count> &a,sInt i)
{
    return &a[i];
}

template <template <typename,int> class ArrayType,class BaseType,int count>
sINLINE const BaseType *sGetPtr(const ArrayType<BaseType *,count> &a,sInt i)
{
    return a[i];
}

template <template <typename,int> class ArrayType,class BaseType,int count>
sINLINE const BaseType *sGetPtr(const ArrayType<BaseType,count> &a,sInt i)
{
    return &a[i];
}


// the forall macro itself
/************************************************************************/ /*!

\ingroup altona_base_types_arrays

This macro iterates sStaticArray and it's subclasses

@param l the array
@param e elements

- You will have to declare the iterator variable 'e' outside befor the macro
- 'e' should be a pointer to the element type. If the element type is already
  a pointer, the type of 'e' should be the element type.

*/ /*************************************************************************/

#define sFORALL(l,e) for(int _i=0;_i<(l).GetCount()?((e)=sGetPtr((l),_i)),1:0;_i++)

// version without iterator declaration
#define sFORALL_(l) for(int _i=0;_i<(l).GetCount();_i++)

/************************************************************************/ /*!

\ingroup altona_base_types_arrays

This macro iterates sStaticArray and it's subclasses in reverse order

@param l the array
@param e elements

see sFORALL for details

*/ /*************************************************************************/

#define sFORALLREVERSE(l,e) for(sInt _i=(l).GetCount()-1;_i>=0?((e)=sGetPtr((l),_i)),1:0;_i--)



/****************************************************************************/
/***                                                                      ***/
/***   Sorting                                                            ***/
/***                                                                      ***/
/****************************************************************************/

//! sort up, using '>' operator (ineffecient but compact)
//! \ingroup altona_base_types_arrays
template <class ArrayType>
void sSortUp(ArrayType &a)
{
    sInt max = a.GetCount();
    for(sInt i=0;i<max-1;i++)
        for(sInt j=i+1;j<max;j++)
            if(a[i] > a[j])
                sSwap(a[i],a[j]);
}

//! sort down, using '<' operator (ineffecient but compact)
//! \ingroup altona_base_types_arrays
template <class ArrayType>
void sSortDown(ArrayType &a)
{
    sInt max = a.GetCount();
    for(sInt i=0;i<max-1;i++)
        for(sInt j=i+1;j<max;j++)
            if(a[i] < a[j])
                sSwap(a[i],a[j]);
}

//! sort up using functor (ineffecient but compact)
//! \ingroup altona_base_types_arrays
template <class ArrayType, class CmpType>
void sCmpSortUp(ArrayType &a, sInt (*cmp) (CmpType, CmpType))
{
    sInt max = a.GetCount();
    for(sInt i=0;i<max-1;i++)
        for(sInt j=i+1;j<max;j++)
            if(cmp(a[i], a[j]) > 0)
                sSwap(a[i],a[j]);
}

//! sort down using functor (ineffecient but compact)
//! \ingroup altona_base_types_arrays
template <class ArrayType, class CmpType>
void sCmpSortDown(ArrayType &a, sInt (*cmp) (CmpType, CmpType))
{
    sInt max = a.GetCount();
    for(sInt i=0;i<max-1;i++)
        for(sInt j=i+1;j<max;j++)
            if(cmp(a[i], a[j]) < 0)
                sSwap(a[i],a[j]);
}

//! sort up using '>' operator on a member variable (ineffecient but compact)
//! \ingroup altona_base_types_arrays
template <class ArrayType,class BaseType,class MemberType>
void sSortUp(ArrayType &a,MemberType BaseType::*o)
{
    sInt max = a.GetCount();
    for(sInt i=0;i<max-1;i++)
        for(sInt j=i+1;j<max;j++)
            if(sGetPtr(a,i)->*o > sGetPtr(a,j)->*o)
                sSwap(a[i],a[j]);
}

//! sort down using '<' operator on a member variable (ineffecient but compact)
//! \ingroup altona_base_types_arrays
template <class ArrayType,class BaseType,class MemberType>
void sSortDown(ArrayType &a,MemberType BaseType::*o)
{
    sInt max = a.GetCount();
    for(sInt i=0;i<max-1;i++)
        for(sInt j=i+1;j<max;j++)
            if(sGetPtr(a,i)->*o < sGetPtr(a,j)->*o)
                sSwap(a[i],a[j]);
}


//! sort up using '<' operator (efficient)
//! \ingroup altona_base_types_arrays
template <class ArrayType>
void sHeapSortUp(ArrayType &a)
{
    sInt count=a.GetCount();

    // pass 1: heapify (sift up)
    for (sInt i=1; i<count; i++)
    {
        sInt child=i;
        while (child>0)
        {
            sInt root=(child-1)/2;
            if (a[root]<a[child])
            {
                sSwap(a[root],a[child]);
                child=root;
            }
            else
                break;
        }
    }

    // pass 2: sort (sift down)
    while (--count>0)
    {
        sSwap(a[0],a[count]);
        sInt root=0;
        sInt child;
        while ((child=root*2+1)<count)
        {
            if (child<count-1 && a[child]<a[child+1])
                child++;
            if (a[root]<a[child])
            {
                sSwap(a[root],a[child]);
                root=child;
            }
            else
                break;
        }
    }
}


//! sort up using '<' operator on member variable (efficient)
//! \ingroup altona_base_types_arrays

template <class ArrayType,class BaseType,class MemberType>
void sHeapSortUp(ArrayType &a,MemberType BaseType::*o)
{
    sInt count=a.GetCount();

    // pass 1: heapify (sift up)
    for (sInt i=1; i<count; i++)
    {
        sInt child=i;
        while (child>0)
        {
            sInt root=(child-1)/2;
            if (sGetPtr(a,root)->*o < sGetPtr(a,child)->*o)
            {
                sSwap(a[root],a[child]);
                child=root;
            }
            else
                break;
        }
    }

    // pass 2: sort (sift down)
    while (--count>0)
    {
        sSwap(a[0],a[count]);
        sInt root=0;
        sInt child;
        while ((child=root*2+1)<count)
        {
            if (child<count-1 && sGetPtr(a,child)->*o < sGetPtr(a,child+1)->*o)
                child++;
            if (sGetPtr(a,root)->*o<sGetPtr(a,child)->*o)
            {
                sSwap(a[root],a[child]);
                root=child;
            }
            else
                break;
        }
    }
}

template <class ArrayType>
void sGetSortPermutation(ArrayType &a, sStaticArray<sInt> &perm)
{
    sInt max = a.GetCount();
    perm.Resize(max);
    for (sInt i=0; i<max; i++) perm[i]=i;
    for(sInt i=0;i<max-1;i++)
        for(sInt j=i+1;j<max;j++)
            if(a[perm[j]] < a[perm[i]])
                sSwap(perm[i],perm[j]);
}


template <class ArrayType, class CmpType>
void sGetSortPermutation(ArrayType &a, sInt (*cmp) (CmpType, CmpType), sStaticArray<sInt> &perm)
{
    sInt max = a.GetCount();
    perm.Resize(max);
    for (sInt i=0; i<max; i++) perm[i]=i;
    for(sInt i=0;i<max-1;i++)
        for(sInt j=i+1;j<max;j++)
            if(cmp(a[perm[i]], a[perm[j]]) > 0)
                sSwap(perm[i],perm[j]);
}

template <class ArrayType,class BaseType,class MemberType>
void sGetSortPermutation(ArrayType &a,MemberType BaseType::*o, sStaticArray<sInt> &perm)
{
    sInt max = a.GetCount();
    perm.Resize(max);
    for (sInt i=0; i<max; i++) perm[i]=i;
    for(sInt i=0;i<max-1;i++)
        for(sInt j=i+1;j<max;j++)
            if(sGetPtr(a,perm[j])->*o < sGetPtr(a,perm[i])->*o)
                sSwap(perm[i],perm[j]);
}

// one should use some kind of function template or somethig...

//template <class Type> struct sCompareGreater { static sBool f(const Type a,const Type b) { return a>b; } };
//template <class Type> struct sCompareLesser  { static sBool f(const Type a,const Type b) { return a<b; } };

/****************************************************************************/
/***                                                                      ***/
/***   Remove and Delete by boolean test                                  ***/
/***                                                                      ***/
/****************************************************************************/

//! remove all elements with an attribute beeing true from list, without keeping order
//! \ingroup altona_base_types_arrays
template <class ArrayType,class BaseType,class MemberType>
void sRemTrue(ArrayType &a,MemberType BaseType::*o)
{
    for(sInt i=0;i<a.GetCount();)
    {
        if(sGetPtr(a,i)->*o)
            a.RemAt(i);
        else
            i++;
    }
}

//! remove all elements with an attribute beeing false from list, without keeping order
//! \ingroup altona_base_types_arrays
template <class ArrayType,class BaseType,class MemberType>
void sRemFalse(ArrayType &a,MemberType BaseType::*o)
{
    for(sInt i=0;i<a.GetCount();)
    {
        if(!(sGetPtr(a,i)->*o))
            a.RemAt(i);
        else
            i++;
    }
}

//! remove all elements with an attribute beeing true from list, maintaining order
//! \ingroup altona_base_types_arrays
template <class ArrayType,class BaseType,class MemberType>
void sRemOrderTrue(ArrayType &a,MemberType BaseType::*o)
{
    sInt max = a.GetCount();
    sInt used = 0;
    for(sInt i=0;i<max;i++)
        if(!(sGetPtr(a,i)->*o))
            a[used++] = a[i];
    a.Resize(used);
}

//! remove all elements with an attribute beeing false from list, maintaining order
//! \ingroup altona_base_types_arrays
template <class ArrayType,class BaseType,class MemberType>
void sRemOrderFalse(ArrayType &a,MemberType BaseType::*o)
{
    sInt max = a.GetCount();
    sInt used = 0;
    for(sInt i=0;i<max;i++)
        if(sGetPtr(a,i)->*o)
            a[used++] = a[i];
    a.Resize(used);
}

//! remove elements with an attribute equals a compare value from list, not keeping order
//! \ingroup altona_base_types_arrays
template <class ArrayType,class BaseType,class MemberType>
void sRemEqual(ArrayType &a,MemberType BaseType::*o,const MemberType &e)
{
    for(sInt i=0;i<a.GetCount();)
    {
        if(sGetPtr(a,i)->*o == e)
            a.RemAt(i);
        else
            i++;
    }
}

//! remove all elements with an attribute beeing true from list, without keeping order, then deleting it
//! \ingroup altona_base_types_arrays
template <class ArrayType,class BaseType,class MemberType>
void sDeleteTrue(ArrayType &a,MemberType BaseType::*o)
{
    for(sInt i=0;i<a.GetCount();)
    {
        if(sGetPtr(a,i)->*o)
        {
            delete sGetPtr(a,i);
            a.RemAt(i);
        }
        else
        {
            i++;
        }
    }
}

//! remove all elements with an attribute beeing false from list, without keeping order, then deleting it
//! \ingroup altona_base_types_arrays
template <class ArrayType,class BaseType,class MemberType>
void sDeleteFalse(ArrayType &a,MemberType BaseType::*o)
{
    for(sInt i=0;i<a.GetCount();)
    {
        if(!(sGetPtr(a,i)->*o))
        {
            delete sGetPtr(a,i);
            a.RemAt(i);
        }
        else
        {
            i++;
        }
    }
}

//! remove and delete all elements from an array
//! \ingroup altona_base_types_arrays
template <class ArrayType>
void sDeleteAll(ArrayType &a)
{
    while(!a.IsEmpty())
        delete a.RemTail();
}

//! for all elements in an array, remove it and call Release()
//! \ingroup altona_base_types_arrays
template <class ArrayType>
void sReleaseAll(ArrayType &a)
{
    while(!a.IsEmpty())
        a.RemTail()->Release();
}

//! for all elements in an array, call AddRef()
//! \ingroup altona_base_types_arrays
template <template <typename> class ArrayType,class BaseType>
void sAddRefAll(ArrayType<BaseType> &a)
{
    BaseType e;
    sFORALL(a,e)
            e->AddRef();
}


/****************************************************************************/
/***                                                                      ***/
/***   Finding                                                            ***/
/***                                                                      ***/
/****************************************************************************/

// const versions

//! Find first element by comparing a member.
template <class ArrayType,class BaseType,class MemberType,class CompareType>
const BaseType *sFind(const ArrayType &a,MemberType BaseType::*o,const CompareType i)
{
    const BaseType *e;
    sFORALL(a,e)
            if(e->*o == i)
            return e;
    return 0;
}

//! Find first element by comparing a member and return the index.
template <class ArrayType,class BaseType,class MemberType,class CompareType>
sInt sFindIndex(const ArrayType &a,MemberType BaseType::*o,const CompareType i)
{
    const BaseType *e;
    sFORALL(a,e)
            if(e->*o == i)
            return _i;
    return -1;
}

//! Continue searching for an element by comparing a member and return the next index.
template <class ArrayType,class BaseType,class MemberType,class CompareType>
sInt sFindNextIndex(const ArrayType &a, sInt index,MemberType BaseType::*o,const CompareType i)
{
    const BaseType *e;
    sInt _i=index+1;
    for (; _i<a.GetCount()?(e=sGetPtr((a),_i)),1:0;_i++)
        if(e->*o == i)
            return _i;
    return -1;
}

//! Find first element by comparing the whole value.
template <class ArrayType,class BaseType>
const BaseType *sFind(const ArrayType &a,const BaseType &v)
{
    const BaseType *e;
    sFORALL(a,e)
            if(*e == v)
            return e;
    return 0;
}

//! Check if a certain element is in an array by comparing the whole value
template <class ArrayType,class BaseType>
sBool sFindPtr(const ArrayType &a,const BaseType *v)
{
    sInt max = a.GetCount();
    for (sInt i=0; i<max; i++)
        if(a[i] == v)
            return 1;
    return 0;
}

//! Find first element by comparing the whole value and return the index.
template <class ArrayType,class BaseType>
sInt sFindIndex(const ArrayType &a,const BaseType &v)
{
    sInt max = a.GetCount();
    for (sInt i=0; i<max; i++)
        if(a[i] == v)
            return i;
    return -1;
}

//! Continue searching for an element by comparing the whole value and return the next index.
template <class ArrayType,class BaseType>
sInt sFindNextIndex(const ArrayType &a, sInt index,const BaseType &v)
{
    for (sInt i=index+1; i<a.GetCount(); i++)
        if(a[i] == v)
            return i;
    return -1;
}

template <class ArrayType,class BaseType>
sBool sContains(const ArrayType &a,const BaseType &v)
{
    return sFindIndex(a,v)!=-1;
}



template <class ArrayType,class BaseType>
sInt sFindIndexPtr(const ArrayType &a,const BaseType *v)
{
    // this can be more efficient implemented using pointerarithmetics
    sInt max = a.GetCount();
    for (sInt i=0; i<max; i++)
        if(&a[i] == v)
            return i;
    return -1;
}

//! Find first element by comparing a member and return the index.
template <class ArrayType,class BaseType,class MemberType>
sInt sFindIndex(const ArrayType &a, MemberType BaseType::*o, const MemberType v)
{
    sInt max = a.GetCount();
    for(sInt i=0;i<max;i++)
        if(sGetPtr(a,i)->*o == v)
            return i;
    return -1;
}

//! Find first element with a certain member being \b true.
template <class ArrayType,class BaseType,class MemberType>
const BaseType *sFindTrue(const ArrayType &a,MemberType BaseType::*o)
{
    const BaseType *e;
    sFORALL(a,e)
            if(e->*o)
            return e;
    return 0;
}

//! Find first element with a certain member being \b false.
template <class ArrayType,class BaseType,class MemberType>
const BaseType *sFindFalse(const ArrayType &a,MemberType BaseType::*o)
{
    const BaseType *e;
    sFORALL(a,e)
            if(!e->*o)
            return e;
    return 0;
}

/****************************************************************************/

// non-const versions

template <class ArrayType,class BaseType,class MemberType,class CompareType>
BaseType *sFind(ArrayType &a,MemberType BaseType::*o,const CompareType i)
{
    BaseType *e;
    sFORALL(a,e)
            if(e->*o == i)
            return e;
    return 0;
}

template <class ArrayType,class BaseType>
BaseType *sFind(ArrayType &a,const BaseType &v)
{
    BaseType *e;
    sFORALL(a,e)
            if(*e == v)
            return e;
    return 0;
}

template <class ArrayType,class BaseType,class MemberType>
BaseType *sFindTrue(ArrayType &a,MemberType BaseType::*o)
{
    BaseType *e;
    sFORALL(a,e)
            if(e->*o)
            return e;
    return 0;
}

template <class ArrayType,class BaseType,class MemberType>
BaseType *sFindFalse(ArrayType &a,MemberType BaseType::*o)
{
    BaseType *e;
    sFORALL(a,e)
            if(!e->*o)
            return e;
    return 0;
}

//! Find first element with a certain member by using case insensitive string compares (sCmpStringI())
template <class ArrayType,class BaseType,class MemberType,class CompareType>
BaseType *sFindStringI(ArrayType &a,MemberType BaseType::*o,const CompareType i)
{
    BaseType *e;
    sFORALL(a,e)
            if(sCmpStringI(e->*o,i)==0)
            return e;
    return 0;
}

//! Find first element with a certain member by using file-path insensitive string compares (sCmpStringP())
template <class ArrayType,class BaseType,class MemberType,class CompareType>
BaseType *sFindStringP(ArrayType &a,MemberType BaseType::*o,const CompareType i)
{
    BaseType *e;
    sFORALL(a,e)
            if(sCmpStringP(e->*o,i)==0)
            return e;
    return 0;
}























/****************************************************************************/
/***                                                                      ***/
/***   Simple Hash Table                                                  ***/
/***                                                                      ***/
/****************************************************************************/
/***                                                                      ***/
/***   - does not manage memory for keys or values                        ***/
/***   - add does not check if the key already exists                     ***/
/***                                                                      ***/
/***   sHashTableBase contains the implementation                         ***/
/***                                                                      ***/
/***   Use the sHashTable template if the key has comparison operator     ***/
/***   and a Hash() member function                                       ***/
/***                                                                      ***/
/***   You can also derive the class yourself                             ***/
/***                                                                      ***/
/****************************************************************************/

// base class

class sHashTableBase
{
private:
    struct Node                     // a node in the hashtable
    {
        Node *Next;                   // linked list inside bin
        const void *Key;              // ptr to key
        void *Value;                  // ptr to value
    };

    // the hashtable

    sInt HashSize;                  // size of hashtable (power of two)
    sInt HashMask;                  // size-1;
    Node **HashTable;                // the hashtable itself

    // don't allocate nodes one-by-one!

    sInt NodesPerBlock;             // nodes per allocation block
    Node *CurrentNodeBlock;         // current node allocation block
    sInt CurrentNode;               // next free node in allocation block
    sArray<Node *> NodeBlocks;      // all allocation blocks
    Node *FreeNodes;                // list of free nodes for reuse

    // interface

    Node *AllocNode();
public:
    sHashTableBase(sInt size=0x4000,sInt nodesperblock=0x100);
    virtual ~sHashTableBase();
    void Clear();
    void ClearAndDeleteValues();
    void ClearAndDeleteKeys();
    void ClearAndDelete();

protected:
    void Add(const void *key,void *value);
    void *Find(const void *key);
    void *Rem(const void *key);
    void GetAll(sArray<void *> *a);

    virtual sBool CompareKey(const void *k0,const void *k1)=0;
    virtual sU32 HashKey(const void *key)=0;
};


// usage

template<class KeyType,class ValueType>
class sHashTable : public sHashTableBase
{
protected:
    sBool CompareKey(const void *k0,const void *k1)    { return (*(const KeyType *)k0)==(*(const KeyType *)k1); }
    sU32 HashKey(const void *key)                      { return ((const KeyType *)key)->Hash(); }
public:
    sHashTable(sInt s=0x4000,sInt n=0x100) : sHashTableBase(s,n)  {}
    void Add(const KeyType *key,ValueType *value)         { sHashTableBase::Add(key,value); }
    ValueType *Find(const KeyType *key)                   { return (ValueType *) sHashTableBase::Find(key); }
    ValueType *Rem(const KeyType *key)                    { return (ValueType *) sHashTableBase::Rem(key); }

    void GetAll(sArray<ValueType *> *a)                 { sHashTableBase::GetAll((sArray<void *> *)a); }
};






sU32 sChecksumMurMur(const sU32 *data,sInt words);


sU32 sAddColor(sU32 a,sU32 b);


sU32 sFadeColor(sInt fade,sU32 a,sU32 b);







/*void sDPrint(const sChar *text)
{
    printf(text);
}*/


//---------------------------------------------------------------------------------------------
// print short text to stdout using std::cout but like printf format
// use this print function to redirect text to log editor
//---------------------------------------------------------------------------------------------
sChar * sDPrintF(const sChar *fmt, ...);



sChar sUpperChar(sU8 c);
sChar sLowerChar(sU8 c);
sChar *sMakeUpper(sChar * s);
sChar *sMakeLower(sChar * s);



inline sBool sIsDigit(sInt i) { return (i>='0' && i<='9'); }
inline sBool sIsLetter(sInt i) { return (i>='a' && i<='z') || (i>='A' && i<='Z') || i=='_'; }
inline sBool sIsSpace(sInt i) { return i==' ' || i=='\t' || i=='\r' || i=='\n'; }
inline sBool sIsHex(sInt i) { return (i>='a' && i<='f') || (i>='A' && i<='F') || (i>='0' && i<='9'); }



sBool sScanFloat(const sChar *&s,sF32 &result);


} // end namespace wz4

#endif // WZ4TYPE2_HPP

