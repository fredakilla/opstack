#include "wz4Types.hpp"

#include <math.h>
#include <string.h>

namespace wz4 {

/****************************************************************************/
/***                                                                      ***/
/***   sRandom                                                            ***/
/***                                                                      ***/
/****************************************************************************/

void sRandom::Seed(sU32 seed)
{
    kern = seed+seed*17+seed*121+(seed*121/17);
    Int32();
    kern ^= seed+seed*17+seed*121+(seed*121/17);
    Int32();
    kern ^= seed+seed*17+seed*121+(seed*121/17);
    Int32();
    kern ^= seed+seed*17+seed*121+(seed*121/17);
    Int32();
}

sU32 sRandom::Step()
{
    const sU32 a = 1103515245;
    const sU32 c = 12345;
    kern = a*kern+c;
    return kern & 0x7fffffff;
}

sU32 sRandom::Int32()
{
    sU32 r0,r1;
    r0 = Step();
    r1 = Step();

    return r0 ^ ((r1<<16) | (r1>>16));
}

sInt sRandom::Int(sInt max_)
{
    sVERIFY(max_>=1);

    sU32 max = sU32(max_-1);
    sU32 mask = sMakeMask(max);

    sU32 v;
    do
    {
        v = Int32()&mask;
    }
    while(v>max);

    return v;
}

sF32 sRandom::Float(sF32 max)
{
    return (0x7fffffff & Int32())*max/(0x8000*65536.0f);
}



/****************************************************************************/
/***                                                                      ***/
/***   sRandomMT                                                          ***/
/***                                                                      ***/
/****************************************************************************/

sU32 sRandomMT::Step()
{
    if(Index==Count)
    {
        Reload();
        Index = 0;
    }
    sU32 v = State[Index++];
    v ^= (v>>11);
    v ^= (v<< 7)&0x9d2c5680UL;
    v ^= (v<<15)&0xefc60000UL;
    v ^= (v>>18);
    return v;
}

static sU32 mtwist(sU32 m,sU32 s0,sU32 s1)
{
    return m ^ ((s0&0x80000000)|(s1&0x7fffffff)) ^ (sU32(-sInt(s1&1))&0x9908b0dfUL);
}

void sRandomMT::Reload()
{
    sU32 *p = State;
    for(sInt i=0;i<Count-Period;i++)
        p[i] = mtwist(p[i+Period],p[i+0],p[i+1]);
    for(sInt i=Count-Period;i<Count-1;i++)
        p[i] = mtwist(p[i+Period-Count],p[i+0],p[i+1]);
    p[Count-1] = mtwist(p[Period-1],p[Count-1],State[0]);
}

void sRandomMT::Seed(sU32 seed)
{
    for(sInt i=0;i<Count;i++)
    {
        State[i] = seed;
        seed = 1812433253UL*((seed^(seed>>30))+i);
    }
    Index = Count;
}

sInt sRandomMT::Int(sInt max_)
{
    sVERIFY(max_>=1);

    sU32 max = sU32(max_-1);
    sU32 mask = sMakeMask(max);

    sU32 v;
    do
    {
        v = Step()&mask;
    }
    while(v>max);

    return v;
}

sF32 sRandomMT::Float(sF32 max)
{
    return Step()*max/(65536.0f*65536.0f);
}

/****************************************************************************/
/***                                                                      ***/
/***   Arithmetic Functions                                               ***/
/***                                                                      ***/
/****************************************************************************/

#ifndef sHASINTRINSIC_SETMEM
void sSetMem(void *dd,sInt s,sInt c) { memset(dd,s,c); }
#endif

#ifndef sHASINTRINSIC_COPYMEM
void sCopyMem(void *dd,const void *ss,sInt c) { memcpy(dd,ss,c); }
#endif

void sMoveMem(void *dd,const void *ss,sInt c) { memmove(dd,ss,c); }

#ifndef sHASINTRINSIC_CMPMEM
sInt sCmpMem(const void *dd,const void *ss,sInt c) { return memcmp(dd,ss,c); }
#endif

// int

#ifndef sHASINTRINSIC_MULDIV
sInt sMulDiv(sInt a,sInt b,sInt c)  { return sS64(a)*b/c; }
#endif

#ifndef sHASINTRINSIC_MULDIVU
sU32 sMulDivU(sU32 a,sU32 b,sU32 c)  { return sU64(a)*b/c; }
#endif

#ifndef sHASINTRINSIC_MULSHIFT
sInt sMulShift(sInt a,sInt b)  { return (sS64(a)*b)>>16; }
#endif

#ifndef sHASINTRINSIC_MULSHIFTU
sU32 sMulShiftU(sU32 a,sU32 b)  { return (sU64(a)*b)>>16; }
#endif

#ifndef sHASINTRINSIC_DIVSHIFT
sInt sDivShift(sInt a,sInt b)  { return (sS64(a)<<16)/b; }
#endif

// float fiddling

#ifndef sHASINTRINSIC_ABSF
sF32 sAbs(sF32 f)               { return fabs(f); }
#endif

#ifndef sHASINTRINSIC_MINF
sF32 sMinF(sF32 a,sF32 b)       { return sMin(a,b); }
#endif

#ifndef sHASINTRINSIC_MAXF
sF32 sMaxF(sF32 a,sF32 b)       { return sMax(a,b); }
#endif

#ifndef sHASINTRINSIC_MOD
sF32 sMod(sF32 over,sF32 under) { return fmod(over,under); }
#endif

sInt sAbsMod(sInt over,sInt under) { return (over >= 0)?(over % under):(under-(-over % under)); }
sF32 sAbsMod(sF32 over,sF32 under) { return (over >= 0.0f)?sMod(over,under):(under-sMod(-over,under)); }

#ifndef sHASINTRINSIC_DIVMOD
void sDivMod(sF32 over,sF32 under,sF32 &div,sF32 &mod)
{
    div = over/under;
    mod = sMod(over,under);
}
#endif


#ifndef sHASINTRINSIC_FRAC_FF
void sFrac(sF32 val,sF32 &frac,sF32 &full)
{
    frac = modff(val,&full);
}
#endif

#ifndef sHASINTRINSIC_FRAC_FI
void sFrac(sF32 val,sF32 &frac,sInt &full)
{
    sF32 f;
    frac = modff(val,&f);
    full = sInt(f);
}
#endif

#ifndef sHASINTRINSIC_FRAC
sF32 sFrac(sF32 val)
{
    sF32 f;
    return modff(val,&f);
}
#endif

#ifndef sHASINTRINSIC_ROUNDDOWN
sF32 sRoundDown(sF32 f)         { return floorf(f); }
#endif

#ifndef sHASINTRINSIC_ROUNDUP
sF32 sRoundUp(sF32 f)           { return ceilf(f); }
#endif

#ifndef sHASINTRINSIC_ROUNDZERO
sF32 sRoundZero(sF32 f)         { return (f>=0.0f)?sFFloor(f):sFCeil(f); }
#endif

#ifndef sHASINTRINSIC_ROUNDNEAR
sF32 sRoundNear(sF32 f)         { return floorf(f+0.5f); }
#endif

#ifndef sHASINTRINSIC_ROUNDDOWNI
sInt sRoundDowint(sF32 f)      { return sInt(floorf(f)); }
#endif

#ifndef sHASINTRINSIC_ROUNDUPI
sInt sRoundUpInt(sF32 f)        { return sInt(ceilf(f)); }
#endif

#ifndef sHASINTRINSIC_ROUNDZEROI
sInt sRoundZeroInt(sF32 f)      { return sInt(f); }
#endif

#ifndef sHASINTRINSIC_ROUNDNEARI
sInt sRoundNearInt(sF32 f)      { return sInt(floorf(f+0.5f)); }
#endif

#ifndef sHASINTRINSIC_SELECT
#ifndef sHASINTRINSIC_SELECTEQ
sF32 sSelectEQ(sF32 a,sF32 b,sF32 t,sF32 f)   { return (a==b) ? t : f; }
#endif

#ifndef sHASINTRINSIC_SELECTNE
sF32 sSelectNE(sF32 a,sF32 b,sF32 t,sF32 f)   { return (a!=b) ? t : f; }
#endif

#ifndef sHASINTRINSIC_SELECTGT
sF32 sSelectGT(sF32 a,sF32 b,sF32 t,sF32 f)   { return (a>=b) ? t : f; }
#endif

#ifndef sHASINTRINSIC_SELECTGE
sF32 sSelectGE(sF32 a,sF32 b,sF32 t,sF32 f)   { return (a> b) ? t : f; }
#endif

#ifndef sHASINTRINSIC_SELECTLT
sF32 sSelectLT(sF32 a,sF32 b,sF32 t,sF32 f)   { return (a<=b) ? t : f; }
#endif

#ifndef sHASINTRINSIC_SELECTLE
sF32 sSelectLE(sF32 a,sF32 b,sF32 t,sF32 f)   { return (a< b) ? t : f; }
#endif
#endif

#ifndef sHASINTRINSIC_SELECT0
#ifndef sHASINTRINSIC_SELECTEQ0
sF32 sSelectEQ(sF32 a,sF32 b,sF32 t)          { return (a==b) ? t : 0; }
#endif

#ifndef sHASINTRINSIC_SELECTNE0
sF32 sSelectNE(sF32 a,sF32 b,sF32 t)          { return (a!=b) ? t : 0; }
#endif

#ifndef sHASINTRINSIC_SELECTGT0
sF32 sSelectGT(sF32 a,sF32 b,sF32 t)          { return (a>=b) ? t : 0; }
#endif

#ifndef sHASINTRINSIC_SELECTGE0
sF32 sSelectGE(sF32 a,sF32 b,sF32 t)          { return (a> b) ? t : 0; }
#endif

#ifndef sHASINTRINSIC_SELECTLT0
sF32 sSelectLT(sF32 a,sF32 b,sF32 t)          { return (a<=b) ? t : 0; }
#endif

#ifndef sHASINTRINSIC_SELECTLE0
sF32 sSelectLE(sF32 a,sF32 b,sF32 t)          { return (a< b) ? t : 0; }
#endif
#endif

// non-trivial float

#ifndef sHASINTRINSIC_SQRT
sF32 sSqrt(sF32 f)        { return sqrtf(f); }
#endif

#ifndef sHASINTRINSIC_RSQRT
sF32 sRSqrt(sF32 f)       { return 1.0f/sqrtf(f); }
#endif

#ifndef sHASINTRINSIC_LOG
sF32 sLog(sF32 f)         { return logf(f); }
#endif

#ifndef sHASINTRINSIC_LOG2
sF32 sLog2(sF32 f)        { return logf(f)/0.69314718055994530941723212145818f; }
#endif

#ifndef sHASINTRINSIC_LOG10
sF32 sLog10(sF32 f)       { return log10f(f); }
#endif

#ifndef sHASINTRINSIC_EXP
sF32 sExp(sF32 f)         { return expf(f); }
#endif

#ifndef sHASINTRINSIC_POW
sF32 sPow(sF32 a,sF32 b)  { return powf(a,b); }
#endif


#ifndef sHASINTRINSIC_SIN
sF32 sSin(sF32 f)         { return sinf(f); }

#endif

#ifndef sHASINTRINSIC_COS
sF32 sCos(sF32 f)         { return cosf(f); }
#endif

#ifndef sHASINTRINSIC_TAN
sF32 sTan(sF32 f)         { return tanf(f); }
#endif

#ifndef sHASINTRINSIC_SINCOS
void sSinCos(sF32 f,sF32 &s,sF32 &c)  { s=sinf(f); c=cosf(f); }
#endif

#ifndef sHASINTRINSIC_ASIN
sF32 sASin(sF32 f)        { return asinf(f); }
#endif

#ifndef sHASINTRINSIC_ACOS
sF32 sACos(sF32 f)        { return acosf(f); }
#endif

#ifndef sHASINTRINSIC_ATAN
sF32 sATan(sF32 f)        { return (sF32) atan(f); }
#endif

#ifndef sHASINTRINSIC_ATAN2
sF32 sATan2(sF32 a,sF32 b){ return (sF32) atan2(a,b); }
#endif

// non-trivial float, fast

#ifndef sHASINTRINSIC_FSQRT
sF32 sFSqrt(sF32 f)       { return sqrtf(f); }
#endif

#ifndef sHASINTRINSIC_FRSQRT
sF32 sFRSqrt(sF32 f)      { return 1.0f/sqrtf(f); }
#endif

#ifndef sHASINTRINSIC_FLOG
sF32 sFLog(sF32 f)        { return logf(f); }
#endif

#ifndef sHASINTRINSIC_FLOG2
sF32 sFLog2(sF32 f)       { return logf(f)/0.69314718055994530941723212145818f; }
#endif

#ifndef sHASINTRINSIC_FLOG10
sF32 sFLog10(sF32 f)      { return log10f(f); }
#endif

#ifndef sHASINTRINSIC_FEXP
sF32 sFExp(sF32 f)        { return expf(f); }
#endif

#ifndef sHASINTRINSIC_FPOW
sF32 sFPow(sF32 a,sF32 b) { return powf(a,b); }
#endif


#ifndef sHASINTRINSIC_FSIN
sF32 sFSin(sF32 f)        { return sinf(f); }
#endif

#ifndef sHASINTRINSIC_FCOS
sF32 sFCos(sF32 f)        { return cosf(f); }
#endif

#ifndef sHASINTRINSIC_FTAN
sF32 sFTan(sF32 f)        { return tanf(f); }
#endif

#ifndef sHASINTRINSIC_FSINCOS
void sFSinCos(sF32 f,sF32 &s,sF32 &c)  { s=sinf(f); c=cosf(f); }
#endif

#ifndef sHASINTRINSIC_FASIN
sF32 sFASin(sF32 f)       { return asinf(f); }
#endif

#ifndef sHASINTRINSIC_FACOS
sF32 sFACos(sF32 f)       { return acosf(f); }
#endif

#ifndef sHASINTRINSIC_FATAN
sF32 sFATan(sF32 f)       { return atanf(f); }
#endif

#ifndef sHASINTRINSIC_FATAN2
sF32 sFATan2(sF32 a,sF32 b){ return atan2f(a,b); }
#endif






sInt sCmpString(const sChar *a,const sChar *b)
{
    sInt aa,bb;
    do
    {
        aa = *a++;
        bb = *b++;
    }
    while(aa!=0 && aa==bb);
    return sSign(aa-bb);
}



uint sColorFade (uint a, uint b, float f)
{
    uint f1 = uint(f*256);
    uint f0 = 256-uint(f*256);
    return (sClamp((((a>>24)&0xff)*f0+((b>>24)&0xff)*f1)/256,uint(0),uint(255)) << 24) |
            (sClamp((((a>>16)&0xff)*f0+((b>>16)&0xff)*f1)/256,uint(0),uint(255)) << 16) |
            (sClamp((((a>> 8)&0xff)*f0+((b>> 8)&0xff)*f1)/256,uint(0),uint(255)) << 8) |
            (sClamp((((a>> 0)&0xff)*f0+((b>> 0)&0xff)*f1)/256,uint(0),uint(255)) << 0);
}


} // end namespace wz4
