#include "wz4Types2.hpp"


#include <stdio.h>
#include <stdarg.h>
#include <iostream>

namespace wz4 {


sU32 sChecksumMurMur(const sU32 *data,sInt words)
{
    const sU32 m = 0x5bd1e995;

    sU32 h = m;
    sU32 k;

    for(sInt i=0;i<words;i++)
    {
        k = data[i];
        k *= m;
        k ^= k>>24;
        k *= m;

        h *= m;
        h ^= k;
    }

    return h;
}



sU32 sAddColor(sU32 a,sU32 b)
{
    return (sClamp<sInt>(((a>> 0)&255)+((b>> 0)&255),0,255)<< 0)
            + (sClamp<sInt>(((a>> 8)&255)+((b>> 8)&255),0,255)<< 8)
            + (sClamp<sInt>(((a>>16)&255)+((b>>16)&255),0,255)<<16)
            + (sClamp<sInt>(((a>>24)&255)+((b>>24)&255),0,255)<<24);
}



sU32 sFadeColor(sInt fade,sU32 a,sU32 b)
{
    sInt f1 = fade;
    sInt f0 = 0x10000-fade;
    return (( ((((a>> 0)&255)*f0)>>16)  + ((((b>> 0)&255)*f1)>>16) )<< 0)
            + (( ((((a>> 8)&255)*f0)>>16)  + ((((b>> 8)&255)*f1)>>16) )<< 8)
            + (( ((((a>>16)&255)*f0)>>16)  + ((((b>>16)&255)*f1)>>16) )<<16)
            + (( ((((a>>24)&255)*f0)>>16)  + ((((b>>24)&255)*f1)>>16) )<<24);
}






sChar * sDPrintF(const sChar *fmt, ...)
{
    static sChar buffer[255] = "";

    va_list argptr;
    va_start(argptr,fmt);
    vsprintf(buffer, fmt, argptr);
    va_end(argptr);

    std::cout << buffer;

    return buffer;
}









sChar sUpperChar(sU8 c)
{
    if(c>=0xe0 && c<=0xfd && c!=247) return c-0x20;
    if(c>='a'  && c<='z'           ) return c-0x20;
    return c;
}

sChar sLowerChar(sU8 c)
{
    if(c>=0xc0 && c<=0xdd && c!=215) return c+0x20;
    if(c>='A'  && c<='Z'           ) return c+0x20;
    return c;
}

sChar *sMakeUpper(sChar * s)
{
    sChar * c = s;
    while (*c > 0)
    {
        *c = sUpperChar(*c);
        c++;
    }
    return s;
}

sChar *sMakeLower(sChar * s)
{
    sChar * c = s;
    while (*c > 0)
    {
        *c = sLowerChar(*c);
        c++;
    }
    return s;
}


sBool sScanFloat(const sChar *&s,sF32 &result)
{
    sF64 val = 0;
    sF64 dec = 1;
    sInt sign = 1;

    // scan sign

    if(*s=='-')
    {
        s++;
        sign = -1;
    }
    else if(*s=='+')
    {
        s++;
    }

    // here we must have either '.' or digit.

    if(!sIsDigit(*s) && *s!='.')
        return 0;

    // scan integer part

    while(sIsDigit(*s))
    {
        val = val * 10 + ((*s++)&15);
    }

    // optional fractional part

    if(*s=='.')
    {
        s++;
        while(sIsDigit(*s))
        {
            dec = dec * 10;
            val += ((*s++)&15) / dec;
        }
    }

    // optional exponent

    if(*s=='e' || *s=='E')
    {
        s++;

        // optional exponent sign
        sInt eSign = 1;
        if(*s == '-')
            s++, eSign = -1;
        else if(*s == '+')
            s++;

        // exponent itself
        if(!sIsDigit(*s))
            return 0;

        sInt eVal = 0;
        while(sIsDigit(*s))
            eVal = eVal * 10 + (*s++ - '0');

        val *= sFPow(10.0f,eSign * eVal);
    }

    // apply sign and write out

    result = sF32(val * sign);

    // done

    return 1;
}





/****************************************************************************/
/***                                                                      ***/
/***   Hashtable                                                          ***/
/***                                                                      ***/
/****************************************************************************/

sHashTableBase::sHashTableBase(sInt size,sInt nodesperblock)
{
    HashSize = size;
    HashMask = size-1;
    HashTable = new Node*[HashSize];
    for(sInt i=0;i<HashSize;i++)
        HashTable[i] = 0;

    NodesPerBlock = nodesperblock;
    CurrentNodeBlock = 0;
    CurrentNode = NodesPerBlock;
    FreeNodes = 0;
}

sHashTableBase::~sHashTableBase()
{
    Clear();
    delete HashTable;
}

void sHashTableBase::Clear()
{
    for(sInt i=0;i<HashSize;i++)
        HashTable[i] = 0;
    sDeleteAll(NodeBlocks);
    FreeNodes = 0;
    CurrentNode = NodesPerBlock;
    CurrentNodeBlock = 0;
}

sHashTableBase::Node *sHashTableBase::AllocNode()
{
    // (a): use free list

    if(FreeNodes)
    {
        Node *n = FreeNodes;
        FreeNodes = n->Next;
        return n;
    }

    // (b): check blocks

    if(CurrentNode==NodesPerBlock)
    {
        CurrentNodeBlock = new Node[NodesPerBlock];
        NodeBlocks.AddTail(CurrentNodeBlock);
        CurrentNode = 0;
    }

    // (c): get next node from block

    return &CurrentNodeBlock[CurrentNode++];
}

void sHashTableBase::Add(const void *key,void *value)
{
    Node *n = AllocNode();
    n->Value = value;
    n->Key = key;
    sU32 hash = HashKey(key) & HashMask;
    n->Next = HashTable[hash];
    HashTable[hash] = n;
}

void *sHashTableBase::Find(const void *key)
{
    sU32 hash = HashKey(key) & HashMask;
    Node *n = HashTable[hash];
    while(n)
    {
        if(CompareKey(n->Key,key))
            return n->Value;
        n = n->Next;
    }
    return 0;
}

void *sHashTableBase::Rem(const void *key)
{
    sU32 hash = HashKey(key) & HashMask;
    Node *n = HashTable[hash];
    Node **l = &HashTable[hash];
    while(n)
    {
        if(CompareKey(n->Key,key))
        {
            *l = n->Next;         // remove from hashtable
            n->Next = FreeNodes;  // insert in free nodes list
            FreeNodes = n;
            return n->Value;      // return value
        }
        l = &n->Next;           // maintain last ptr
        n = n->Next;            // next element
    }
    return 0;
}

void sHashTableBase::ClearAndDeleteValues()
{
    Node *n;
    for(sInt i=0;i<HashSize;i++)
    {
        n = HashTable[i];
        while(n)
        {
            delete (sU8 *)n->Value;
            n = n->Next;
        }
    }
    Clear();
}

void sHashTableBase::ClearAndDeleteKeys()
{
    Node *n;
    for(sInt i=0;i<HashSize;i++)
    {
        n = HashTable[i];
        while(n)
        {
            delete (sU8 *)n->Key;
            n = n->Next;
        }
    }
    Clear();
}

void sHashTableBase::ClearAndDelete()
{
    Node *n;
    for(sInt i=0;i<HashSize;i++)
    {
        n = HashTable[i];
        while(n)
        {
            delete (sU8 *)n->Value;
            delete (sU8 *)n->Key;
            n = n->Next;
        }
    }
    Clear();
}

void sHashTableBase::GetAll(sArray<void *> *a)
{
    for(sInt i=0;i<HashSize;i++)
    {
        Node *n = HashTable[i];
        while(n)
        {
            a->AddTail(n->Value);
            n = n->Next;
        }
    }
}

} // end namespace wz4
