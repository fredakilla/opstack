#ifndef WZ4LIB_HPP
#define WZ4LIB_HPP

#include "wz4Types.hpp"
#include "wz4Types2.hpp"
#include "wz4Math.hpp"
#include "wz4Algorithms.hpp"
#include "wz4Common.hpp"
#include "wz4Bspline.hpp"
#include "wz4Anim.hpp"
#include "wz4Bitmap.hpp"
#include "wz4Mesh.hpp"

#endif // WZ4LIB_HPP
