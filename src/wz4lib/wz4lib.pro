#--------------------------------------------------------------------
# 3rdparty libraries path
#--------------------------------------------------------------------

URHO3D = /home/fred/Documents/Urho3D/BUILD

#--------------------------------------------------------------------
# target
#--------------------------------------------------------------------

TEMPLATE = lib
CONFIG -= qt
CONFIG += c++11
CONFIG += staticlib
CONFIG -= debug_and_release
CONFIG -= debug_and_release_target

#--------------------------------------------------------------------
# output directory
#--------------------------------------------------------------------

CONFIG(debug,debug|release) {
    DESTDIR = $$PWD/../../binary
} else {
    DESTDIR = $$PWD/../../binary
}

QMAKE_CLEAN += $$DESTDIR/$$TARGET

#--------------------------------------------------------------------
# compilation flags
#--------------------------------------------------------------------

unix:!macx: QMAKE_CXXFLAGS_WARN_ON -= -Wall
unix:!macx: QMAKE_CFLAGS_WARN_ON -= -Wall

unix:!macx: QMAKE_CXXFLAGS += -Wall
unix:!macx: QMAKE_CXXFLAGS += -Wno-comment
unix:!macx: QMAKE_CXXFLAGS += -Wno-ignored-qualifiers
unix:!macx: QMAKE_CXXFLAGS += -Wno-unused-parameter
unix:!macx: QMAKE_CXXFLAGS += -msse2

CONFIG(debug,debug|release) {
#message( debug )
} else {
#message( release )
unix:!macx: QMAKE_CXXFLAGS += -Wno-strict-aliasing
unix:!macx: QMAKE_CXXFLAGS_RELEASE += -O3
}

#--------------------------------------------------------------------
# libraries includes
#--------------------------------------------------------------------

INCLUDEPATH += $${URHO3D}/include
INCLUDEPATH += $${URHO3D}/include/Urho3D/ThirdParty

#--------------------------------------------------------------------
# project files
#--------------------------------------------------------------------

HEADERS += \
    wz4Types.hpp \
    wz4Math.hpp \
    wz4Mesh.hpp \
    wz4Types2.hpp \
    wz4Anim.hpp \
    wz4Common.hpp \
    wz4Bitmap.hpp \
    wz4Bspline.hpp \
    wz4Algorithms.hpp \
    wz4Lib.hpp

SOURCES += \
    wz4Types.cpp \
    wz4Mesh.cpp \
    wz4Anim.cpp \
    wz4Bitmap.cpp \
    wz4Math.cpp \
    wz4Bspline.cpp \
    wz4Types2.cpp
