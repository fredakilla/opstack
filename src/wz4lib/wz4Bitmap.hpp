#ifndef WZ4BITMAP_HPP
#define WZ4BITMAP_HPP

#include "wz4Types.hpp"
#include "wz4Math.hpp"


namespace wz4 {

#define BI_ADD        0
#define BI_SUB        1
#define BI_MUL        2
#define BI_DIFF       3
#define BI_ALPHA      4
#define BI_MULCOL     5
#define BI_ADDCOL     6
#define BI_SUBCOL     7
#define BI_GRAY       8
#define BI_INVERT     9
#define BI_SCALECOL   10
#define BI_MERGE      11
#define BI_BRIGHTNESS 12
#define BI_SUBR       13
#define BI_MULMERGE   14
#define BI_SHARPEN    15

#define BI_HARDLIGHT  0x10
#define BI_OVER       0x11
#define BI_ADDSMOOTH  0x12
#define BI_MIN        0x13
#define BI_MAX        0x14

#define BI_RANGE      0x15

void xInitPerlin();

struct GenBitmapGradientPoint
{
    sF32 Pos;
    sVector4 Color;
};


class Wz4Bitmap
{
public:
    sU64*   Data;      // the bitmap itself
    sInt    XSize;     // xsize
    sInt    YSize;     // ysize
    sInt    Size;      // xsize*ysize, saves some bytes of code for common loops

    Wz4Bitmap();
    ~Wz4Bitmap();

    void    Init(sInt x,sInt y);
    void    InitSize(Wz4Bitmap *s)      { Init(s->XSize,s->YSize); }
    sBool   Incompatible(Wz4Bitmap *b)  { return XSize!=b->XSize || YSize!=b->YSize; }
    void    Loop(sInt mode,Wz4Bitmap *srca,Wz4Bitmap *srcb);
    void    Loop(sInt mode,sU64 *srca,Wz4Bitmap *srcb);

    void    CopyFrom(Wz4Bitmap *);
    void    CopyTo(sU8* bbuffer);
    //void  CopyTo(video::ITexture *);

    // generators

    void    Flat(sU32 color);
    void    Checker(sU32 col0,sU32 col1,sInt maskx,sInt masky);
    //void  Gradient(GenBitmapGradientPoint *,sInt count,sInt flags);
    void    Perlin(sInt freq,sInt oct,sF32 fadeoff,sInt seed,sInt mode,sF32 amp,sF32 gamma,sU32 col0,sU32 col1);
    void    GlowRect(sF32 cx,sF32 cy,sF32 rx,sF32 ry,sF32 sx,sF32 sy,sU32 color,sF32 alpha,sF32 power,sInt wrap,sInt flags);
    void    Dots(sU32 color0,sU32 color1,sInt count,sInt seed);
    void    Cell(sU32 col0,sU32 col1,sU32 col2,sInt max,sInt seed,sF32 amp,sF32 gamma,sInt mode,sF32 mindistf,sInt percent,sF32 aspect);
    void    Bricks(sInt bmxs,sInt bmys,sInt color0,sInt color1,sInt colorf,sF32 ffugex,sF32 ffugey,sInt tx,sInt ty,sInt seed,sInt heads,sInt flags,sF32 side,sF32 colorbalance);

    // filters

    void    Color(sInt mode,sU32 col);
    void    Merge(sInt mode,Wz4Bitmap *other);
    void    HSCB(sF32 fh,sF32 fs,sF32 fc,sF32 fb);
    void    ColorBalance(sVector30 shadows, sVector30 midtones,sVector30 highlights);
    void    Blur(sInt flags,sF32 sx,sF32 sy,sF32 _amp);
    void    Sharpen(Wz4Bitmap *in,sInt order,sF32 sx,sF32 sy,sF32 amp);

    // sample

    void    Rotate(Wz4Bitmap *in,sF32 cx,sF32 cy,sF32 angle,sF32 sx,sF32 sy,sF32 tx,sF32 ty,sInt border);
    void    RotateMul(sF32 cx,sF32 cy,sF32 angle,sF32 sx,sF32 sy,sF32 tx,sF32 ty,sInt border,sU32 color,sInt mode,sInt count,sU32 fade);
    void    Twirl(Wz4Bitmap *src,sF32 strength,sF32 gamma,sF32 rx,sF32 ry,sF32 cx,sF32 cy,sInt border);
    void    Distort(Wz4Bitmap *src,Wz4Bitmap *map,sF32 dist,sInt border);
    void    Normals(Wz4Bitmap *src,sF32 _dist,sInt mode);
    void    Unwrap(Wz4Bitmap *src,sInt mode);
    void    Bulge(Wz4Bitmap *src,sF32 f);

    // special

    void    PreMulAlpha();
    void    Downsample(Wz4Bitmap *in,sInt flags);
    void    Bump(Wz4Bitmap *bb,sInt subcode,sF32 px,sF32 py,sF32 pz,sF32 da,sF32 db,sU32 _diff,sU32 _ambi,sF32 outer,sF32 falloff,sF32 amp,sU32 _spec,sF32 spow,sF32 samp);

    // OPS origin

    void    BitCrusher(sF32 steps, sF32 phase);
    void    Range(Wz4Bitmap *in, sInt mode, sU32 color0, sU32 color1);
    void    Mask(Wz4Bitmap *out, Wz4Bitmap *in0,Wz4Bitmap *in1,Wz4Bitmap *in2, sInt mode);

    // POC

    void    DistanceField(sInt threshold, sInt width, sInt scale);

private:
    void            FreeData();
    void            AllocateData(sU32 size);
    static sU64     GetColor64(sU32 c);
};

} // end namespace wz4

#endif // WZ4BITMAP_HPP

