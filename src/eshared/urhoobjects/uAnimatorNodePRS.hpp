#ifndef U_ANIMATORNODE_HPP
#define U_ANIMATORNODE_HPP

#include "eshared.hpp"

//!----------------------------------------------------------------------------------------------------------------------
//!
//! Logic component used to animate Node transformation (pos, rotate, scale)
//!
//!----------------------------------------------------------------------------------------------------------------------
class uAnimatorNodePRS : public LogicComponent
{
    URHO3D_OBJECT(uAnimatorNodePRS, LogicComponent)

public:
    uAnimatorNodePRS(Context* context) :
        LogicComponent(context),
        m_position(nullptr),
        m_rotate(nullptr),
        m_scale(nullptr)
    {
        // Only the scene update event is needed: unsubscribe from the rest for optimization
        SetUpdateEventMask(USE_UPDATE);

        URHO3D_ATTRIBUTE("Para", VariantMap, m_vmap, 0, AM_DEFAULT);
    }

    void ApplyAttributes()
    {
        m_position = (eParameter*)m_vmap["Position"].GetVoidPtr();
        m_rotate = (eParameter*)m_vmap["Rotate"].GetVoidPtr();
        m_scale = (eParameter*)m_vmap["Scale"].GetVoidPtr();

        eASSERT(m_position);
        eASSERT(m_rotate);
        eASSERT(m_scale);

    }

    // Init parameters with parameters array of operators
    void Init(const eArray<eParameter *> &params)
    {
        for(eU32 i=0U; i<params.size(); i++)
        {
            String paraName = params[i]->getName().c_str();
            m_vmap[paraName] = (void*)params[i];
        }

        ApplyAttributes();
    }

    void Update(float timeStep)
    {
        /*Vector3 t(m_translate->getAnimValue().fxyz);
        Quaternion r(m_rotate->getAnimValue().fxyz*e360);
        Vector3 s(m_scale->getAnimValue().fxyz);
        Matrix3x4 m = node_->GetTransform() * Matrix3x4(t,r,s);
        node_->SetTransform(m.Translation(), m.Rotation(), m.Scale());*/

        node_->SetTransform(m_position->getAnimValue().fxyz,
                            m_rotate->getAnimValue().fxyz*e360,
                            m_scale->getAnimValue().fxyz);
    }

private:
    VariantMap  m_vmap;         // map of all eParameters from Node operator
    eParameter* m_position;
    eParameter* m_rotate;
    eParameter* m_scale;
};

#endif // U_ANIMATORNODE_HPP
