#ifndef U_COMPONENT_WZ4MESH_HPP
#define U_COMPONENT_WZ4MESH_HPP

#include "eshared.hpp"

//!----------------------------------------------------------------------------------------------------------------------
//!
//! Static model component builed from a Wz4Mesh.
//!
//!----------------------------------------------------------------------------------------------------------------------
class URHO3D_API uWz4MeshComponent : public StaticModel
{
    URHO3D_OBJECT(uWz4MeshComponent, StaticModel)

public:
    /// Construct.
    uWz4MeshComponent(Context* context);
    /// Destruct.
    virtual ~uWz4MeshComponent();
    /// Register object factory. StaticModel must be registered first.
    static void RegisterObject(Context* context);
    /// Convert a Wz4Mesh to an Urho3D Model with solid geometry.
    void ChargeSolidMesh(wz4::Wz4Mesh* wz4mesh);
    /// Convert a Wz4Mesh to an Urho3D Model with wireframe geometry for mesh edit mode.
    void ChargeWireMesh(wz4::Wz4Mesh *wz4mesh);
};


#endif // U_COMPONENT_WZ4MESH_HPP
