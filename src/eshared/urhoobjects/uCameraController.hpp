#ifndef U_CAMERACONTROLLER_HPP
#define U_CAMERACONTROLLER_HPP

#include "eshared.hpp"

//!----------------------------------------------------------------------------------------------------------------------
//!
//! Get mouse and keyboard to move a node.
//! Ideal for camera nodes.
//!
//!----------------------------------------------------------------------------------------------------------------------
class uCameraController : public LogicComponent
{
    URHO3D_OBJECT(uCameraController, LogicComponent)    

public:
    uCameraController(Context* context) :
        LogicComponent(context),
        yaw_(0.0f),
        pitch_(0.0f)
    {
        // Only the scene update event is needed: unsubscribe from the rest for optimization
        SetUpdateEventMask(USE_UPDATE);
    }

    void Update(float timeStep)
    {
        // Use timeStep from Time subsystem instead of Scene timeStep
        // because scene time can be paused in editor
        Time* time = GetSubsystem<Time>();
        timeStep = time->GetTimeStep();

        Input* input = GetSubsystem<Input>();

        // Movement speed as world units per second
        const float MOVE_SPEED = 20.0f;
        // Mouse sensitivity as degrees per pixel
        const float MOUSE_SENSITIVITY = 0.1f;

        // Use this frame's mouse motion to adjust camera node yaw and pitch. Clamp the pitch between -90 and 90 degrees
        IntVector2 mouseMove = input->GetMouseMove();
        yaw_ += MOUSE_SENSITIVITY * mouseMove.x_;
        pitch_ += MOUSE_SENSITIVITY * mouseMove.y_;
        pitch_ = Clamp(pitch_, -90.0f, 90.0f);

        // Construct new orientation for the camera scene node from yaw and pitch. Roll is fixed to zero
        node_->SetRotation(Quaternion(pitch_, yaw_, 0.0f));       

        // Read WASD keys and move the camera scene node to the corresponding direction if they are pressed
        // Use the Translate() function (default local space) to move relative to the node's orientation.
        if (input->GetKeyDown('W'))
            node_->Translate(Vector3::FORWARD * MOVE_SPEED * timeStep);
        if (input->GetKeyDown('S'))
            node_->Translate(Vector3::BACK * MOVE_SPEED * timeStep);
        if (input->GetKeyDown('A'))
            node_->Translate(Vector3::LEFT * MOVE_SPEED * timeStep);
        if (input->GetKeyDown('D'))
            node_->Translate(Vector3::RIGHT * MOVE_SPEED * timeStep);

    }

private:

    /// Camera yaw angle.
    float yaw_;
    /// Camera pitch angle.
    float pitch_;
};

#endif // U_CAMERACONTROLLER_HPP
