#include "uWz4Meshcomponent.hpp"

VertexElementType __getVertexElementType(const eU32 descElement)
{
    VertexElementType type = MAX_VERTEX_ELEMENT_TYPES;

    const eU32 descType = descElement & wz4::sVF_TYPEMASK;
    switch(descType)
    {
    case wz4::sVF_F1:
        type = TYPE_FLOAT;
        break;
    case wz4::sVF_F2:
        type = TYPE_VECTOR2;
        break;
    case wz4::sVF_F3:
        type = TYPE_VECTOR3;
        break;
    case wz4::sVF_F4:
        type = TYPE_VECTOR4;
        break;
    case wz4::sVF_I4:
        type = TYPE_UBYTE4;
        break;
    case wz4::sVF_C4:
        type = TYPE_UBYTE4_NORM;
        break;
    default:
        sVERIFYFALSE; // unknown vertex element type
    }

    return type;
}

VertexElementSemantic __getVertexElementSemantic(const eU32 descElement)
{
    VertexElementSemantic semantic = MAX_VERTEX_ELEMENT_SEMANTICS;

    const eU32 descUse = descElement & wz4::sVF_USEMASK;
    switch(descUse)
    {
    case wz4::sVF_POSITION:
        semantic = SEM_POSITION;
        break;
    case wz4::sVF_NORMAL:
        semantic = SEM_NORMAL;
        break;
    case wz4::sVF_TANGENT:
        semantic = SEM_TANGENT;
        break;
    case wz4::sVF_COLOR0:
    case wz4::sVF_COLOR1:
    case wz4::sVF_COLOR2:
    case wz4::sVF_COLOR3:
        semantic = SEM_COLOR;
        break;
    case wz4::sVF_UV0:
    case wz4::sVF_UV1:
    case wz4::sVF_UV2:
    case wz4::sVF_UV3:
    case wz4::sVF_UV4:
    case wz4::sVF_UV5:
    case wz4::sVF_UV6:
    case wz4::sVF_UV7:
        semantic = SEM_TEXCOORD;
        break;
    default:
        sVERIFYFALSE; // unknown vertex element semantic
    }

    return semantic;
}

void __getVertexElements(const eU32* vertexDescription, PODVector<VertexElement>& elements)
{
    const eU32 *desc = vertexDescription;
    while(*desc)
    {
        const eU32 descElement = *desc;

        VertexElementType type = __getVertexElementType(descElement);
        eASSERT(type != MAX_VERTEX_ELEMENT_TYPES);

        VertexElementSemantic semantic = __getVertexElementSemantic(descElement);
        eASSERT(semantic != MAX_VERTEX_ELEMENT_SEMANTICS);

        elements.Push(VertexElement(type, semantic));

        desc++;
    }
}

uWz4MeshComponent::uWz4MeshComponent(Context* context) :
    StaticModel(context)
{
}

uWz4MeshComponent::~uWz4MeshComponent()
{
}

void uWz4MeshComponent::RegisterObject(Context* context)
{
    context->RegisterFactory<uWz4MeshComponent>("Geometry");
    URHO3D_COPY_BASE_ATTRIBUTES(StaticModel);
}

void uWz4MeshComponent::ChargeWireMesh(wz4::Wz4Mesh *wz4mesh)
{
    // vertex description
    static const eU32 vertexDescription[] =
    {
        wz4::sVF_POSITION | wz4::sVF_F3,
        wz4::sVF_COLOR0 | wz4::sVF_F4,
        wz4::sVF_END,
    };

    // get VertexElement from vertex description
    PODVector<VertexElement> elements;
    __getVertexElements(vertexDescription, elements);
    eASSERT(elements.Size() > 0);

    wz4mesh->ChargeWire(vertexDescription);

    // compute bounding box
    wz4::sAABBox boudingBox;
    wz4mesh->CalcBBox(boudingBox);
    boundingBox_ = boudingBox;

    // create model to hold geometries
    model_.Reset();
    if(!model_)
        model_ = new Model(context_);

    // set number of geometries (lines, faces, vertices)
    unsigned geoCount = 1U;
    if(wz4mesh->WireGeoFaces)
        geoCount++;
    if(wz4mesh->WireGeoVertex)
        geoCount++;
    SetNumGeometries(geoCount);

    model_->SetNumGeometries(geoCount);
    model_->SetBoundingBox(boudingBox);


    int geoIndex = 0;

    // Lines (Always set on)
    {
        unsigned numVertices = wz4mesh->WireGeoLines->_vertexCount;
        unsigned numIndices = wz4mesh->WireGeoLines->_indexCount;

        VertexBuffer* vb = new VertexBuffer(context_);
        vb->SetShadowed(true);
        vb->SetSize(numVertices, elements);
        vb->SetData(wz4mesh->WireGeoLines->_vbo);

        IndexBuffer* ib = new IndexBuffer(context_);
        ib->SetShadowed(true);
        ib->SetSize(numIndices, true);
        ib->SetData(wz4mesh->WireGeoLines->_ebo);

        SharedPtr<Geometry> geometry(new Geometry(context_));
        geometry->SetVertexBuffer(0, vb);
        geometry->SetIndexBuffer(ib);
        geometry->SetDrawRange(LINE_LIST, 0, numIndices, 0, numVertices);

        geometries_[geoIndex][0] = geometry;

        model_->SetGeometry(geoIndex, 0, geometries_[geoIndex][0]);
        model_->SetNumGeometryLodLevels(geoIndex, 1);

        geoIndex++;
    }


    // Selected faces
    if(wz4mesh->WireGeoFaces)
    {
        unsigned numVertices = wz4mesh->WireGeoFaces->_vertexCount;
        unsigned numIndices = wz4mesh->WireGeoFaces->_indexCount;

        VertexBuffer* vb = new VertexBuffer(context_);
        vb->SetShadowed(true);
        vb->SetSize(numVertices, elements);
        vb->SetData(wz4mesh->WireGeoFaces->_vbo);

        IndexBuffer* ib = new IndexBuffer(context_);
        ib->SetShadowed(true);
        ib->SetSize(numIndices, true);
        ib->SetData(wz4mesh->WireGeoFaces->_ebo);

        SharedPtr<Geometry> geometry(new Geometry(context_));
        geometry->SetVertexBuffer(0, vb);
        geometry->SetIndexBuffer(ib);
        geometry->SetDrawRange(TRIANGLE_LIST, 0, numIndices, 0, numVertices);

        geometries_[geoIndex][0] = geometry;

        model_->SetGeometry(geoIndex, 0, geometries_[geoIndex][0]);
        model_->SetNumGeometryLodLevels(geoIndex, 1);

        geoIndex++;
    }


    // Selected vertices
    if(wz4mesh->WireGeoVertex)
    {
        unsigned numVertices = wz4mesh->WireGeoVertex->_vertexCount;
        unsigned numIndices = wz4mesh->WireGeoVertex->_indexCount;

        VertexBuffer* vb = new VertexBuffer(context_);
        vb->SetShadowed(true);
        vb->SetSize(numVertices, elements);
        vb->SetData(wz4mesh->WireGeoVertex->_vbo);

        SharedPtr<Geometry> geometry(new Geometry(context_));
        geometry->SetVertexBuffer(0, vb);
        geometry->SetDrawRange(POINT_LIST, 0, numIndices, 0, numVertices);

        geometries_[geoIndex][0] = geometry;

        model_->SetGeometry(geoIndex, 0, geometries_[geoIndex][0]);
        model_->SetNumGeometryLodLevels(geoIndex, 1);

        geoIndex++;
    }

    ResetLodLevels();


    // Set material for all geometries (Lines, Vertices, Faces)

    ResourceCache* cache = GetSubsystem<ResourceCache>();
    SharedPtr<Material> material(new Material(context_));
    material->SetTechnique(0, cache->GetResource<Technique>("Techniques/NoTextureUnlitVCol.xml"));
    material->SetCullMode(CULL_NONE);

    for (unsigned i = 0; i < batches_.Size(); ++i)
        batches_[i].material_ = material;
}

void uWz4MeshComponent::ChargeSolidMesh(wz4::Wz4Mesh *wz4mesh)
{
    // vertex description
    static const eU32 vertexDescription[] =
    {
        wz4::sVF_POSITION | wz4::sVF_F3,
        wz4::sVF_NORMAL | wz4::sVF_F3,
        wz4::sVF_TANGENT | wz4::sVF_F3,
        wz4::sVF_UV0 | wz4::sVF_F2,
        wz4::sVF_END,
    };

    // get VertexElement from vertex description
    PODVector<VertexElement> elements;
    __getVertexElements(vertexDescription, elements);
    eASSERT(elements.Size() > 0);

    ResourceCache* cache = GetSubsystem<ResourceCache>();

    /*SharedPtr<Material> material(new Material(context_));
    material->SetTechnique(0, cache->GetResource<Technique>("Techniques/NoTextureUnlitVCol.xml"));
    material->SetCullMode(CULL_NONE);*/

    // default material
    Material* material = cache->GetResource<Material>("Materials/DefaultGrey.xml");



    // charge solid
    wz4mesh->ChargeSolid(1, vertexDescription);

    eInt clusterCount = wz4mesh->Clusters.GetCount();



    // copy geometry ref to model
    // model is the geometry container that will be used when cloning node components
    // See: StaticModel::SetModel()
    model_.Reset();
    if(!model_)
        model_ = new Model(context_);
    model_->SetNumGeometries(clusterCount);
    //model_->SetNumGeometryLodLevels(0, 1);
    //model_->SetGeometry(0, 0, geometryLodLevels[0]);
    //model_->SetBoundingBox(boudingBox);
    //model_->SetName("Wz4Mesh"); // Important for cache











    // allocate internal memory
    SetNumGeometries(clusterCount);

   /* unsigned numLodLevels = 1;
    Vector<SharedPtr<Geometry> > geometryLodLevels;
    geometryLodLevels.Reserve(numLodLevels);*/

    for(int i=0; i<clusterCount; i++)
    {
        if(wz4mesh->Clusters[i]->Geo[0])
        {

        SharedPtr<Geometry> geometry(new Geometry(context_));
        geometry->SetNumVertexBuffers(clusterCount);

        unsigned numVertices = wz4mesh->Clusters[i]->Geo[0]->_vertexCount;
        unsigned numIndices = wz4mesh->Clusters[i]->Geo[0]->_indexCount;

        VertexBuffer* vb = new VertexBuffer(context_);
        vb->SetShadowed(true);
        vb->SetSize(numVertices, elements);
        vb->SetData(wz4mesh->Clusters[i]->Geo[0]->_vbo);

        bool isLargeIndex = wz4mesh->Clusters[i]->IndexSize == wz4::sGF_INDEX16 ? false : true;

        IndexBuffer* ib = new IndexBuffer(context_);
        ib->SetShadowed(true);
        ib->SetSize(numIndices, isLargeIndex);
        ib->SetData(wz4mesh->Clusters[i]->Geo[0]->_ebo);

        geometry->SetVertexBuffer(0, vb);
        geometry->SetIndexBuffer(ib);
        geometry->SetDrawRange(TRIANGLE_LIST, 0, numIndices, 0, numVertices);

        //geometryLodLevels.Push(geometry);
        geometries_[i][0] = geometry;


        // apply cluster material or default material
        if(wz4mesh->Clusters[i]->UrhoMat)
        {
            // cluster had a material
            batches_[i].material_ = wz4mesh->Clusters[i]->UrhoMat;
            batches_[i].material_->AddRef();
        }
        else
        {
            // apply default material
            batches_[i].material_ = material;
        }

        // set world transform
        const Matrix3x4* worldTransform = node_ ? &node_->GetWorldTransform() : (const Matrix3x4*)0;
        batches_[i].worldTransform_ = worldTransform;

        //batches_[i].material_ = material;
        /*if(wz4mesh->Clusters[0]->UrhoMat)
            batches_[i].material_ = wz4mesh->Clusters[0]->UrhoMat;
        else
        {
            if(i < batches_.Size() )
                batches_[i].material_ = material;
            else
                batches_[0].material_ = material;
        }*/

        model_->SetNumGeometryLodLevels(i, 1);
        model_->SetGeometry(i, 0, geometry);

        }

    }

    //geometries_[0] = geometryLodLevels;
    ResetLodLevels();


    // compute bounding box
    wz4::sAABBox boudingBox;
    wz4mesh->CalcBBox(boudingBox);
    boundingBox_ = boudingBox;

    /*// copy geometry ref to model
    // model is the geometry container that will be used when cloning node components
    // See: StaticModel::SetModel()
    model_.Reset();
    if(!model_)
        model_ = new Model(context_);
    model_->SetNumGeometries(clusterCount);
    model_->SetNumGeometryLodLevels(0, 1);
    model_->SetGeometry(0, 0, geometryLodLevels[0]);
    model_->SetBoundingBox(boudingBox);
    model_->SetName("Wz4Mesh"); // Important for cache*/


    model_->SetBoundingBox(boudingBox);
    model_->SetName(String("Wz4Mesh")); // Important for cache

    // Add model to cache to allow Clone
    //ResourceCache* cache = eGraphics::m_gcontext->GetSubsystem<ResourceCache>();
    cache->AddManualResource(model_);

    MarkNetworkUpdate();    
}
