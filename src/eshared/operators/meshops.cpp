/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   This file is part of
 *       ______        _                             __ __
 *      / ____/____   (_)____ _ ____ ___   ____ _   / // /
 *     / __/  / __ \ / // __ `// __ `__ \ / __ `/  / // /_
 *    / /___ / / / // // /_/ // / / / / // /_/ /  /__  __/
 *   /_____//_/ /_//_/ \__, //_/ /_/ /_/ \__,_/     /_/.
 *                    /____/
 *
 *   Copyright � 2003-2012 Brain Control, all rights reserved.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#if defined(eEDITOR)
    #define NOMINMAX
    #include <stdio.h>
#endif

#include "eshared.hpp"
#include "baseoperators/imesh.hpp"
using namespace wz4;


static sBool logic(sInt selflag,sF32 select)
{
    switch(selflag)
    {
    default:
        return 1;
    case 1:
        return 0;
    case 2:
        return select>=0.5f;
    case 3:
        return select<=0.5f;
    }
}

static sF32 logicF(sInt selflag,sF32 select)
{
    if (!(selflag&0x04)) return logic(selflag, select)?1.0f:0.0f;

    switch(selflag&0x03)
    {
    default:
        return 1;
    case 1:
        return 0;
    case 2:
        return select;
    case 3:
        return 1.0f-select;
    }
}



eIOpMeshRenderer::eIOpMeshRenderer()
{
    // create a scene node to hold a wz4Mesh component
    m_node = getRootNode()->CreateChild("NodeWz4MeshPreview");
    m_node->SetPosition(Vector3(0.0f, 0.0f, 0.0f));
    m_node->SetScale(1.0f);

    // create a wz4MeshComponent
    m_staticModel = m_node->CreateComponent<uWz4MeshComponent>();

    // create a light facing the same camera's direction
   /* Node* dirLightNode = getDefaultCamera()->CreateChild("DirLightNode");
    dirLightNode->SetDirection( getDefaultCamera()->GetDirection() );
    Light* light = dirLightNode->CreateComponent<Light>();
    light->SetLightType(LIGHT_DIRECTIONAL);
    light->SetBrightness(1.0);
    light->SetColor(Color(1,1,1,1));*/


    //m_staticModel = m_node->CreateComponent<StaticModel>();

    /*ResourceCache* cache = GetSubsystem<ResourceCache>();
    //m_renderMaterial = new Material(context_);
    //m_renderMaterial->SetTechnique(0, cache->GetResource<Technique>("Techniques/Diff.xml"));
    m_renderMaterial = cache->GetResource<Material>("Materials/Stone.xml");*/


    // Create a wz4 checker texture used in background viewport (to )
   /* wz4::Wz4Bitmap gb;
    gb.Init(256,256);
    gb.Flat(0xFFFFFFFF);
    gb.Checker(0xFF111111, 0xFFFFFFFF, 16, 16);

    Texture2D * decalTex = new Texture2D(context_);
    decalTex->SetSize(256,256, Graphics::GetRGBAFormat(), TextureUsage::TEXTURE_DYNAMIC);
    unsigned char* imageData = new unsigned char[256*256*4];
    //nMemCopy(imageData, gb.Data, gb.Size*sizeof(uint8)*4);
    gb.CopyTo((eU8*)imageData);
    decalTex->SetData(0, 0, 0, 256, 256, imageData);

    decalTex->SetFilterMode(TextureFilterMode::FILTER_NEAREST);
    decalTex->SetAddressMode(TextureCoordinate::COORD_U, TextureAddressMode::ADDRESS_MIRROR);

    ResourceCache* cache = GetSubsystem<ResourceCache>();
    m_renderMaterial = new Material(context_);
    m_renderMaterial->SetTechnique(0, cache->GetResource<Technique>("Techniques/Diff.xml"));
    m_renderMaterial->SetTexture(TU_DIFFUSE, decalTex);

    SharedPtr<Material> material(new Material(context_));
    material->SetTechnique(0, cache->GetResource<Technique>("Techniques/NoTextureUnlitVCol.xml"));
    m_wireMaterial = material;
    m_wireMaterial->SetCullMode(CULL_NONE);*/



}

eIOpMeshRenderer::~eIOpMeshRenderer()
{
}

void eIOpMeshRenderer::init()
{
}


#if 0
void eWz4MeshOpRenderer::computeMesh()
{



    Wz4MeshComponent* mm = m_node->CreateComponent<Wz4MeshComponent>();
    mm->SetMesh(m_meshToRender);

    //m_fromScratchModel = mm->GetModel();

    return;


    /*if(0)
    {
        m_meshToRender->ChargeSolid(1);

        unsigned numVertices = m_meshToRender->Clusters[0]->Geo[0]->_vertexCount;
        unsigned numIndices = m_meshToRender->Clusters[0]->Geo[0]->_indexCount;


        // Shadowed buffer needed for raycasts to work, and so that data can be automatically restored on device loss
        m_vb->SetShadowed(true);

        //vb->SetSize(numVertices, MASK_POSITION|MASK_NORMAL);
        m_vb->SetSize(numVertices, MASK_POSITION | MASK_TEXCOORD1);
        m_vb->SetData(m_meshToRender->Clusters[0]->Geo[0]->_vbo);

        bool largeIndex = m_meshToRender->Clusters[0]->IndexSize == wz4::sGF_INDEX16 ? false : true;

        m_ib->SetShadowed(true);
        m_ib->SetSize(numIndices, largeIndex);
        m_ib->SetData(m_meshToRender->Clusters[0]->Geo[0]->_ebo);

        m_geom->SetVertexBuffer(0, m_vb);
        m_geom->SetIndexBuffer(m_ib);
        m_geom->SetDrawRange(TRIANGLE_LIST, 0, numIndices, 0, numVertices);

        m_fromScratchModel->SetNumGeometries(1);
        m_fromScratchModel->SetGeometry(0, 0, m_geom);

        // compute bounding box
        wz4::sAABBox boudingBox;
        m_meshToRender->CalcBBox(boudingBox);
        m_fromScratchModel->SetBoundingBox(boudingBox);
    }
    else
    {
        m_meshToRender->ChargeWire(0);

        unsigned numVertices = m_meshToRender->WireGeoLines->_vertexCount;
        unsigned numIndices = m_meshToRender->WireGeoLines->_indexCount;

        m_vb->SetShadowed(true);

        PODVector<VertexElement> elements;
        elements.Push(VertexElement(TYPE_VECTOR3, SEM_POSITION));
        elements.Push(VertexElement(TYPE_VECTOR3, SEM_NORMAL));
        elements.Push(VertexElement(TYPE_UBYTE4, SEM_COLOR));
        m_vb->SetSize(numVertices, elements);
        //m_vb->SetSize(numVertices, MASK_POSITION | MASK_NORMAL | MASK_COLOR );

        m_vb->SetData(m_meshToRender->WireGeoLines->_vbo);


        m_ib->SetShadowed(true);
        m_ib->SetSize(numIndices, true);
        m_ib->SetData(m_meshToRender->WireGeoLines->_ebo);

        m_geom->SetVertexBuffer(0, m_vb);
        m_geom->SetIndexBuffer(m_ib);
        m_geom->SetDrawRange(LINE_LIST, 0, numIndices, 0, numVertices);

        m_fromScratchModel->SetNumGeometries(1);
        m_fromScratchModel->SetGeometry(0, 0, m_geom);

        // compute bounding box
        wz4::sAABBox boudingBox;
        m_meshToRender->CalcBBox(boudingBox);
        m_fromScratchModel->SetBoundingBox(boudingBox);
    }*/

}
#endif



void eIOpMeshRenderer::operatorUpdated(eIOperator* op)
{
    eASSERT(op->getResultClass() == eOC_MESH);

    wz4::Wz4Mesh* mesh = ((eIOpMesh*)op)->getResult().mesh;  


    //Wz4MeshComponent* tmpWz4MeshComponent = m_node->CreateComponent<Wz4MeshComponent>();
    /*Wz4MeshComponent* tmpWz4MeshComponent = new Wz4MeshComponent(context_);
    tmpWz4MeshComponent->SetMesh(mesh);

    // update staticmodel with new mesh
    m_staticModel->SetModel(tmpWz4MeshComponent->GetModel());
    m_staticModel->SetMaterial(m_renderMaterial);*/




   /* m_node->RemoveAllComponents();
    Wz4MeshComponent* wz = m_node->CreateComponent<Wz4MeshComponent>();
    wz->SetMesh(mesh);
    wz->SetMaterial(m_renderMaterial);*/

    //m_staticModel2->SetMesh(mesh);
    //m_staticModel2->SetMaterial(m_renderMaterial);

    m_staticModel->ChargeWireMesh(mesh);
    //m_staticModel2->SetMaterial(m_wireMaterial);




    // add node to scene
    //m_node->SetParent(getDefaultScene());
    m_node->SetParent(getRootNode());

    //tmpWz4MeshComponent->Remove();
    //delete tmpWz4MeshComponent;
    ePRINT("nbr comp = %d", m_node->GetComponents().Size());


}







































//!----------------------------------------------------------------------------------------------------------------------
//! Cube (mesh) operator
//!
//! Generate cube mesh.
//!----------------------------------------------------------------------------------------------------------------------
eOP_DEF_MESH(eCubeOp, "Cube", 'c', 0, 0, eOP_INPUTS())

    eParameter* m_tesselate;
    eParameter* m_scale;

    eOP_INIT()
    {
        eOP_PAR_IXYZ(m_tesselate, "Tesselate", 1, 4096, 1, 1, 1);
        eOP_PAR_FXYZ(m_scale, "Scale", eF32_MIN, eF32_MAX, 1.0f, 1.0f, 1.0f);        
    }

    eOP_DEINIT()
    {
        eSAFE_DELETE(m_mesh);
    }

    eOP_EXEC()
    {
        eIXYZ tesselate = m_tesselate->getBaseValue().ixyz;
        eFXYZ scale = m_scale->getBaseValue().fxyz;

        //eSAFE_DELETE(m_mesh);

        // generate cube mesh
        //m_mesh = new wz4::Wz4Mesh();
        m_mesh->MakeCube(tesselate.x, tesselate.y, tesselate.z);



        // scale and center mesh
        sMatrix34 mat;
        sF32 sx = scale.x;
        sF32 sy = scale.y;
        sF32 sz = scale.z;
        mat.i.Init(sx/tesselate.x,0,0);
        mat.j.Init(0,sy/tesselate.y,0);
        mat.k.Init(0,0,sz/tesselate.z);
        mat.l.Init(-sx*0.5f,-sy*0.5f,-sz*0.5f);
        m_mesh->Transform(mat);

    }

eOP_END(eCubeOp)



//!----------------------------------------------------------------------------------------------------------------------
//! Select (mesh) operator
//!
//! Select vertices or faces in mesh.
//!----------------------------------------------------------------------------------------------------------------------
eOP_DEF_MESH(OpMeshSelect, "Select", 's', 1, 1, eOP_INPUTS(eOC_MESH))

    eParameter* m_inputType;
    eParameter* m_outputType;
    eParameter* m_position;
    eParameter* m_rotate;
    eParameter* m_scale;
    eParameter* m_random;
    eParameter* m_randomSeed;
    eParameter* m_cluster;
    eParameter* m_logic;
    eParameter* m_invert;

    enum E_INPUT_TYPE
    {
        EIT_CUBE        = 0,
        EIT_SPHERE      = 1,
        EIT_VERTEX      = 2,
        EIT_FACE        = 3,
        EIT_ALL         = 4,
        EIT_TEXTURE     = 5,
        EIT_CLUSTER     = 6,
        EIT_SLOT        = 7
    };

    enum E_OUTPUT_TYPE
    {
        EOT_INNERVERTEX     = 0,
        EOT_TOUCHEDFACE     = 1,
        EOT_ENCLOSEDFACE    = 2,
        EOT_FULLVERTEX      = 3
    };

    enum E_SELECT_LOGIC
    {
        ESL_ASSIGN  = 0,    // Assign current selection to mesh and clear previous existing selection
        ESL_SET     = 1,    // Add current selection to existing selection
        ESL_CLEAR   = 2     // Clear current selection from existing selection
    };

    eOP_INIT()
    {
        eParameter* label;

        eOP_PAR_ENUM(m_inputType, "InputType", "Cube|Sphere|Vertex|Face|All|Texture|Cluster|Slot", 0);
        eOP_PAR_ENUM(m_outputType, "OutputType", "InnerVertex|TouchedFace|EnclosedFace|FullVertex", 0);

        eOP_PAR_LABEL(label, "Label", "Cube or Sphere Transformation");

        eOP_PAR_FXYZ(m_position, "Position", eF32_MIN, eF32_MAX, 0.0f, 0.0f, 0.0f);
        eOP_PAR_FXYZ(m_rotate, "Rotate", eF32_MIN, eF32_MAX, 0.0f, 0.0f, 0.0f);        
        eOP_PAR_FXYZ(m_scale, "Scale", eF32_MIN, eF32_MAX, 1.0f, 1.0f, 1.0f);

        eOP_PAR_LABEL(label, "Label", "Random Selection");
        eOP_PAR_FLOAT(m_random, "Random", 0.0f, 1.0f, 1.0f);
        eOP_PAR_INT(m_randomSeed, "RandomSeed", 0, 255, 0);

        eOP_PAR_LABEL(label, "Label", "Cluster Index");
        eOP_PAR_INT(m_cluster, "Cluster", 0, 1024, 0);


        eOP_PAR_LABEL(label, "Label", "Selection Logic");
        eOP_PAR_ENUM(m_logic, "Mode", "Assign|Set|Clear", 0);
        eOP_PAR_BOOL(m_invert, "Invert", eFALSE);

        setShowDebug(eTRUE);
    }



    eOP_DEINIT()
    {
        eSAFE_DELETE(m_mesh);
    }

    eOP_EXEC()
    {
        eInt inputType = m_inputType->getBaseValue().enumSel;
        eInt outputType = m_outputType->getBaseValue().enumSel;
        eFXYZ position = m_position->getBaseValue().fxyz;
        eFXYZ rotate = m_rotate->getBaseValue().fxyz;
        eFXYZ scale = m_scale->getBaseValue().fxyz;
        eF32 random = m_random->getBaseValue().flt;
        eF32 randomSeed = m_randomSeed->getBaseValue().integer;
        eU32 cluster = m_cluster->getBaseValue().integer;
        E_SELECT_LOGIC logic = (E_SELECT_LOGIC)m_logic->getBaseValue().enumSel;
        eBool invert = m_invert->getBaseValue().enumSel;

        copyInput(0);

        sSRT srt;
        sMatrix34 mat;
        sMatrix34 inv;
        Wz4MeshVertex *vp;
        Wz4MeshFace *face;
        sVector31 v;
        sRandom rnd;

        rnd.Seed(randomSeed);

        srt.Translate = position;
        srt.Rotate = rotate;        
        srt.Scale = scale;
        srt.MakeMatrix(mat);
        srt.MakeMatrixInv(inv);

        sInt pc = m_mesh->Vertices.GetCount();

        /*sInt refR = 0x80*((para->RefColor>>16)&0xff);
           sInt refG = 0x80*((para->RefColor>>8)&0xff);
           sInt refB = 0x80*((para->RefColor>>0)&0xff);
           sInt refInner = 0x80*para->RefColorErr;
           sInt refOuter = 0x80*(para->RefColorErr+para->RefColorSpread);
           sF32 refScale = 1.0f/(0x80*para->RefColorSpread);*/

        E_INPUT_TYPE flagsInput = (E_INPUT_TYPE)inputType; //para->Flags & 0x1c0;
        E_OUTPUT_TYPE flagsOutput = (E_OUTPUT_TYPE)outputType; // (para->Flags >> 4) & 3;
        sBool selectVerts = flagsOutput==EOT_INNERVERTEX || flagsOutput==EOT_FULLVERTEX;

        sF32 *fsel = new sF32[pc];

        // Face select
        if(flagsInput == EIT_FACE)
        {
            for(sInt i=0;i<pc;i++)
                fsel[i] = 0.0f;

            sFORALL(m_mesh->Faces,face)
            {
                if(face->Select>0.0f)
                {
                    for(sInt i=0;i<face->Count;i++)
                        fsel[face->Vertex[i]]=face->Select;
                }
            }
        }

        // Cluster select
        if(flagsInput == EIT_CLUSTER)
        {
            for(sInt i=0;i<pc;i++)
                fsel[i] = 0.0f;

            sFORALL(m_mesh->Faces,face)
            {
                if(face->Cluster==cluster)
                    for(sU8 i=0;i<face->Count;i++)
                        fsel[face->Vertex[i]]= 1;
            }
        }

        sFORALL(m_mesh->Vertices,vp)
        {
            v = vp->Pos * inv;
            sF32 inside = 0.0f;
            switch(flagsInput)
            {
            case EIT_CUBE:
                inside = (v.x>=-1&&v.x<=1 && v.y>=-1&&v.y<=1 && v.z>=-1&&v.z<=1)?1.0f:0.0f;
                break;
            case EIT_SPHERE:
                inside = ((v.x*v.x+v.y*v.y+v.z*v.z)<=1)?1.0f:0.0f;
                break;
            case EIT_VERTEX:
                inside = vp->Select;
                break;
            case EIT_FACE:
            case EIT_CLUSTER:
                inside = fsel[_i];
                break;
            case EIT_ALL:
                inside = 1.0f;
                break;
            /*case 0x140:  // Texture
               {
                 GenBitmap *bmp = cmd->GetInput<GenBitmap *>(1);
                 if (!bmp) break;

                 sF32 baseu, basev;
                 if (para->TextureUV&1)
                 {
                   baseu=vp->U1;
                   basev=vp->V1;
                 }
                 else
                 {
                   baseu=vp->U0;
                   basev=vp->V0;
                 }

                 if (para->TextureUV&2)
                 {
                   baseu=sFMod(baseu,1); if (baseu<0) baseu+=1;
                   basev=sFMod(basev,1); if (basev<0) basev+=1;
                 }
                 else
                 {
                   baseu=sClamp(baseu,0.0f,0.9999f);
                   basev=sClamp(basev,0.0f,0.9999f);
                 }

                 sInt u=(sInt)(bmp->XSize*baseu);
                 sInt v=(sInt)(bmp->YSize*basev);
                 sU64 pixel=bmp->Data[bmp->XSize*v+u];
                 sInt b=(pixel&0xffff);
                 sInt g=((pixel>>16)&0xffff);
                 sInt r=((pixel>>32)&0xffff);
                 sInt err=sMax(sMax(sAbs(r-refR),sAbs(g-refG)),sAbs(b-refB));
                 if (err<refInner)
                   inside = 1.0f;
                 else if (err<refOuter)
                   inside = (refOuter-err)*refScale;
                 else
                   inside= 0.0f;
               }
               break;*/
            }

            // Logic Invert
            if(invert) //para->Flags & 1)
                inside=1.0f-inside;

            fsel[_i] = inside;
        }

        if(selectVerts)
        {
            // Vertex Selection (inner or full)

            if (flagsOutput == EOT_INNERVERTEX)
            {
                // InnerVertex

                for(sInt i=0;i<pc;i++)
                    if (rnd.Float(1)>random)
                        fsel[i] = 0;
            }
            else
            {
                // smear (full vertex)
                sInt *base = m_mesh->BasePos(1);
                for (sInt i=0;i<pc;i++)
                    fsel[base[i]] = sMax(fsel[base[i]],fsel[i]);

                for (sInt i=0;i<pc;i++)
                {
                    if (rnd.Float(1)>random)
                        fsel[i] = 0;
                    fsel[i] = fsel[base[i]];
                }
                delete[] base;
            }

            sFORALL(m_mesh->Vertices,vp)
            {
                sF32 inside = fsel[_i];
                switch(logic)//para->Flags & 12)
                {
                case ESL_CLEAR:  // clear
                    vp->Select=sMax(vp->Select-inside,0.0f);
                    break;
                case ESL_SET:  // set
                    vp->Select=sMin(vp->Select+inside,1.0f);
                    break;
                case ESL_ASSIGN:  // assign
                    vp->Select = inside;
                    break;
                }
            }
        }
        else
        {
            // Face Selection (touched or enclosed)

            sFORALL(m_mesh->Faces,face)
            {
                sInt n = 0;
                for(sInt i=0;i<face->Count;i++)
                    n += (fsel[face->Vertex[i]]>=0.5f)?1:0;
                sBool action = 0;

                switch(flagsOutput)
                {
                case EOT_TOUCHEDFACE:   // Touched Face
                    action = (n>0);
                    break;
                case EOT_ENCLOSEDFACE:  // Enclosed Face
                    action = (n==face->Count);
                    break;
                }

                if(rnd.Float(1)>random)
                    action = 0;

                switch(logic) //para->Flags & 12)
                {
                case ESL_CLEAR:  // clear
                    if(action)
                        face->Select = 0;
                    break;
                case ESL_SET:  // set
                    if(action)
                        face->Select = 1;
                    break;
                case ESL_ASSIGN:  // assign
                    face->Select = action?1:0;
                    break;
                }

            }
        }

        // face or vertex ?
        /*sInt type = 0;
           if(((para->Flags&0x1c0)!=0x1c0))
             type = (selectVerts)?wMST_VERTEX:wMST_FACE;
           if(((para->Flags&0x1c0)==0x1c0))
             type = ((para->Flags >> 4) & 1)?wMST_FACE:wMST_VERTEX;

           // load slot
           if(((para->Flags&0x1c0)==0x1c0))
             m_mesh->SelStoreLoad(wMSM_LOAD,type,para->InputSlot);

           // store slot
           if(((para->Flags&0x1c0)!=0x1c0) && para->OutputSlot>0)
             m_mesh->SelStoreLoad(wMSM_STORE,type,para->OutputSlot-1);*/


        delete[] fsel;
    }




    /*void DrawDebugBox(const Matrix3x4& transform, const Color& color, bool depthTest)
    {
      sSRT srt;
      sMatrix34 mat;
      sVector31 v[8],p;

      srt.Scale = s;
      srt.Rotate = r;
      srt.Translate = t;
      srt.MakeMatrix(mat);

      sF32 c = 1;
      p.Init(-c,-c,-c); v[0] = p*transform;
      p.Init(-c,-c, c); v[1] = p*transform;
      p.Init(-c, c, c); v[2] = p*transform;
      p.Init(-c, c,-c); v[3] = p*transform;
      p.Init( c,-c,-c); v[4] = p*transform;
      p.Init( c,-c, c); v[5] = p*transform;
      p.Init( c, c, c); v[6] = p*transform;
      p.Init( c, c,-c); v[7] = p*transform;

      unsigned uintColor = Color::RED.ToUInt(); //color.ToUInt();

      eGraphics::m_gdebuRenderer->AddLine(v[0],v[1], uintColor, depthTest);
      eGraphics::m_gdebuRenderer->AddLine(v[1],v[2], uintColor, depthTest);
      eGraphics::m_gdebuRenderer->AddLine(v[2],v[3], uintColor, depthTest);
      eGraphics::m_gdebuRenderer->AddLine(v[3],v[0], uintColor, depthTest);
      eGraphics::m_gdebuRenderer->AddLine(v[4],v[5], uintColor, depthTest);
      eGraphics::m_gdebuRenderer->AddLine(v[5],v[6], uintColor, depthTest);
      eGraphics::m_gdebuRenderer->AddLine(v[6],v[7], uintColor, depthTest);
      eGraphics::m_gdebuRenderer->AddLine(v[7],v[4], uintColor, depthTest);
      eGraphics::m_gdebuRenderer->AddLine(v[0],v[4], uintColor, depthTest);
      eGraphics::m_gdebuRenderer->AddLine(v[1],v[5], uintColor, depthTest);
      eGraphics::m_gdebuRenderer->AddLine(v[2],v[6], uintColor, depthTest);
      eGraphics::m_gdebuRenderer->AddLine(v[3],v[7], uintColor, depthTest);


    }*/


    void DrawDebugBox(const BoundingBox& box, const Matrix3x4& transform, const Color& color, bool depthTest)
    {
        const Vector3& min = box.min_;
        const Vector3& max = box.max_;

        Vector3 v0(transform * min);
        Vector3 v1(transform * Vector3(max.x_, min.y_, min.z_));
        Vector3 v2(transform * Vector3(max.x_, max.y_, min.z_));
        Vector3 v3(transform * Vector3(min.x_, max.y_, min.z_));
        Vector3 v4(transform * Vector3(min.x_, min.y_, max.z_));
        Vector3 v5(transform * Vector3(max.x_, min.y_, max.z_));
        Vector3 v6(transform * Vector3(min.x_, max.y_, max.z_));
        Vector3 v7(transform * max);

        unsigned uintColor = color.ToUInt();

        eGraphics::m_gdebuRenderer->AddLine(v0, v1, uintColor, depthTest);
        eGraphics::m_gdebuRenderer->AddLine(v1, v2, uintColor, depthTest);
        eGraphics::m_gdebuRenderer->AddLine(v2, v3, uintColor, depthTest);
        eGraphics::m_gdebuRenderer->AddLine(v3, v0, uintColor, depthTest);
        eGraphics::m_gdebuRenderer->AddLine(v4, v5, uintColor, depthTest);
        eGraphics::m_gdebuRenderer->AddLine(v5, v7, uintColor, depthTest);
        eGraphics::m_gdebuRenderer->AddLine(v7, v6, uintColor, depthTest);
        eGraphics::m_gdebuRenderer->AddLine(v6, v4, uintColor, depthTest);
        eGraphics::m_gdebuRenderer->AddLine(v0, v4, uintColor, depthTest);
        eGraphics::m_gdebuRenderer->AddLine(v1, v5, uintColor, depthTest);
        eGraphics::m_gdebuRenderer->AddLine(v2, v7, uintColor, depthTest);
        eGraphics::m_gdebuRenderer->AddLine(v3, v6, uintColor, depthTest);


    }



    /*void ConvertTo(Matrix3x4& in, sMatrix34& out)const
    {
        out.i.x = in.m00_;
        out.j.x = in.m01_;
        out.k.x = in.m02_;
        out.l.x = in.m03_;
        out.i.y = in.m10_;
        out.j.y = in.m11_;
        out.k.y = in.m12_;
        out.l.y = in.m13_;
        out.i.z = in.m20_;
        out.j.z = in.m21_;
        out.k.z = in.m22_;
        out.l.z = in.m23_;
    }

    void ConvertTo2(sMatrix34& in, Matrix3x4& out)const
    {
        out.m00_ = in.i.x;
        out.m01_ = in.j.x;
        out.m02_ = in.k.x;
        out.m03_ = in.l.x;
        out.m10_ = in.i.y;
        out.m11_ = in.j.y;
        out.m12_ = in.k.y;
        out.m13_ = in.l.y;
        out.m20_ = in.i.z;
        out.m21_ = in.j.z;
        out.m22_ = in.k.z;
        out.m23_ = in.l.z;
    }*/


    static Vector3 PointOnSphere(const Sphere& sphere, unsigned theta, unsigned phi)
    {
        return Vector3(
            sphere.center_.x_ + sphere.radius_ * Sin((float)theta) * Sin((float)phi),
            sphere.center_.y_ + sphere.radius_ * Cos((float)phi),
            sphere.center_.z_ + sphere.radius_ * Cos((float)theta) * Sin((float)phi)
        );
    }


    void DrawDebugSphere(const Sphere& sphere, const Matrix3x4& transform, const Color& color, bool depthTest)
    {
        unsigned uintColor = color.ToUInt();

        unsigned step = 28;

        for (unsigned j = 0; j < 180; j += step)
        {
            for (unsigned i = 0; i < 360; i += step)
            {
                Vector3 p1 = transform * PointOnSphere(sphere, i, j);
                Vector3 p2 = transform * PointOnSphere(sphere, i + step, j);
                Vector3 p3 = transform * PointOnSphere(sphere, i, j + step);
                Vector3 p4 = transform * PointOnSphere(sphere, i + step, j + step);

                eGraphics::m_gdebuRenderer->AddLine(p1, p2, uintColor, depthTest);
                eGraphics::m_gdebuRenderer->AddLine(p3, p4, uintColor, depthTest);
                eGraphics::m_gdebuRenderer->AddLine(p1, p3, uintColor, depthTest);
                eGraphics::m_gdebuRenderer->AddLine(p2, p4, uintColor, depthTest);
            }
        }
    }


    eOP_DEBUGDRAW(StringHash eventType, VariantMap& eventData)
    {
        E_INPUT_TYPE inputType = (E_INPUT_TYPE)m_inputType->getBaseValue().enumSel;

        if(inputType != EIT_CUBE && inputType != EIT_SPHERE)
        {
            UnsubscribeFromEvent(E_POSTRENDERUPDATE);
            return;
        }

        eFXYZ scale = m_scale->getBaseValue().fxyz;
        eFXYZ rotate = m_rotate->getBaseValue().fxyz;
        eFXYZ translate = m_position->getBaseValue().fxyz;

        sSRT srt;
        sMatrix34 mat;
        srt.Rotate = rotate;
        srt.Translate = translate;
        srt.Scale = scale;
        srt.MakeMatrix(mat);

        Color color  = (isCurrentShownOp()) ? Color::YELLOW : Color::GRAY;

        if(inputType == EIT_CUBE)
        {
            sAABBox box;
            box.Min = sVector31(-1,-1,-1);
            box.Max = sVector31(1,1,1);
            DrawDebugBox(box, mat, color, true);
        }
        else if(inputType == EIT_SPHERE)
        {
            Sphere sphere;
            sphere.radius_ = 1.0f;
            DrawDebugSphere(sphere, mat, color, true);
        }
    }

eOP_END(OpMeshSelect)




//!----------------------------------------------------------------------------------------------------------------------
//! SetMaterial (mesh) operator
//!
//! Assign a material on a mesh part.
//!----------------------------------------------------------------------------------------------------------------------
eOP_DEF_MESH(OpMeshSetMaterial, "SetMaterial", 'm', 1, 1, eOP_INPUTS(eOC_MESH))

    eParameter* m_materialFile;
    eParameter* m_selection;
    eParameter* m_cluster;

    enum E_SELECTION_TYPE
    {
        EST_ALL         = 0,
        EST_NONE        = 1,
        EST_SELECTED    = 2,
        EST_UNSELECTED  = 3,
        EST_CLUSTER     = 4,
        EST_ALLCLUSTER  = 5
    };

    eOP_INIT()
    {
        eOP_PAR_FILE(m_materialFile, "Material", "Materials/Mushroom.xml");
        eOP_PAR_ENUM(m_selection, "Selection", "All|None|Selected|Unselected|Cluster|All clusters", EST_ALL);
        eOP_PAR_INT(m_cluster, "ClusterIndex", 0, 1024, 0);
    }

    eOP_EXEC()
    {
        String materialFile(m_materialFile->getBaseValue().string.c_str());
        eInt selection = m_selection->getBaseValue().enumSel;
        eInt clusterIndex = m_selection->getBaseValue().integer;

        copyInput(0);

        ResourceCache* cache = eGraphics::m_gcontext->GetSubsystem<ResourceCache>();
        Material* in1 = cache->GetResource<Material>(materialFile);

        if(selection == EST_CLUSTER)
        {
            sInt cli = clusterIndex;
            if(cli>=0 && cli<m_mesh->Clusters.GetCount())
            {
                m_mesh->Clusters[cli]->UrhoMat->ReleaseRef();
                m_mesh->Clusters[cli]->UrhoMat = in1;
                in1->AddRef();
            }
        }
        else if(selection == EST_ALLCLUSTER)
        {
            Wz4MeshCluster * c;
            sFORALL(m_mesh->Clusters, c)
            {
                c->UrhoMat->ReleaseRef();
                c->UrhoMat = in1;
                in1->AddRef();
            }
        }
        else
        {
            if(selection == EST_ALL)
                sDeleteAll(m_mesh->Clusters);

            Wz4MeshCluster *cl;
            sInt cli = -1;
            sFORALL(m_mesh->Clusters,cl)
            {
                if(cl->UrhoMat==in1)
                    cli = _i;
            }
            if(cli==-1)
            {
                cl = new Wz4MeshCluster;
                cl->UrhoMat = in1; in1->AddRef();
                cli = m_mesh->Clusters.GetCount();
                m_mesh->Clusters.AddTail(cl);
            }

            Wz4MeshFace *mf;
            sFORALL(m_mesh->Faces,mf)
            {
                if(logic(selection,mf->Select))
                    mf->Cluster = cli;
            }

            if(m_mesh->Skeleton) m_mesh->SplitClustersAnim(74);
            if(m_mesh->Chunks.GetCount()) m_mesh->SplitClustersChunked(74);

        }
        m_mesh->Flush();
    }

eOP_END(OpMeshSetMaterial)














eOP_DEF_MESH(OpMeshExtrude, "Extrude", 'e', 1, 1, eOP_INPUTS(eOC_MESH))


/*operator Wz4Mesh Extrude(Wz4Mesh)
{
  column = 2;
  shortcut = 'e';
  flags = passinput|passoutput;
  parameter
  {
    int Steps(1..1024) = 1;
    float Amount(-1024..1024 step 0.01) = 1;
    flags Flags "Faces"("group|single");
    layout continue flags Flags "Direction"("*1group|normal|center");
    if((Flags & 6)==4)
      float31 Center(-1024..1024);
    float LocalScale(0..16.0 step 0.01) = 1.0;
    flags SelectUpdateFlag "Selection"("newest faces|original faces");
    float2 UVOffset "UV offset"(-1024..1024 step 0.01) = 0;
  }
  code
  {
    out->Extrude(para->Steps,para->Amount,para->Flags,para->Center,para->LocalScale,para->SelectUpdateFlag,para->UVOffset);
  }
}*/


    eParameter* m_steps;
    eParameter* m_amount;
    eParameter* m_faces;
    eParameter* m_direction;
    eParameter* m_center;
    eParameter* m_localScale;
    eParameter* m_selection;
    eParameter* m_uvOffset;


    eOP_INIT()
    {
        eOP_PAR_INT(m_steps, "Steps", 1, 1024, 1);
        eOP_PAR_FLOAT(m_amount, "Amount", -1024.0f, 1024.0f, 1.0f);
        eOP_PAR_ENUM(m_faces, "Faces", "Group|Single", 0);
        eOP_PAR_ENUM(m_direction, "Direction", "Group|Normal|Center", 0);
        eOP_PAR_FXYZ(m_center, "Center", eF32_MIN, eF32_MAX, 0.0f, 0.0f, 0.0f);
        eOP_PAR_FLOAT(m_localScale, "LocalScale", 0.0f, 16.0f, 1.0f);
        eOP_PAR_ENUM(m_selection, "Selection", "Newest faces|Original faces", 1);
        eOP_PAR_FXY(m_uvOffset, "UVOffset", eF32_MIN, eF32_MAX, 0.0f, 0.0f);
    }

    eOP_EXEC()
    {
        sInt steps = m_steps->getBaseValue().integer;
        sF32 amount = m_amount->getBaseValue().flt;
        sVector31 center = m_center->getBaseValue().fxyz;
        sF32 localScale = m_localScale->getBaseValue().flt;
        sVector2 uvOffset = m_uvOffset->getBaseValue().fxy;
        sInt selection = m_selection->getBaseValue().enumSel;

        sInt flags = m_faces->getBaseValue().enumSel;                   // 0,1
        flags += (m_direction->getBaseValue().enumSel * ePow(2,1));     // + 0,2,4

        copyInput(0);
        m_mesh->Extrude(steps, amount, flags, center, localScale, selection, uvOffset);
    }


eOP_END(OpMeshExtrude)





eOP_DEF_MESH(OpMeshGrid, "Grid", 'g', 0, 0, eOP_INPUTS())

    eParameter* m_tesselate;
    eParameter* m_sides;
    eParameter* m_size;

    eOP_INIT()
    {
        eOP_PAR_IXY(m_tesselate, "Tesselate", 1, 4096, 1, 1);
        eOP_PAR_ENUM(m_sides, "Sides", "-|Invert|Double", 0);
        eOP_PAR_FLOAT(m_size, "Size", eF32_MIN, eF32_MAX, 1.0f);
    }

    eOP_EXEC()
    {
        eIXY tesselate = m_tesselate->getBaseValue().ixy;
        eInt sides = m_sides->getBaseValue().enumSel;
        eF32 size = m_size->getBaseValue().flt;

        // create one grid

        m_mesh->MakeGrid(tesselate.x, tesselate.y);

        if(sides == 2) // double side grid
        {
            Wz4Mesh *m2 = new Wz4Mesh;
            m2->CopyFrom(m_mesh);
            sMatrix34 mat;
            mat.i.x = -1;
            mat.j.y = -1;
            mat.l.x = sF32(tesselate.x);
            m2->Transform(mat);
            m_mesh->Add(m2);
            delete m2;
        }

        // scale and position grid

        sMatrix34 mat;
        sF32 sx = size/(tesselate.x);
        sF32 sz = size/(tesselate.y);
        sF32 sy = 1;
        sF32 tx = -size*0.5f;
        sF32 tz = -size*0.5f;

        if(sides == 1) // Invert side grid
        {
            sy = -sy;
            sz = -sz;
            tz = -tz;
        }

        mat.i.Init(sx,0,0);
        mat.j.Init(0,sy,0);
        mat.k.Init(0,0,sz);
        mat.l.Init(tx,0,tz);
        m_mesh->Transform(mat);

        // flip uv

        Wz4MeshVertex *mv;
        sFORALL(m_mesh->Vertices,mv)
            mv->V0 = 1-mv->V0;

        //m_mesh->CalcNormalAndTangents();
    }


eOP_END(OpMeshGrid)


