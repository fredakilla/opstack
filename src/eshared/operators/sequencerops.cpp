/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   This file is part of
 *       ______        _                             __ __
 *      / ____/____   (_)____ _ ____ ___   ____ _   / // /
 *     / __/  / __ \ / // __ `// __ `__ \ / __ `/  / // /_
 *    / /___ / / / // // /_/ // / / / / // /_/ /  /__  __/
 *   /_____//_/ /_//_/ \__, //_/ /_/ /_/ \__,_/     /_/.   
 *                    /____/                              
 *
 *   Copyright � 2003-2012 Brain Control, all rights reserved.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "../eshared.hpp"

#include "baseoperators/iviewport.hpp"
#include "baseoperators/isequencer.hpp"







//-------------------------------------------------------------------------------------------------------------------------
//!
//! eISequencerOp
//!
//-------------------------------------------------------------------------------------------------------------------------

eIOperatorRenderer* eIOpSequencer::createOperatorRenderer()
{
    return new eIOpSequencerRenderer;
}



//!----------------------------------------------------------------------------------------------------------------------
//! Scene (sequencer) operator
//!
//! Adds a scene entry to the sequencer.
//!----------------------------------------------------------------------------------------------------------------------

eOP_DEF_SEQ(eSeqSceneOp, "Clip", 'c', 1, 1, eOP_INPUTS(eOC_POV))

    eParameter* m_startTime;
    eParameter* m_duration;
    eParameter* m_track;
    eParameter* m_timeOffset;
    eParameter* m_timeScale;
    eParameter* m_blendMode;
    eParameter* m_blendRatios;
    eParameter* m_rtname;
    eParameter* m_mergeMode;
    eParameter* m_power;

    eOP_INIT()
    {
        eOP_PAR_FLOAT(m_startTime, "Start time", 0.0f, eF32_MAX, 0.0f);
        eOP_PAR_FLOAT(m_duration, "Duration", 0.01f, eF32_MAX, 10.0f);
        eOP_PAR_INT(m_track, "Track", 0, eSequencer::MAX_TRACKS-1, 0);

        eOP_PAR_LABEL2("Extended properties");
        eOP_PAR_FLOAT(m_timeOffset, "Time offset", 0.0f, eF32_MAX, 0.0f);
        eOP_PAR_FLOAT(m_timeScale, "Time scale", eF32_MIN, eF32_MAX, 1.0f);

        eOP_PAR_LABEL2("Blending");
        eOP_PAR_ENUM(m_mergeMode, "Blending", "None|Add|Sub|Mul|Darken|Lighten|Difference|Negation|Exclusion|Dodge|Burn", 0);
        eOP_PAR_FXY(m_blendRatios, "Blend ratios", 0.0f, 1.0f, 1.0f, 1.0f);
        eOP_PAR_FLOAT(m_power, "Power", 0, eF32_MAX, 1.0f);
        eOP_PAR_STRING(m_rtname, "rtname", "");
    }


    eOP_EXEC()
    {
        /*eSeqEntry entry;

        entry.type = eSET_SCENE;
        entry.startTime = m_startTime->getBaseValue().flt;
        entry.duration = m_duration->getBaseValue().flt;
        entry.blendMode = (eSeqBlendMode&)m_blendMode->getBaseValue().enumSel;
        entry.blendRatios = m_blendRatios->getBaseValue().fxy;

        //entry.scene.urhoScene = ((eIViewportOp *)getAboveOp(0))->getResult().scene;
        entry.scene.urhoViewport = ((eIViewportOp *)getAboveOp(0))->getResult().viewport;
        entry.scene.timeOffset = m_timeOffset->getBaseValue().flt;
        entry.scene.timeScale = m_timeScale->getBaseValue().flt;

        m_seq.addEntry(entry, m_track->getBaseValue().integer);*/






        eSeqEntry entry;

        entry.startTime = m_startTime->getBaseValue().flt;
        entry.duration = m_duration->getBaseValue().flt;
        //entry.blendMode = (eSeqBlendMode&)m_blendMode->getBaseValue().enumSel;
        entry.blendRatios = m_blendRatios->getBaseValue().fxy;
        entry.type = eSET_URHOTEST;


        String rtName(m_rtname->getBaseValue().string.c_str());


        // Create Render target

        Camera* camera = ((eIOpViewport *)getAboveOp(0))->getResult().viewport->GetCamera();
        Scene* scene = ((eIOpViewport *)getAboveOp(0))->getResult().viewport->GetScene();
        RenderPath* renderPath = ((eIOpViewport *)getAboveOp(0))->getResult().viewport->GetRenderPath();

        Graphics* graphics = GetSubsystem<Graphics>();

        SharedPtr<Texture2D> renderTexture(new Texture2D(context_));
        renderTexture->SetSize(graphics->GetWidth(), graphics->GetHeight(), Graphics::GetRGBAFormat(), TEXTURE_RENDERTARGET);
        renderTexture->SetFilterMode(FILTER_DEFAULT);
        renderTexture->SetName(rtName);
        GetSubsystem<ResourceCache>()->AddManualResource(renderTexture);

        SharedPtr<RenderSurface> surface(renderTexture->GetRenderSurface());
        SharedPtr<Viewport> rttViewport(new Viewport(context_, scene, camera));
        rttViewport->SetRenderPath(renderPath);
        surface->SetViewport(0, rttViewport);
        surface->SetUpdateMode(SURFACE_UPDATEALWAYS);


        entry.urho.renderTexture = renderTexture;
        entry.urho.rttViewport = rttViewport;
        entry.urho.surface = surface;

        /*renderTexture->AddRef();
        rttViewport->AddRef();
        surface->AddRef();*/


        String mode;
        switch(m_mergeMode->getBaseValue().enumSel)
        {
            default:    mode = "MERGE_MODE_NONE";           break;
            case 0:     mode = "MERGE_MODE_NONE";           break;
            case 1:     mode = "MERGE_MODE_ADD";            break;
            case 2:     mode = "MERGE_MODE_SUB";            break;
            case 3:     mode = "MERGE_MODE_MUL";            break;
            case 4:     mode = "MERGE_MODE_DARKEN";         break;
            case 5:     mode = "MERGE_MODE_LIGHTEN";        break;
            case 6:     mode = "MERGE_MODE_DIFFERENCE";     break;
            case 7:     mode = "MERGE_MODE_NEGATION";       break;
            case 8:     mode = "MERGE_MODE_EXCLUSION";      break;
            case 9:     mode = "MERGE_MODE_DODGE";          break;
            case 10:    mode = "MERGE_MODE_BURN";           break;
        }


        // create renderpath command

        Vector2 blendRatio(m_blendRatios->getBaseValue().fxy.x, m_blendRatios->getBaseValue().fxy.y);

        RenderPathCommand* rpc = new RenderPathCommand;
        rpc->type_ = CMD_QUAD;
        rpc->tag_ = String("ClipSequence");
        rpc->vertexShaderName_ = "MergeBuffer";
        rpc->pixelShaderName_ = "MergeBuffer";
        rpc->blendMode_ = BlendMode::BLEND_REPLACE;
        rpc->SetOutput(0, "viewport"); //(rtName == "aa") ? "mixed2" : "mixed" );
        rpc->SetTextureName(TU_DIFFUSE, "viewport"); //(rtName == "aa") ? "mixed" : "mixed2" );
        rpc->SetTextureName(TU_NORMAL, rtName);
        rpc->SetShaderParameter("BlendRatio", blendRatio);
        rpc->SetShaderParameter("ClearColor", Vector4(1,0.0,0,0));
        rpc->SetShaderParameter("Power", m_power->getBaseValue().flt);
        rpc->SetShaderParameter("FlipUV", false);
        rpc->pixelShaderDefines_ = mode;
        entry.urho.renderPathCommand = rpc;


        // add entry to sequencer
        m_seq.addEntry(entry, m_track->getBaseValue().integer);

    }

eOP_END(eSeqSceneOp)


















/*
//!--------------------M--------------------------------------------------------------------------------------------------
//! Scene (sequencer) operator
//!
//! Adds a scene entry to the sequencer.
//!----------------------------------------------------------------------------------------------------------------------
#if defined(HAVE_OP_SEQUENCER_SCENE) || defined(eEDITOR)
eOP_DEF_SEQ(eSeqSceneOp, "SceneSequence", 's', 1, 1, eOP_INPUTS(eOC_SCENE))
    eOP_EXEC2(DISABLE_STATIC_PARAMS,
        eOP_PAR_FLOAT(eSeqSceneOp, startTime, "Start time", 0.0f, eF32_MAX, 0.0f,
        eOP_PAR_FLOAT(eSeqSceneOp, duration, "Duration", 0.01f, eF32_MAX, 10.0f,
        eOP_PAR_INT(eSeqSceneOp, track, "Track", 0, eSequencer::MAX_TRACKS-1, 0,
        eOP_PAR_LABEL(eSeqSceneOp, label0, "Extended properties", "Extended properties",
        eOP_PAR_FLOAT(eSeqSceneOp, timeOffset, "Time offset", 0.0f, eF32_MAX, 0.0f,
        eOP_PAR_FLOAT(eSeqSceneOp, timeScale, "Time scale", eF32_MIN, eF32_MAX, 1.0f,
        eOP_PAR_ENUM(eSeqSceneOp, blendMode, "Blending", "Additive|Subtractive|Multiplicative|Brighter|Darker|None", 5,
        eOP_PAR_FXY(eSeqSceneOp, blendRatios, "Blend ratios", 0.0f, 1.0f, 1.0f, 1.0f,
        eOP_PAR_END)))))))))
    {
        eSeqEntry entry;

        entry.type = eSET_SCENE;
        entry.startTime = startTime;
        entry.duration = duration;
        entry.blendMode = (eSeqBlendMode&)blendMode;
        entry.blendRatios = blendRatios;

        entry.scene.urhoScene = ((eISceneOp *)getAboveOp(0))->getResult().scene;
        entry.scene.timeOffset = timeOffset;
        entry.scene.timeScale = timeScale;

        m_seq.addEntry(entry, track);
    }
    eOP_EXEC2_END
eOP_END(eSeqSceneOp);
#endif
*/
/*
//!----------------------------------------------------------------------------------------------------------------------
//! Overlay (sequencer) operator
//!
//! Adds an overlay entry to the sequencer.
//!----------------------------------------------------------------------------------------------------------------------
#if defined(HAVE_OP_SEQUENCER_OVERLAY) || defined(eEDITOR)
eOP_DEF_SEQ(eSeqOverlayOp, "Overlay", 'o', 1, 1, eOP_INPUTS(eOC_BMP|eOC_MAT))
    eOP_EXEC2(DISABLE_STATIC_PARAMS,
        eOP_PAR_FLOAT(eSeqOverlayOp, startTime, "Start time", 0.0f, eF32_MAX, 0.0f,
        eOP_PAR_FLOAT(eSeqOverlayOp, duration, "Duration", 0.01f, eF32_MAX, 1.0f,
        eOP_PAR_INT(eSeqOverlayOp, track, "Track", 0, eSequencer::MAX_TRACKS-1, 0,
        eOP_PAR_LABEL(eSeqOverlayOp, label0, "Extended properties", "Extended properties",
        eOP_PAR_ENUM(eSeqOverlayOp, blendMode, "Blending", "Additive|Subtractive|Multiplicative|Brighter|Darker|None", 0,
        eOP_PAR_FXY(eSeqOverlayOp, blendRatios, "Blend ratios", 0.0f, 1.0f, 1.0f, 1.0f,
        eOP_PAR_FXYZW(eSeqOverlayOp, rect, "Rectangle", eF32_MIN, eF32_MAX, 0.0f, 0.0f, 1.0f, 1.0f,
        eOP_PAR_FXY(eSeqOverlayOp, scrollUv, "Scroll U/V", eF32_MIN, eF32_MAX, 0.0f, 0.0f,
        eOP_PAR_FXY(eSeqOverlayOp, tileUv, "Tile U/V", eF32_MIN, eF32_MAX, 1.0f, 1.0f,
        eOP_PAR_ENUM(eSeqOverlayOp, texAddr, "U/V address mode", "Wrap|Clamp|Mirror", 1,
        eOP_PAR_BOOL(eSeqOverlayOp, filtered, "Filtered", eTRUE,
        eOP_PAR_END))))))))))))
    {
        eSeqEntry entry;

        entry.type = eSET_OVERLAY;
        entry.duration = duration;
        entry.startTime = startTime;
        entry.blendMode = (eSeqBlendMode)blendMode;
        entry.blendRatios = blendRatios;

        //@@eTexture2d *tex = nullptr;
        //@@if (getAboveOp(0)->getResultClass() == eOC_MAT)
        //@@    tex = (eTexture2d *)((eIMaterialOp *)getAboveOp(0))->getResult().mat.textures[eMTU_DIFFUSE];
        //@@else
        //@@    tex = ((eIBitmapOp *)getAboveOp(0))->getResult().uav->tex;
        //@@

        eTexture2d *tex = nullptr;

        wz4::Wz4Bitmap* wz4bitmap =  ((eIBitmapOp *)getAboveOp(0))->getResult().bitmap;
        Texture2D * decalTex = new Texture2D(eGraphics::m_gcontext);
        decalTex->SetSize(wz4bitmap->XSize,wz4bitmap->YSize, Graphics::GetRGBAFormat(), TextureUsage::TEXTURE_STATIC);
        unsigned char* imageData = new unsigned char[wz4bitmap->XSize*wz4bitmap->YSize*4];
        wz4bitmap->CopyTo((eU8*)imageData);
        decalTex->SetData(0, 0, 0, wz4bitmap->XSize, wz4bitmap->YSize, imageData);

        tex = new eTexture2d;
        tex->width = wz4bitmap->XSize;
        tex->width = wz4bitmap->YSize;
        tex->urhoTex = decalTex;

        //entry.urho.



        entry.overlay.rect = rect;
        entry.overlay.tileUv = tileUv;
        entry.overlay.texture = tex;
        entry.overlay.filtered = filtered;
        entry.overlay.scrollUv = scrollUv;
        entry.overlay.texAddr = eTMF_WRAP<<texAddr;

        m_seq.addEntry(entry, track);
    }
    eOP_EXEC2_END
eOP_END(eSeqOverlayOp);
#endif
*/
