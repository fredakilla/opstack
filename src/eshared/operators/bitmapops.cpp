#include "eshared.hpp"
#include "baseoperators/ibitmap.hpp"

//!----------------------------------------------------------------------------------------------------------------------
//! Fill (bitmap) operator
//!
//! Fills the whole bitmap with just one color.
//!----------------------------------------------------------------------------------------------------------------------
eOP_DEF_BMP(eFillOp, "Fill", 'f', 0, 0, eOP_INPUTS())

    eParameter* m_widthSel;
    eParameter* m_heightSel;
    eParameter* m_color;

    eOP_INIT()
    {
        eOP_PARAM2_ADD_ENUM(m_widthSel, "Width", "1|2|4|8|16|32|64|128|256|512|1024|2048|4096|8192", 8);
        eOP_PARAM2_ADD_ENUM(m_heightSel, "Height", "1|2|4|8|16|32|64|128|256|512|1024|2048|4096|8192", 8);
        eOP_PARAM2_ADD_RGBA(m_color, "Color", 0, 0, 0, 255);
    }

    eOP_EXEC()
    {
        eInt widthSel = m_widthSel->getBaseValue().enumSel;
        eInt heightSel = m_heightSel->getBaseValue().enumSel;
        eColor color = m_color->getBaseValue().color;

        reallocate(1<<widthSel, 1<<heightSel);
        m_bitmap->Flat(color.abgr);
    }

eOP_END(eFillOp)


//!----------------------------------------------------------------------------------------------------------------------
//! GlowRect (bitmap) operator
//!
//!
//!----------------------------------------------------------------------------------------------------------------------
eOP_DEF_BMP(eGlowRectOp, "GlowRect", 'g', 1, 1, eOP_INPUTS(eOC_BMP))

    eParameter* m_center;
    eParameter* m_radius;
    eParameter* m_size;
    eParameter* m_color;
    eParameter* m_blend;
    eParameter* m_power;
    eParameter* m_wrap;
    eParameter* m_flag;

    eOP_INIT()
    {
        eOP_PARAM2_ADD_FXY(m_center, "Center", -4.0f, 4.0f, 0.5f, 0.5f);
        eOP_PARAM2_ADD_FXY(m_radius, "Radius", 0.0f, 4.0f, 0.5f, 0.5f);
        eOP_PARAM2_ADD_FXY(m_size, "Size", 0.0f, 4.0f, 0.0f, 0.0f);
        eOP_PARAM2_ADD_RGBA(m_color, "Color", 255, 255, 255, 255);
        eOP_PARAM2_ADD_FLOAT(m_blend, "Blend", 0.0f, 1.0f, 1.0f);
        eOP_PARAM2_ADD_FLOAT(m_power, "Power", 0.0f, 16.0f, 0.5f);
        eOP_PARAM2_ADD_FLAGS(m_wrap, "Wrap", "Wrap", 0);
        eOP_PARAM2_ADD_FLAGS(m_flag, "Flag", "alt power|rectangle", 0);
    }

    eOP_EXEC()
    {
        copyInput(0);


        eFXY center = m_center->getBaseValue().fxy;
        eFXY radius = m_radius->getBaseValue().fxy;
        eFXY size = m_size->getBaseValue().fxy;
        eColor color = m_color->getBaseValue().color;
        eF32 blend = m_blend->getBaseValue().flt;
        eF32 power = m_power->getBaseValue().flt;
        eU8 wrap = m_wrap->getBaseValue().flags;
        eU8 flag = m_flag->getBaseValue().flags;

        m_bitmap->GlowRect(center.x, center.y,
                           radius.x, radius.y,
                           size.x, size.y,
                           color.abgr,
                           blend,
                           power,
                           wrap,
                           flag);

    }

eOP_END(eGlowRectOp)

