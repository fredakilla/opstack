/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   This file is part of
 *       ______        _                             __ __
 *      / ____/____   (_)____ _ ____ ___   ____ _   / // /
 *     / __/  / __ \ / // __ `// __ `__ \ / __ `/  / // /_
 *    / /___ / / / // // /_/ // / / / / // /_/ /  /__  __/
 *   /_____//_/ /_//_/ \__, //_/ /_/ /_/ \__,_/     /_/.   
 *                    /____/                              
 *
 *   Copyright � 2003-2012 Brain Control, all rights reserved.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "../eshared.hpp"

#include "baseoperators/ieffect.hpp"

//!----------------------------------------------------------------------------------------------------------------------
//! AddComponents (node) operator
//!
//! Add components to a node
//!----------------------------------------------------------------------------------------------------------------------
eOP_DEF_FX(eRenderPathFXOp, "RenderPath", 'r', 0, 1, eOP_INPUTS(eOC_FX))

eOP_EXEC()
{
    String fx; //= renderpathCommandTxt;


    if(getAboveOpCount() > 0)
    {
        eIOpEffect* fxop = (eIOpEffect*)getAboveOp(0);
        const String& inputCommand = fxop->getResult().rpc.metadata_;
        fx += inputCommand;
    }



    const char renderpath[] =
            "<renderpath>"
            "    <command type=\"clear\" color=\"0.0 0.0 0.0 1.0\" depth=\"1.0\" stencil=\"0\" />"
            "    <command type=\"scenepass\" pass=\"base\" vertexlights=\"false\" metadata=\"base\" />"
            "    <command type=\"forwardlights\" pass=\"light\" />"
            "    <command type=\"scenepass\" pass=\"postopaque\" />"
            "    <command type=\"scenepass\" pass=\"refract\">"
            "        <texture unit=\"environment\" name=\"viewport\" />"
            "    </command>"
            "    <command type=\"scenepass\" pass=\"alpha\" vertexlights=\"true\" sort=\"backtofront\" metadata=\"base\" />"
            "    <command type=\"scenepass\" pass=\"postalpha\" sort=\"backtofront\" />"
            "</renderpath>";


    const String& patchString = renderpath;
    if (!patchString.Empty())
    {
        SharedPtr<XMLFile> patchFile(new XMLFile(eGraphics::m_gcontext));
        if (patchFile->FromString(patchString))
        {
            m_res.renderPath = new RenderPath();
            m_res.renderPath->Load(patchFile);


            if(getAboveOpCount() > 0)
            {
                eIOpEffect* fxop = (eIOpEffect*)getAboveOp(0);
                m_res.renderPath->AddCommand(fxop->getResult().rpc);
            }


            //Renderer* renderer = GetSubsystem<Renderer>();
            //Viewport* viewport = renderer->GetViewport(0);
            //_viewport->SetRenderPath(patchFile);
        }
    }
}

eOP_END(eRenderPathFXOp)




//!----------------------------------------------------------------------------------------------------------------------
//! AddComponents (node) operator
//!
//! Add components to a node
//!----------------------------------------------------------------------------------------------------------------------
eOP_DEF_FX(eGammaCorrectionFXOp, "GammaCorrection", 'g', 0, 1, eOP_INPUTS(eOC_FX))

eOP_EXEC()
{
    String patchString; //= renderpathCommandTxt;


    if(getAboveOpCount() > 0)
    {
       /* eIEffectOp* fxop = (eIEffectOp*)getAboveOp(0);
        const String& inputCommand = fxop->getResult().rpc._;
        patchString += inputCommand;

        ePRINT("metedadadazdza %s", fxop->getResult().rpc.metadata_.CString());*/

        //m_res.renderPath->
    }



    //RenderPathCommand rpc;
    const char renderpathCommandTxt[] =
            "<command type=\"quad\" tag=\"GammaCorrection\" vs=\"GammaCorrection\" ps=\"GammaCorrection\" output=\"viewport\">"
            "        <texture unit=\"diffuse\" name=\"viewport\" />"
            "</command>";

    patchString += renderpathCommandTxt;


    SharedPtr<XMLFile> patchFile(new XMLFile(eGraphics::m_gcontext));
    if (patchFile->FromString(renderpathCommandTxt))
    {
        m_res.rpc.Load(patchFile->GetRoot());

    }
}

eOP_END(eGammaCorrectionFXOp)
