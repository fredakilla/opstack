#include "baseoperators/icomponent.hpp"
#include "baseoperators/imesh.hpp"
#include "urhoobjects/uWz4Meshcomponent.hpp"

//!----------------------------------------------------------------------------------------------------------------------
//!
//! Create component renderer
//!
//!----------------------------------------------------------------------------------------------------------------------
eIOperatorRenderer* eIOpComponent::createOperatorRenderer()
{
    m_opRenderer = new eIOpComponentRenderer;
    return m_opRenderer;
}


//!----------------------------------------------------------------------------------------------------------------------
//! StaticModel (component) operator
//!
//! Create a StaticModel component
//!----------------------------------------------------------------------------------------------------------------------
eOP_DEF_COMPONENT(eComponentStaticModelOp, "StaticModel", 's', 1, 1, eOP_INPUTS(eOC_REALNODE|eOC_COMPONENT))

    eParameter* m_staticModelFile;
    eParameter* m_materialFile;
    eParameter* m_label;
    eParameter* m_castShadows;
    eParameter* m_maxLights;
    eParameter* m_drawDistance;
    eParameter* m_shadowDistance;

    eOP_INIT()
    {
        eOP_PAR_FILE(m_staticModelFile, "Model", "Models/Mushroom.mdl");
        eOP_PAR_FILE(m_materialFile, "Material", "Materials/Mushroom.xml");
        eOP_PAR_LABEL(m_label, "", "Drawable properties");
        eOP_PAR_FLOAT(m_drawDistance, "DrawDistance", 0.0f, eF32_MAX, 0.0f);
        eOP_PAR_FLOAT(m_shadowDistance, "ShadowDistance", 0.0f, eF32_MAX, 0.0f);
        eOP_PAR_INT(m_maxLights, "MaxLights", 0, 1024, 0);
        eOP_PAR_BOOL(m_castShadows, "CastShadows", eFALSE);
    }

    eOP_EXEC()
    {
        String staticModelFile(m_staticModelFile->getBaseValue().string.c_str());
        String materialFile(m_materialFile->getBaseValue().string.c_str());

        ResourceCache* cache = eGraphics::m_gcontext->GetSubsystem<ResourceCache>();        

        m_result.component = m_result.node->CreateComponent<StaticModel>();

        StaticModel* staticModel = StaticCast<StaticModel>(m_result.component);
        staticModel->SetModel(cache->GetResource<Model>(staticModelFile));
        staticModel->SetMaterial(cache->GetResource<Material>(materialFile));

        // Drawable properties

        staticModel->SetDrawDistance(m_drawDistance->getBaseValue().flt);
        staticModel->SetShadowDistance(m_shadowDistance->getBaseValue().flt);
        //staticModel->SetLodBias(float bias);
        //staticModel->SetViewMask(unsigned mask);
        //staticModel->SetLightMask(unsigned mask);
        //staticModel->SetShadowMask(unsigned mask);
        //staticModel->SetZoneMask(unsigned mask);
        staticModel->SetMaxLights(m_maxLights->getBaseValue().integer);
        staticModel->SetCastShadows(m_castShadows->getBaseValue().boolean);
        //staticModel->SetOccluder(bool enable);
        //staticModel->SetOccludee(bool enable);
    }

eOP_END(eComponentStaticModelOp)


//!----------------------------------------------------------------------------------------------------------------------
//! Camera (component) operator
//!
//! Create a camera component
//!----------------------------------------------------------------------------------------------------------------------
eOP_DEF_COMPONENT(eComponentCameraOp, "Camera", 'c', 1, 1, eOP_INPUTS(eOC_REALNODE|eOC_COMPONENT))

    eParameter* m_zoom;

    eOP_INIT()
    {
        eOP_PAR_FLOAT(m_zoom, "Zoom", eF32_MIN, eF32_MAX, 1.0f);
    }  

    eOP_EXEC()
    {
        m_result.component = m_result.node->CreateComponent<Camera>();

        Camera* camera = StaticCast<Camera>(m_result.component);        
        camera->SetZoom(m_zoom->getBaseValue().flt);
    }

eOP_END(eComponentCameraOp)


//!----------------------------------------------------------------------------------------------------------------------
//! Script (component) operator
//!
//! Create a script component
//!----------------------------------------------------------------------------------------------------------------------
eOP_DEF_COMPONENT(eComponentScriptOp, "Script", 'a', 1, 1, eOP_INPUTS(eOC_REALNODE|eOC_COMPONENT))

    eParameter* m_scriptFile;
    eParameter* m_scriptClass;

    eOP_INIT()
    {
        eOP_PAR_FILE(m_scriptFile, "ScriptFile", "Scripts/Utilities/Rotator.as");
        eOP_PAR_STRING(m_scriptClass, "ClassName", "");
    }

    eOP_EXEC()
    {
        const eChar* scriptFile = m_scriptFile->getBaseValue().string.c_str();
        const eChar* scriptClassName = m_scriptClass->getBaseValue().string.c_str();

        ResourceCache* cache = eGraphics::m_gcontext->GetSubsystem<ResourceCache>();

        m_result.component = m_result.node->CreateComponent<ScriptInstance>();

        ScriptInstance* script = StaticCast<ScriptInstance>(m_result.component);
        script->CreateObject(cache->GetResource<ScriptFile>(scriptFile), scriptClassName);
        cache->ReloadResource(cache->GetResource<ScriptFile>(scriptFile));
    }

eOP_END(eComponentScriptOp)


//!----------------------------------------------------------------------------------------------------------------------
//! Light (component) operator
//!
//! Create a light component
//!----------------------------------------------------------------------------------------------------------------------
eOP_DEF_COMPONENT(eComponentLightOp, "Light", 'l', 1, 1, eOP_INPUTS(eOC_REALNODE|eOC_COMPONENT))

    eParameter* m_type;
    eParameter* m_color;
    eParameter* m_brightness;
    eParameter* m_range;
    eParameter* m_castShadows;
    eParameter* m_shadowIntensity;
    eParameter* m_specularIntensity;

    eOP_INIT()
    {
        eOP_PAR_ENUM(m_type, "Type", "Directional|Spot|Point", 2);
        eOP_PAR_RGB(m_color, "Color", 255, 255, 255);
        eOP_PAR_FLOAT(m_brightness, "Brightness", eF32_MIN, eF32_MAX, 1.0f);
        eOP_PAR_FLOAT(m_range, "Range", eF32_MIN, eF32_MAX, 10.0f);
        eOP_PAR_BOOL(m_castShadows, "CastShadows", eTRUE);
        eOP_PAR_FLOAT(m_shadowIntensity, "ShadowIntensity", 0.0f, 1.0f, 0.0f);
        eOP_PAR_FLOAT(m_specularIntensity, "SpecularIntensity", eF32_MIN, eF32_MAX, 1.0f);
    }

    eOP_EXEC()
    {
        eColor color = m_color->getBaseValue().color;

        m_result.component = m_result.node->CreateComponent<Light>();

        Light* light = StaticCast<Light>(m_result.component);
        light->SetLightType(LightType(m_type->getBaseValue().enumSel));
        light->SetRange(m_range->getBaseValue().flt);
        light->SetBrightness(m_brightness->getBaseValue().flt);
        light->SetColor(Color(color.r/255.0f, color.g/255.0f, color.b/255.0f, 1.0f));
        light->SetCastShadows(m_castShadows->getBaseValue().boolean);
        light->SetShadowIntensity(m_shadowIntensity->getBaseValue().flt);
        light->SetSpecularIntensity(m_specularIntensity->getBaseValue().flt);
    }

eOP_END(eComponentLightOp)


//!----------------------------------------------------------------------------------------------------------------------
//! Wz4Mesh (component) operator
//!
//! Create a static model component from a wz4 mesh
//!----------------------------------------------------------------------------------------------------------------------
eOP_DEF_COMPONENT(eOpComponentWz4Mesh, "Wz4Mesh", 'm', 2, 2, eOP_INPUTS(eOC_REALNODE, eOC_MESH))

    eParameter* m_label;
    eParameter* m_castShadows;
    eParameter* m_maxLights;
    eParameter* m_drawDistance;
    eParameter* m_shadowDistance;

    eOP_INIT()
    {
        eOP_PAR_LABEL(m_label, "", "Drawable properties");
        eOP_PAR_FLOAT(m_drawDistance, "DrawDistance", 0.0f, eF32_MAX, 0.0f);
        eOP_PAR_FLOAT(m_shadowDistance, "ShadowDistance", 0.0f, eF32_MAX, 0.0f);
        eOP_PAR_INT(m_maxLights, "MaxLights", 0, 1024, 0);
        eOP_PAR_BOOL(m_castShadows, "CastShadows", eFALSE);
    }

    eOP_EXEC()
    {
        wz4::Wz4Mesh* wz4mesh = getInput<eIOpMesh>(1)->getResult().mesh;

        m_result.component = m_result.node->CreateComponent<uWz4MeshComponent>();

        uWz4MeshComponent* wz4MeshComponent = StaticCast<uWz4MeshComponent>(m_result.component);
        wz4MeshComponent->ChargeSolidMesh(wz4mesh);

        // Drawable properties

        wz4MeshComponent->SetDrawDistance(m_drawDistance->getBaseValue().flt);
        wz4MeshComponent->SetShadowDistance(m_shadowDistance->getBaseValue().flt);
        //wz4MeshComponent->SetLodBias(float bias);
        //wz4MeshComponent->SetViewMask(unsigned mask);
        //wz4MeshComponent->SetLightMask(unsigned mask);
        //wz4MeshComponent->SetShadowMask(unsigned mask);
        //wz4MeshComponent->SetZoneMask(unsigned mask);
        wz4MeshComponent->SetMaxLights(m_maxLights->getBaseValue().integer);
        wz4MeshComponent->SetCastShadows(m_castShadows->getBaseValue().boolean);
        //wz4MeshComponent->SetOccluder(bool enable);
        //wz4MeshComponent->SetOccludee(bool enable);
    }

eOP_END(eOpComponentWz4Mesh)


