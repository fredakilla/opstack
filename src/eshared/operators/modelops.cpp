/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   This file is part of
 *       ______        _                             __ __
 *      / ____/____   (_)____ _ ____ ___   ____ _   / // /
 *     / __/  / __ \ / // __ `// __ `__ \ / __ `/  / // /_
 *    / /___ / / / // // /_/ // / / / / // /_/ /  /__  __/
 *   /_____//_/ /_//_/ \__, //_/ /_/ /_/ \__,_/     /_/.   
 *                    /____/                              
 *
 *   Copyright � 2003-2012 Brain Control, all rights reserved.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "../eshared.hpp"


#include "urhoobjects/uAnimatorNodePRS.hpp"
#include "baseoperators/icomponent.hpp"
#include "baseoperators/inode.hpp"

#include "baseoperators/iviewport.hpp"
#include "baseoperators/iscene.hpp"

#include "baseoperators/ieffect.hpp"




eIOperatorRenderer* eIOpNode::createOperatorRenderer()
{
    m_opRenderer = new eIOpNodeRenderer;
    return m_opRenderer;
}

eIOperatorRenderer* eIOpViewport::createOperatorRenderer()
{
    return new eIOpViewportRenderer;
}

eIOperatorRenderer* eIOpScene::createOperatorRenderer()
{
    return new eIOpSceneRenderer;
}








//!----------------------------------------------------------------------------------------------------------------------
//! RealNode (RealNode) operator
//!
//! Create an Urho3D scene node
//!----------------------------------------------------------------------------------------------------------------------
eOP_DEF_REALNODE(eRealNodeOp, "Node", 'n', 0, 0, eOP_INPUTS())

    eOP_INIT()
    {
    }

    eOP_DEINIT()
    {
        freeResult();
    }

    eOP_EXEC()
    {
        reAllocateResult();
    }

eOP_END(eRealNodeOp)



//!----------------------------------------------------------------------------------------------------------------------
//! StaticModel (component) operator
//!
//! Create a StaticModel component
//!----------------------------------------------------------------------------------------------------------------------
eOP_DEF_REALNODE(eRealNodeBuildOp, "BuildNode", 'b', 1, 1, eOP_INPUTS(eOC_COMPONENT))

eParameter* m_rebuild;

eOP_INIT()
{

}

eOP_EXEC()
{

    eIOpPtrArray stackOps;
    stackOps.reserve(eDemoData::getTotalOpCount());
    getOpsInStack2(stackOps);

    if(stackOps.size() > 0)
    {
        eIOpRealNode* realNodeOp = (eIOpRealNode*)stackOps[0];
        Node* node = realNodeOp->getResult().realNode;

        PODVector<Component*> componentsToKeep;

        for (unsigned i=1; i<stackOps.size(); ++i)
        {
            eIOpComponent* componentOp = (eIOpComponent*)stackOps[i];
            Component* c = componentOp->getResult().component;
            componentsToKeep.Push(c);
        }





    }



#if 0
    eIOpPtrArray stackOps;
    stackOps.reserve(eDemoData::getTotalOpCount());
    getOpsInStack2(stackOps);

    if(stackOps.size() > 0)
    {
        eIRealNodeOp* realNodeOp = (eIRealNodeOp*)stackOps[0];
        Node* node = realNodeOp->getResult().node;

        PODVector<Component*> compsToKeep;

        for (eU32 i=1; i<stackOps.size(); i++)
        {
            eIComponentOp* componentOp = (eIComponentOp*)stackOps[i];
            Component* c = componentOp->getResult().component;

            ePRINT("%s", c->GetTypeName().CString());
            compsToKeep.Push(c);
        }

        PODVector<Component*> comps;
        node->GetComponents(comps, StringHash(c->GetTypeName()));

        for (unsigned j = 0; j < comps.Size(); ++j)
            for (unsigned j = 0; j < compsToKeep.Size(); ++j)
        {
            if(!comps.Contains(compsToKeep[j]))
                //compsToRemove.Push(c);
                node->RemoveComponent(comps[j]);
        }

            /*PODVector<Component*> compsToRemove;

            PODVector<Component*> comps;
            node->GetComponents(comps, StringHash(c->GetTypeName()));
            for (unsigned j = 0; j < comps.Size(); ++j)
            {
                if(!comps.Contains(c))
                    compsToRemove.Push(c);
                    //node->RemoveComponent(comps[j]);
            }


            for (unsigned j = 0; j < compsToRemove.Size(); ++j)
            {
                ePRINT("to remove : %s", compsToRemove[j]->GetTypeName().CString());

                node->RemoveComponent(compsToRemove[j]);
            }*/



       // }

        m_result.node = node;
    }

#endif
}

eOP_END(eRealNodeBuildOp)














//!----------------------------------------------------------------------------------------------------------------------
//! Node (node) operator
//!
//! Create a new node from components
//!----------------------------------------------------------------------------------------------------------------------
eOP_DEF_NODE(eNodeOp, "Node", 'n', 1, 1, eOP_INPUTS(eOC_COMPONENT) )

    eParameter* m_position;
    eParameter* m_rotate;    
    eParameter* m_scale;

    eOP_INIT()
    {
        eOP_PAR_FXYZ(m_position, "Position", eF32_MIN, eF32_MAX, 0.0f, 0.0f, 0.0f);
        eOP_PAR_FXYZ(m_rotate, "Rotate", eF32_MIN, eF32_MAX, 0.0f, 0.0f, 0.0f);
        eOP_PAR_FXYZ(m_scale, "Scale", eF32_MIN, eF32_MAX, 1.0f, 1.0f, 1.0f);


    }

    eOP_DEINIT()
    {
        freeResultNode();
    }

   /* virtual void _subscribeToEvents()
    {
        SubscribeToEvent(E_SCENEUPDATE, URHO3D_HANDLER(eNodeOp, HandleUpdate));
    }
    virtual void _unSubscribeToEvents()
    {
       UnsubscribeFromAllEvents();
    }

    void HandleUpdate(StringHash eventType, VariantMap& eventData)
    {
        // animate parameters
        _animateParameters(eGraphics::getTime());

        // animate node
        //animTransform(m_scale->getAnimValue().fxyz,
        //              m_rotate->getAnimValue().fxyz,
        //              m_translate->getAnimValue().fxyz);
    }
*/

  // void animate(eF32 time)
  // {
  //     // animate parameters
  //     //_animateParameters(eGraphics::getTime());
  //
  //     // animate node
  //     animTransform(m_scale->getAnimValue().fxyz,
  //                   m_rotate->getAnimValue().fxyz,
  //                   m_translate->getAnimValue().fxyz);
  // }


    eOP_EXEC()
    {
        ////if(!HasParametersChanged())
        ////    return;


        reAllocateResultNode();

        // (re)allocate current result node
        /*reAllocateResultNode();

        // get all components from aboves operators
        for(eU32 i=0; i<getAboveOpCount(); i++)
        {
            const eIComponentOp *input = (eIComponentOp *)getAboveOp(i);
            eASSERT(input);

            // clone component
            m_result.sceneNode->CloneComponent(input->getResult().component);
        }*/


        m_result.sceneNode = getInput<eIOpComponent>(0)->getResult().node;
        //m_result.sceneNode = ((eIOpComponent*)getAboveOp(0))->getResult().node;


        // set node transformation
        Vector3 p(m_position->getBaseValue().fxyz);
        Quaternion r(m_rotate->getBaseValue().fxyz*e360);
        Vector3 s(m_scale->getBaseValue().fxyz);
        m_result.sceneNode->SetTransform(p, r, s);

        // if animated (script data), create a component to manage animation
        if(isAnimated())
        {
            // create an AnimatorNode component
            uAnimatorNodePRS* animator = m_result.sceneNode->CreateComponent<uAnimatorNodePRS>();
            animator->Init(m_params);
        }

        ///SetParametersKey();
    }

eOP_END(eNodeOp)

//!----------------------------------------------------------------------------------------------------------------------
//! Transform (node) operator
//!
//! Create a new parent scene node with transformation
//!----------------------------------------------------------------------------------------------------------------------
eOP_DEF_NODE(eModelTransformOp, "Transform", 't', 1, 1, eOP_INPUTS(eOC_NODE))

    eParameter* m_position;
    eParameter* m_rotate;
    eParameter* m_scale;

    eOP_INIT()
    {
        eOP_PAR_FXYZ(m_position, "Position", eF32_MIN, eF32_MAX, 0.0f, 0.0f, 0.0f);
        eOP_PAR_FXYZ(m_rotate, "Rotate", eF32_MIN, eF32_MAX, 0.0f, 0.0f, 0.0f);
        eOP_PAR_FXYZ(m_scale, "Scale", eF32_MIN, eF32_MAX, 1.0f, 1.0f, 1.0f);
    }

    eOP_DEINIT()
    {
        freeResultNode();
    }

    /*virtual void _subscribeToEvents()
    {
        SubscribeToEvent(E_SCENEUPDATE, URHO3D_HANDLER(eModelTransformOp, HandleUpdate));
    }
    virtual void _unSubscribeToEvents()
    {
       UnsubscribeFromAllEvents();
    }




    void HandleUpdate(StringHash eventType, VariantMap& eventData)
    {
        using namespace SceneUpdate;

        // Take the frame time step, which is stored as a float
        float timeStep = eventData[P_TIMESTEP].GetFloat();

        // get timeline time and animate parameters
        float time = eGraphics::getTime();
        _animateParameters(time);

        // animate transformation
        animTransform(m_scale->getAnimValue().fxyz,
                      m_rotate->getAnimValue().fxyz,
                      m_translate->getAnimValue().fxyz);
    }*/


    /*void animate(eF32 time)
    {
        // animate parameters
        _animateParameters(eGraphics::getTime());

        // animate node
        animTransform(m_scale->getAnimValue().fxyz,
                      m_rotate->getAnimValue().fxyz,
                      m_translate->getAnimValue().fxyz);
    }*/


    eOP_EXEC()
    {
        reAllocateResultNode();

        copyInputNode();

        /*setTransform(m_scale->getBaseValue().fxyz,
                     m_rotate->getBaseValue().fxyz,
                     m_translate->getBaseValue().fxyz);*/

        Vector3 p(m_position->getBaseValue().fxyz);
        Quaternion r(m_rotate->getBaseValue().fxyz*e360);
        Vector3 s(m_scale->getBaseValue().fxyz);
        Matrix3x4 m = m_result.sceneNode->GetTransform() * Matrix3x4(p,r,s);
        m_result.sceneNode->SetTransform(m.Translation(), m.Rotation(), m.Scale());



        // if animated (script data), create a component to manage animation
        if(isAnimated())
        {
            // create an AnimatorNode component
            uAnimatorNodePRS* animator = m_result.sceneNode->CreateComponent<uAnimatorNodePRS>();
            animator->Init(m_params);
        }


        /* SCRIPT

        ResourceCache* cache = eGraphics::m_gcontext->GetSubsystem<ResourceCache>();

        // Add our custom Rotator script object (using the ScriptInstance C++ component to instantiate / store it) which will
        // rotate the scene node each frame, when the scene sends its update event
        ScriptInstance* instance = node->CreateComponent<ScriptInstance>();
        instance->CreateObject(cache->GetResource<ScriptFile>("Scripts/Utilities/Rotator.as"), "Rotator");
        // Call the script object's "SetRotationSpeed" function. Function arguments need to be passed in a VariantVector
        VariantVector parameters;
        parameters.Push(Vector3(rotate.x*e360, rotate.y*e360, rotate.z*e360));
        instance->Execute("void SetRotationSpeed(const Vector3&in)", parameters);

        */

        /*
         *  C++ version
         * eGraphics::m_gcontext->RegisterFactory<Rotator>();

        Rotator* rotator = node->CreateComponent<Rotator>();
        rotator->SetRotationSpeed(Vector3(10.0f, 20.0f, 30.0f));
        //rotator->setTransormSpeed(m);*/

    }

eOP_END(eModelTransformOp)

//!----------------------------------------------------------------------------------------------------------------------
//! Add (node) operator
//!
//! Create a new parent scene node for many scenes nodes
//!----------------------------------------------------------------------------------------------------------------------
eOP_DEF_NODE(eNodeAddOp, "Add", 'a', 2, 32, eOP_INPUTS(eOP_32INPUTS(eOC_NODE)))

    eOP_DEINIT()
    {
        freeResultNode();
    }

    eOP_EXEC()
    {
        reAllocateResultNode();

        // get childs nodes from aboves op
        for(eU32 i=0; i<getAboveOpCount(); i++)
        {
            copyInputNode(i);
        }
    }

eOP_END(eNodeAddOp)



//!----------------------------------------------------------------------------------------------------------------------
//! Add (node) operator
//!
//! Create a new parent scene node for many scenes nodes
//!----------------------------------------------------------------------------------------------------------------------
eOP_DEF_VIEWPORT(eViewportOp, "ViewPort", 'v', 2, 3, eOP_INPUTS(eOC_SCENE, eOC_NODE, eOC_FX))


eParameter* m_zoom;
eParameter* m_lookAt;
eParameter* m_position;

eParameter* m_linkCameraOp;

eOP_INIT()
{
    //eOP_PAR_FXYZ(m_position, "Position", eF32_MIN, eF32_MAX, 0.0f, 0.0f, -10.0f);
   // eOP_PAR_FXYZ(m_lookAt, "LookAt", eF32_MIN, eF32_MAX, 0.0f, 0.0f, 0.0f);
    //eOP_PAR_FLOAT(m_zoom, "Zoom", eF32_MIN, eF32_MAX, 1.0f);

    //eOP_PAR_LINK_MODEL(m_linkCameraOp, "Camera", eOC_NODE, eTRUE);


}


eOP_DEINIT()
{
    if(m_result.viewport)
    {

        m_result.viewport->GetScene()->RemoveChildren(true,true,false);
        m_result.viewport->GetScene()->RemoveAllComponents();
        m_result.viewport->GetScene()->Remove();
        m_result.viewport->GetScene()->ReleaseRef();
        m_result.viewport->GetCamera()->ReleaseRef();
        m_result.viewport->ReleaseRef();
    }
}


eOP_EXEC()
{

    if(m_result.viewport)
    {

        ///m_result.viewport->GetScene()->RemoveChildren(true,true,false);
        ///m_result.viewport->GetScene()->RemoveAllComponents();
        ///m_result.viewport->GetScene()->Remove();
        ///m_result.viewport->GetScene()->ReleaseRef();



        //m_result.viewport->GetScene()->GetChild(0U)->Remove();
        //m_result.viewport->GetScene()->GetChild(0U)->ReleaseRef();
        //m_result.viewport->GetScene()->GetChild(1U)->ReleaseRef();
        //m_result.viewport->GetScene()->ReleaseRef();

        ///m_result.viewport->GetCamera()->ReleaseRef();


        ///m_result.viewport->ReleaseRef();

        //m_result.viewport->GetCamera()->Remove();
        //m_result.viewport->GetScene()->ReleaseRef();
        //m_result.viewport->GetCamera()->Remove();

        //in0->getResult().sceneNode->Remove();
        //m_result.scene->Remove();
        //m_result.camera->Remove();
        //m_result.cameraNode->ReleaseRef();
    }

    //Scene* scene = nullptr;
    //Node* sceneNode = nullptr;



    const eIOpScene* in0Scene = (eIOpScene *)getAboveOp(0);
    eASSERT(in0Scene);
    Scene* scene = in0Scene->getResult().scene;



    /*Scene* scene = new Scene(eGraphics::m_gcontext);
    scene->AddRef();
    scene->CreateComponent<Octree>();

    //Node* rootNode = scene->CreateChild("RootNode");
    scene->AddChild(in0->getResult().sceneNode);
    //in0->getResult().sceneNode->AddRef();

    //Node* rootNode = in0->getResult().sceneNode->Clone();
    //in0->getResult().sceneNode->Remove();
    //Node* sceneNode = in0->getResult().sceneNode;

    //scene->AddChild(rootNode);*/





    // find the camera componnent from input 2

    Camera* camera = nullptr;

    //eINodeOp* cameraOp = eDemoData::findOperator(m_linkCameraOp->getBaseValue().linkedOpId);

    eIOpNode* inCameraNode = (eIOpNode *)getAboveOp(1);

    if(inCameraNode)
    {
        //PODVector<Node*> cameras;

        camera = inCameraNode->getResult().sceneNode->GetComponent<Camera>(true);

        scene->AddChild(inCameraNode->getResult().sceneNode);

        //cameraOp->getResult().sceneNode->AddRef();

        /*cameraOp->getResult().sceneNode->GetChildrenWithComponent<Camera>(cameras, true);
        if(cameras.Size() == 1)
        {
            Node* cameraNode = cameraOp->getResult().sceneNode; //new Node(eGraphics::m_gcontext);
            //m_result.cameraNode->AddRef();

            ///m_result.camera = cameras[0]->GetComponent<Camera>();
            //m_result.camera->AddRef();
            camera = cameras[0]->GetComponent<Camera>();


            scene->AddChild(cameraNode);

            //if(!m_result.scene->GetChildren().Contains(m_result.cameraNode))
            //    m_result.scene->AddChild(m_result.cameraNode);

            //if(!sceneNode->GetChildren().Contains(m_result.cameraNode))
            //    m_result.scene->AddChild(m_result.cameraNode);
        }*/
    }


    eASSERT(camera);
    //eASSERT(rootNode);


    if(m_result.viewport)
    {
        m_result.viewport->ReleaseRef();
    }

    m_result.viewport = new Viewport(context_);
    m_result.viewport->AddRef();
    m_result.viewport->SetScene(scene);
    m_result.viewport->SetCamera(camera);
    //camera->AddRef();



    // Get RenderPath from input 3

    const eIOpEffect *inRenderPath = (eIOpEffect *)getAboveOp(2);
    eASSERT(inRenderPath);
    m_result.viewport->SetRenderPath(inRenderPath->getResult().renderPath);



    /*ResourceCache* cache = eGraphics::m_gcontext->GetSubsystem<ResourceCache>();

    RenderPathCommand rpc;
    const char renderpathCommandTxt[] =
            "<command type=\"quad\" tag=\"GammaCorrection\" vs=\"GammaCorrection\" ps=\"GammaCorrection\" output=\"viewport\">"
            "        <texture unit=\"diffuse\" name=\"viewport\" />"
            "</command>";

    const String& patchString = renderpathCommandTxt;
    SharedPtr<XMLFile> patchFile(new XMLFile(context_));
    if (patchFile->FromString(renderpathCommandTxt))
    {
        rpc.Load(patchFile->GetRoot());
    }



    RenderPath* effectRenderPath = new RenderPath();
    effectRenderPath->Load(cache->GetResource<XMLFile>("RenderPaths/Forward.xml"));

    effectRenderPath->AddCommand(rpc);

    //RenderPath* effectRenderPath = new //m_result.viewport->GetRenderPath()->Clone();
    //effectRenderPath->Append(cache->GetResource<XMLFile>("PostProcess/Tonemap.xml"));
    //effectRenderPath->Append(cache->GetResource<XMLFile>("PostProcess/GammaCorrection.xml"));
   // effectRenderPath->Append(cache->GetResource<XMLFile>("PostProcess/FXAA2.xml"));

    //effectRenderPath->SetEnabled("TonemapReinhardEq3", false);
    //effectRenderPath->SetEnabled("TonemapUncharted2", true);


   //effectRenderPath->SetShaderParameter("TonemapExposureBias", Variant(1.0));


    m_result.viewport->SetRenderPath(effectRenderPath);*/





    //m_result.viewport->SetRenderPath(renderPath);




    //m_result.cameraNode = cameraOp->getResult().sceneNode; //new Node(eGraphics::m_gcontext);
    //m_result.cameraNode->AddRef();
    //m_result.cameraNode->SetPosition(Vector3(position.x, position.y, position.z));
    //m_result.cameraNode->LookAt(Vector3(lookAt.x, lookAt.y, lookAt.z));

    //m_result.camera = m_result.cameraNode->GetComponent<Camera>();
   // m_result.camera->SetZoom(zoom);

    //m_result.camera = m_result.cameraNode->CreateComponent<Camera>();
    //m_result.camera->SetZoom(zoom);

    /*m_result.scene = new Scene(eGraphics::m_gcontext);
    m_result.scene->CreateComponent<Octree>();
    m_result.scene->AddChild(in0->getResult().sceneNode);*/
    //m_result.scene->AddChild(m_result.cameraNode);



}

eOP_END(eViewportOp)





//!----------------------------------------------------------------------------------------------------------------------
//! AddComponents (node) operator
//!
//! Add components to a node
//!----------------------------------------------------------------------------------------------------------------------
eOP_DEF_NODE(eNodeAddComponentsOp, "AddComponents", 'c', 2, 32, eOP_INPUTS(eOC_NODE,eOP_32INPUTS(eOC_COMPONENT)))

eOP_DEINIT()
{
    /*if(m_result.sceneNode)
    {
        m_result.sceneNode->RemoveChildren(true,true,false);
        m_result.sceneNode->RemoveAllComponents();
        m_result.sceneNode->Remove();
        m_result.sceneNode->ReleaseRef();
    }*/

    freeResultNode();
}


    eOP_EXEC()
    {
        reAllocateResultNode();
        copyInputNode();

        for(eU32 i=1; i<getAboveOpCount(); i++)
        {
            const eIOpComponent* inComponent = (eIOpComponent *)getAboveOp(i);
            eASSERT(inComponent);
            m_result.sceneNode->CloneComponent(inComponent->getResult().component);
        }
    }

eOP_END(eNodeAddComponentsOp)















//!----------------------------------------------------------------------------------------------------------------------
//! Add (node) operator
//!
//! Create a new parent scene node for many scenes nodes
//!----------------------------------------------------------------------------------------------------------------------
eOP_DEF_NODE(eNodeMulOp, "Multiply", 'm', 1, 1, eOP_INPUTS(eOC_NODE))

eParameter* m_count;
eParameter* m_prescale;
eParameter* m_prerotate;
eParameter* m_pretranslate;
eParameter* m_scale;
eParameter* m_rotate;
eParameter* m_translate;
eParameter* m_label;



eOP_INIT()
{
    eOP_PAR_FXYZ(m_prescale, "PreScale", eF32_MIN, eF32_MAX, 1.0f, 1.0f, 1.0f);
    eOP_PAR_FXYZ(m_prerotate, "PreRotate", eF32_MIN, eF32_MAX, 0.0f, 0.0f, 0.0f);
    eOP_PAR_FXYZ(m_pretranslate, "PreTranslate", eF32_MIN, eF32_MAX, 0.0f, 0.0f, 0.0f);

    eOP_PAR_LABEL(m_label, "", "Multiply");

    eOP_PAR_INT(m_count, "Count", 1, 1024, 1);
    eOP_PAR_FXYZ(m_scale, "Scale", eF32_MIN, eF32_MAX, 1.0f, 1.0f, 1.0f);
    eOP_PAR_FXYZ(m_rotate, "Rotate", eF32_MIN, eF32_MAX, 0.0f, 0.0f, 0.0f);
    eOP_PAR_FXYZ(m_translate, "Translate", eF32_MIN, eF32_MAX, 0.0f, 0.0f, 0.0f);
}


eOP_DEINIT()
{
    /*if(m_result.sceneNode)
    {
        m_result.sceneNode->RemoveChildren(true,true,false);
        m_result.sceneNode->RemoveAllComponents();
        m_result.sceneNode->Remove();
        m_result.sceneNode->ReleaseRef();
    }*/
}


    eOP_EXEC()
    {


        reAllocateResultNode();
        copyInputNode();

        Vector3 pret(m_pretranslate->getBaseValue().fxyz.x, m_pretranslate->getBaseValue().fxyz.y, m_pretranslate->getBaseValue().fxyz.z);
        Quaternion prer(m_prerotate->getBaseValue().fxyz.x*e360, m_prerotate->getBaseValue().fxyz.y*e360, m_prerotate->getBaseValue().fxyz.z*e360);
        Vector3 pres(m_prescale->getBaseValue().fxyz.x, m_prescale->getBaseValue().fxyz.y, m_prescale->getBaseValue().fxyz.z);

        Vector3 t(m_translate->getBaseValue().fxyz.x, m_translate->getBaseValue().fxyz.y, m_translate->getBaseValue().fxyz.z);
        Quaternion r(m_rotate->getBaseValue().fxyz.x*e360, m_rotate->getBaseValue().fxyz.y*e360, m_rotate->getBaseValue().fxyz.z*e360);
        Vector3 s(m_scale->getBaseValue().fxyz.x, m_scale->getBaseValue().fxyz.y, m_scale->getBaseValue().fxyz.z);

        //88Node* node = m_result.sceneNode;

        Matrix3x4 premul(t,r,s);

        Matrix3x4 prepremul(pret,prer,pres);

       // Matrix3x4 mul = node->GetTransform() * premul;



        Vector3 t2;
        Quaternion r2;
        Vector3 s2;

        const eIOpNode *input = (eIOpNode *)getAboveOp(0);
        eASSERT(input);
        eASSERT(input->getResult().sceneNode);
        eASSERT(m_result.sceneNode);


        /*setTransform(m_prescale->getBaseValue().fxyz,
                     m_prerotate->getBaseValue().fxyz,
                     m_pretranslate->getBaseValue().fxyz);*/


        //Matrix3x4 mul = premul * input->getResult().sceneNode->GetTransform();
        Matrix3x4 mul = input->getResult().sceneNode->GetTransform();


        for(int i=1; i<m_count->getBaseValue().integer; i++)
        {

            Node* cloneNode = input->getResult().sceneNode->Clone();
            m_result.sceneNode->AddChild(cloneNode);

            mul = premul * mul;
            //mul = mul * premul;

            mul.Decompose(t2,r2,s2);
            cloneNode->SetTransform(t2,r2,s2);

        }



    }

eOP_END(eNodeMulOp)







//!----------------------------------------------------------------------------------------------------------------------
//! Scene (Scene) operator
//!
//! Create a new scene and attach nodes childs
//!----------------------------------------------------------------------------------------------------------------------
eOP_DEF_SCENE(eSceneNode, "Scene ", ' ', 1, 32, eOP_INPUTS(eOP_32INPUTS(eOC_NODE)))

    eOP_DEINIT()
    {
        if(m_result.scene)
        {
            m_result.scene->RemoveChildren(true,true,false);
            m_result.scene->RemoveAllComponents();
            m_result.scene->Remove();
        }
    }


    eOP_EXEC()
    {
        if(m_result.scene)
        {
            m_result.scene->RemoveChildren(true,true,false);
            m_result.scene->RemoveAllComponents();
            m_result.scene->Remove();
        }

        m_result.scene = new Scene(eGraphics::m_gcontext);
        m_result.scene->CreateComponent<Octree>();

        // get childs nodes from aboves op
        for(eU32 i=0; i<getAboveOpCount(); i++)
        {
            const eIOpNode *input = (eIOpNode *)getAboveOp(i);
            eASSERT(input);

            // add node
            //m_result.scene->AddChild(input->getResult().sceneNode);

            // clone node
            /*Node* tmp = new Node(context_);
            tmp->AddChild(input->getResult().sceneNode);
            m_result.scene->AddChild(input->getResult().sceneNode->Clone());
            tmp->RemoveChild(input->getResult().sceneNode);
            delete tmp;*/


            Node* root = m_result.scene->CreateChild("rottSceneNode");
            JSONValue jsonElement;
            input->getResult().sceneNode->SaveJSON(jsonElement);
            // m_result.scene->InstantiateJSON(jsonElement, Vector3(0,0,0), Quaternion(0, Vector3(0,0,0)));
            root->LoadJSON(jsonElement);

        }

        ePRINT("Scene (operator) - nb node in scene = %d", m_result.scene->GetNumChildren(true));
    }

eOP_END(eSceneNode)



//!----------------------------------------------------------------------------------------------------------------------
//! Clone (node) operator
//!
//! Clone scene node, components and child nodes.
//!----------------------------------------------------------------------------------------------------------------------
eOP_DEF_NODE(eNodeDeepClone, "Clone", 'a', 1,1, eOP_INPUTS(eOP_32INPUTS(eOC_NODE)))

    eOP_DEINIT()
    {
        freeResultNode();
    }

    eOP_EXEC()
    {
        reAllocateResultNode();

        for(eU32 i=0; i<getAboveOpCount(); i++)
        {
            cloneInputNode(i);

            /*const eINodeOp *input = (eINodeOp *)getAboveOp(i);

            eASSERT(input);
            eASSERT(input->getResult().sceneNode);

            Node* tmp = new Node(context_);
            tmp->AddChild(input->getResult().sceneNode);

            m_result.sceneNode->AddChild(input->getResult().sceneNode->Clone());

            tmp->RemoveChild(input->getResult().sceneNode);
            delete tmp;*/
        }
    }

eOP_END(eNodeDeepClone)
