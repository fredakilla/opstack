/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   This file is part of
 *       ______        _                             __ __
 *      / ____/____   (_)____ _ ____ ___   ____ _   / // /
 *     / __/  / __ \ / // __ `// __ `__ \ / __ `/  / // /_
 *    / /___ / / / // // /_/ // / / / / // /_/ /  /__  __/
 *   /_____//_/ /_//_/ \__, //_/ /_/ /_/ \__,_/     /_/.   
 *                    /____/                              
 *
 *   Copyright � 2003-2012 Brain Control, all rights reserved.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "../eshared.hpp"
#include "baseoperators/isequencer.hpp"


//!----------------------------------------------------------------------------------------------------------------------
//!
//! eIDemoOp
//!
//!----------------------------------------------------------------------------------------------------------------------

eIOperatorRenderer* eIOpDemo::createOperatorRenderer()
{
    return new eIOpDemoRenderer;
}







//!----------------------------------------------------------------------------------------------------------------------
//! Load (misc) operator
//!
//! Loads a previously saved operator and
//! passes the output to its output operators.
//!----------------------------------------------------------------------------------------------------------------------

#if defined(HAVE_OP_MISC_LOAD) || defined(eEDITOR)
eOP_DEF(eLoadOp, eIOpStructure, "Load", "Misc", eColor(170, 170, 170), 'l', eOC_ALL, 0, 0, eOP_INPUTS())

    eParameter* m_load;

    eOP_INIT()
    {
        eOP_PARAM2_ADD_LINK(eLoadOp, m_load, "Load", eOC_ALL, eTRUE);
    }

    //eOP_EXEC2(ENABLE_STATIC_PARAMS){}eOP_EXEC2_END
    /*eOP_EXEC2(DISABLE_STATIC_PARAMS,
        eOP_PAR_LINK(eLoadOp, loadedOp, "Load", eOC_ALL, eTRUE,
        eOP_PAR_END))
    {
    }
    eOP_EXEC2_END*/

    virtual eIOperator * _getRealOp() const
    {
        return eDemoData::findOperator(m_params[0]->getAnimValue().linkedOpId);
    }

eOP_END(eLoadOp)
#endif


//!----------------------------------------------------------------------------------------------------------------------
//! Store (misc) operator
//!
//! Stores an operator so it can be loaded
//! somewhere else using the load operator.
//!----------------------------------------------------------------------------------------------------------------------

#if defined(HAVE_OP_MISC_STORE) || defined(eEDITOR)
eOP_DEF(eStoreOp, eIOpStructure, "Store", "Misc", eColor(170, 170, 170), 's', eOC_ALL, 1, 1, eOP_INPUTS(eOC_ALL))

    /*eOP_EXEC2(ENABLE_STATIC_PARAMS,eOP_PAR_END)
    {
    }
    eOP_EXEC2_END*/
    //eOP_EXEC2(ENABLE_STATIC_PARAMS){}eOP_EXEC2_END

eOP_END(eStoreOp)
#endif


//!----------------------------------------------------------------------------------------------------------------------
//! Nop (misc) operator
//!
//! Does nothing more than forwarding the
//! input operator.
//!----------------------------------------------------------------------------------------------------------------------

#if defined(HAVE_OP_MISC_NOP) || defined(eEDITOR)
eOP_DEF(eNopOp, eIOpStructure, "Nop", "Misc", eColor(170, 170, 170), 'n', eOC_ALL, 1, 1, eOP_INPUTS(eOC_ALL))

    /*eOP_EXEC2(ENABLE_STATIC_PARAMS,eOP_PAR_END)
    {
    }
    eOP_EXEC2_END*/
    //eOP_EXEC2(ENABLE_STATIC_PARAMS){}eOP_EXEC2_END

eOP_END(eNopOp)
#endif


//!----------------------------------------------------------------------------------------------------------------------
//! Path (misc) operator
//!
//! Holds a path.
//!----------------------------------------------------------------------------------------------------------------------


#if defined(HAVE_OP_MISC_PATH) || defined(eEDITOR)
eOP_DEF(ePathOp, eIOpPath, "Path", "Misc", eColor(170, 170, 170), 'p', eOC_PATH, 0, 0, eOP_INPUTS())

    eParameter* m_pathPara;


#ifdef eEDITOR    
    eOP_INIT()
    {
        eOP_PAR_PATH(m_pathPara, "Path");

        ePath4 &path = m_pathPara->getBaseValue().path; // getParameter(0).getBaseValue().path;
        for (eInt i=0; i<4; i++)
        {
            path.getSubPath(i).addKey(0.0f, (eF32)i, ePI_CUBIC);
            path.getSubPath(i).addKey(1.0f, (eF32)i, ePI_CUBIC);
        }

        /*ePath4 &path = getParameter(0).getBaseValue().path;
        for (eInt i=0; i<4; i++)
        {
            path.getSubPath(i).addKey(0.0f, (eF32)i, ePI_CUBIC);
            path.getSubPath(i).addKey(1.0f, (eF32)i, ePI_CUBIC);
        }*/
    }
#endif

    void updatePath()
    {
        m_path = m_pathPara->getBaseValue().path;
    }


    void _callExecute2()
    {
        updatePath();
    }


    /*eOP_EXEC2(DISABLE_STATIC_PARAMS,
        eOP_PAR_PATH(ePathOp, path, "Path");
        eOP_PAR_END)
    {
        m_path = path;
    }
    eOP_EXEC2_END*/

eOP_END(ePathOp)
#endif



//!----------------------------------------------------------------------------------------------------------------------
//! Demo (misc) operator
//!
//! Operator which holds all data relevant for
//! a complete demo (sequencer + sound).
//!----------------------------------------------------------------------------------------------------------------------

/*
eOP_DEF(eDemoOp, eIDemoOp, "Demo", "Misc", eColor(170, 170, 170), 'd', eOC_DEMO, 1, 32, eOP_INPUTS(eOP_32INPUTS(eOC_SEQ)))
	eOP_INIT() 
	{
		m_songDelay = 0.0f;
	}

	eOP_EXEC2(DISABLE_STATIC_PARAMS,
        eOP_PAR_ENUM(eDemoOp, aspectRatioSel, "Letter box", "4:3|16:9|16:10|2.35:1", 0,
        eOP_PAR_STRING(eDemoOp, prodName, "Production name", "Unnamed",
        eOP_PAR_LINK_DEMO(eDemoOp, loadingScreenOp, "Loading screen", eOC_DEMO, eFALSE,
        eOP_PAR_FILE(eDemoOp, songPath, "Song", "",
		eOP_PAR_FLOAT(eDemoOp, songDelay, "Song delay", 0.0f, eF32_MAX, 0.0f,
		eOP_PAR_END))))))
    {
        const eF32 aspectRatios[] = {4.0f/3.0f, 16.0f/9.0f, 16.0f/10.0f, 2.35f};
        m_seq.setAspectRatio(aspectRatios[aspectRatioSel]);
        m_seq.clear();

        for (eU32 i=0; i<getAboveOpCount(); i++)
        {
            const eSequencer &curSeq = ((eISequencerOp *)getAboveOp(i))->getResult().seq;
            m_seq.merge(curSeq);
        }

        m_demo.setSequencer(&m_seq);

        // load song into TF4
        if (m_songPath != songPath || songDelay != m_songDelay)
        {
#ifdef ePLAYER
            const eByteArray &song = getParameter(3).getAnimValue().fileData;
#else
            const eByteArray &song = eFile::readAll(songPath);
#endif
            eTfPlayerLoadSong(*eSfx, (song.isEmpty() ? nullptr : &song[0]), song.size(), songDelay);
            m_songPath = songPath;
			m_songDelay = songDelay;
        }
    }
	eOP_EXEC2_END

    eOpProcessResult process(eF32 time, eOpCallback callback, ePtr param)
    {
        // process the whole stack
        if (m_processAll)
            return eIOperator::process(time, callback, param);

        // only process active sequencer operators
        eIOpPtrArray oldInOps = m_inputOps;
        m_inputOps.clear();

        for (eU32 i=0; i<oldInOps.size(); i++)
        {
            eISequencerOp *seqOp = (eISequencerOp *)oldInOps[i];
            const eF32 startTime = seqOp->getParameter(0).getAnimValue().flt;
            const eF32 duration = seqOp->getParameter(1).getAnimValue().flt;
            const eU32 track = seqOp->getParameter(2).getAnimValue().integer;

            if (time >= startTime && time <= startTime+duration)
            {
                seqOp->setSubTime(startTime);
                m_inputOps.append(seqOp);
            }
        }

        const eOpProcessResult res = eIOperator::process(time, callback, param);
        m_inputOps = oldInOps;

        for (eU32 i=0; i<m_inputOps.size(); i++)
            m_inputOps[i]->setSubTime(0.0f);

        return res;
    }

    eOP_VAR(eSequencer     m_seq);
    eOP_VAR(static eString m_songPath);
	eOP_VAR(eF32		   m_songDelay);
eOP_END(eDemoOp);

eString eDemoOp::m_songPath;
*/


eOP_DEF(eDemoOp, eIOpDemo, "Demo", "Misc", eColor(170, 170, 170), 'd', eOC_DEMO, 1, 32, eOP_INPUTS(eOP_32INPUTS(eOC_SEQ)))

eParameter* m_aspectRatioSel;
eParameter* m_prodName;
eParameter* m_loadingScreenOp;
eParameter* songPath;
eParameter* songDelay;

    eOP_INIT()
    {
        setProcessAll(eTRUE);

    eOP_PAR_ENUM(m_aspectRatioSel, "Letter box", "4:3|16:9|16:10|2.35:1", 0);
    eOP_PAR_STRING(m_prodName, "Production name", "Unnamed");
    eOP_PAR_LINK_DEMO(m_loadingScreenOp, "Loading screen", eOC_DEMO, eFALSE);
    eOP_PAR_FILE(songPath, "Song", "");
    eOP_PAR_FLOAT(songDelay, "Song delay", 0.0f, eF32_MAX, 0.0f);

        m_songDelay = 0.0f;
    }

    eOP_EXEC()
    {
        const eF32 aspectRatios[] = {4.0f/3.0f, 16.0f/9.0f, 16.0f/10.0f, 2.35f};
        m_seq.setAspectRatio(aspectRatios[m_aspectRatioSel->getBaseValue().enumSel]);
        m_seq.clear();

        for (eU32 i=0; i<getAboveOpCount(); i++)
        {
            const eSequencer &curSeq = ((eIOpSequencer *)getAboveOp(i))->getResult().seq;
            m_seq.merge(curSeq);
        }

        m_demo.setSequencer(&m_seq);

        // load song into TF4
        if (m_songPath != songPath->getBaseValue().string || songDelay->getBaseValue().flt != m_songDelay)
        {
#ifdef ePLAYER
            const eByteArray &song = getParameter(3).getAnimValue().fileData;
#else
            const eByteArray &song = eFile::readAll(songPath->getBaseValue().string);
#endif
           /*eTfPlayerLoadSong(*eSfx, (song.isEmpty() ? nullptr : &song[0]), song.size(), songDelay);
            m_songPath = songPath;
            m_songDelay = songDelay;*/
        }
    }

    eOpProcessResult process(eF32 time, eOpCallback callback, ePtr param)
    {
        // process the whole stack
        if (m_processAll)
            return eIOperator::process(time, callback, param);

        // only process active sequencer operators
        eIOpPtrArray oldInOps = m_inputOps;
        m_inputOps.clear();

        for (eU32 i=0; i<oldInOps.size(); i++)
        {
            eIOpSequencer *seqOp = (eIOpSequencer *)oldInOps[i];
            const eF32 startTime = seqOp->getParameter(0).getAnimValue().flt;
            const eF32 duration = seqOp->getParameter(1).getAnimValue().flt;
            const eU32 track = seqOp->getParameter(2).getAnimValue().integer;

            if (time >= startTime && time <= startTime+duration)
            {
                seqOp->setSubTime(startTime);
                m_inputOps.append(seqOp);
            }
        }

        const eOpProcessResult res = eIOperator::process(time, callback, param);
        m_inputOps = oldInOps;

        for (eU32 i=0; i<m_inputOps.size(); i++)
            m_inputOps[i]->setSubTime(0.0f);

        return res;
    }

    eOP_VAR(eSequencer     m_seq);
    eOP_VAR(static eString m_songPath);
    eOP_VAR(eF32		   m_songDelay);

eOP_END(eDemoOp)

eString eDemoOp::m_songPath;

/*
//!----------------------------------------------------------------------------------------------------------------------
//! Scene (misc) operator
//!
//! Holds all scene data organized in a spatial
//! subdivision data structure.
//!----------------------------------------------------------------------------------------------------------------------

#if defined(HAVE_OP_MISC_SCENE) || defined(eEDITOR)
eOP_DEF(eSceneFileOp, eISceneOp, "SceneFile", "Misc", eColor(170, 170, 170), 's', eOC_SCENE, 0, 0, eOP_INPUTS())
eOP_EXEC2(ENABLE_STATIC_PARAMS,
          eOP_PAR_FILE(eSceneFileOp, filePath, "File", "",
          eOP_PAR_END))
{
    //const eSceneData &sd = ((eIModelOp *)getAboveOp(0))->getResult().sceneData;
    //m_scene.setSceneData(sd);

    ResourceCache* cache = eGraphics::m_gcontext->GetSubsystem<ResourceCache>();

    m_res.scene = new Scene(eGraphics::m_gcontext);
    SharedPtr<File> file = cache->GetFile(filePath); // Scenes/SceneLoadExample.xml
    if(file)
        m_res.scene->LoadXML(*file);

}
eOP_EXEC2_END
eOP_END(eSceneFileOp);
#endif
*/


/*
//!----------------------------------------------------------------------------------------------------------------------
//! Scene (misc) operator
//!
//! Holds all scene data organized in a spatial
//! subdivision data structure.
//!----------------------------------------------------------------------------------------------------------------------

#if defined(HAVE_OP_MISC_SCENE) || defined(eEDITOR)
eOP_DEF(eSceneOp, eISceneOp, "Scene", "Misc", eColor(170, 170, 170), 's', eOC_SCENE, 1, 1, eOP_INPUTS(eOC_MODEL))
    eOP_EXEC2(ENABLE_STATIC_PARAMS,
          eOP_PAR_END)
    {

    }
eOP_EXEC2_END
eOP_END(eSceneOp);
#endif
*/
