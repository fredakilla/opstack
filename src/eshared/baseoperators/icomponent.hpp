#ifndef ICOMPONENT_HPP
#define ICOMPONENT_HPP

#include "eshared.hpp"
#include "baseoperators/irealnode.hpp"
#include "urhoobjects/uWz4Meshcomponent.hpp"

//!----------------------------------------------------------------------------------------------------------------------
//!
//! Base class for components operators
//!
//!----------------------------------------------------------------------------------------------------------------------
class eIOpComponent : public eIOperator
{
public:
    struct Result : public eOpResult
    {
        Result() :
            component(nullptr),
            node(nullptr)
        {
        }

        SharedPtr<Component>    component;  // result component of operator
        WeakPtr<Node>           node;       // node ptr where to add component
    };
    eIOperatorRenderer* createOperatorRenderer();

public:
    eIOpComponent()
    {
    }

    virtual const Result & getResult() const
    {
        return m_result;
    }

    void freeResult()
    {
        if(m_result.component)
            m_result.component->Remove();
    }

private:
    virtual void _preExecute()
    {
        // get node ptr
        const eIOperator* op = getInput<eIOperator>(0);
        if(op->getResultClass() == eOC_REALNODE)
        {
            const eIOpRealNode *input = (eIOpRealNode *)op;
            eASSERT(input->getResult().realNode);
            m_result.node = input->getResult().realNode;
        }
        else if(op->getResultClass() == eOC_COMPONENT)
        {
            const eIOpComponent *input = (eIOpComponent *)op;
            eASSERT(input->getResult().node);
            m_result.node = input->getResult().node;
        }
        else
        {
            eASSERT(0); // Unknown op above
        }

        // remove old component if exists
        freeResult();
    }

protected:
    Result      m_result;
};



//!----------------------------------------------------------------------------------------------------------------------
//!
//! A base renderer for components operators
//!
//!----------------------------------------------------------------------------------------------------------------------
class eIOpComponentRenderer : public eIOperatorRenderer
{

public:
    eIOpComponentRenderer()
    {
    }

    ~eIOpComponentRenderer()
    {
    }

    void init()
    {
    }

    void operatorUpdated(eIOperator* op)
    {
        const WeakPtr<Component> component = ((eIOpComponent*)op)->getResult().component;

        m_nodeRoot->RemoveAllComponents();
        m_nodeRoot->CloneComponent(component);

        ePRINT("Viewing component : %s", component->GetCategory().CString());
        ePRINT("m_nodeRoot child count : %d", m_nodeRoot->GetChildren().Size());
        ePRINT("m_nodeRoot component count : %d", m_nodeRoot->GetComponents().Size());
    }
};


#endif // ICOMPONENT_HPP
