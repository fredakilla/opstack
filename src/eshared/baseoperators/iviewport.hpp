#ifndef IVIEWPORT_HPP
#define IVIEWPORT_HPP

#include "eshared.hpp"

//!----------------------------------------------------------------------------------------------------------------------
//!
//!
//!
//!----------------------------------------------------------------------------------------------------------------------
class eIOpViewport : public eIOperator
{
    //URHO3D_OBJECT(eIViewportOp, Object)

public:
    struct Result : public eOpResult
    {
        Result() : cameraNode(nullptr), camera(nullptr), scene(nullptr)
        {
        }

        SharedPtr<Node>     cameraNode;
        SharedPtr<Camera>   camera;
        SharedPtr<Scene>    scene;
        SharedPtr<Viewport>    viewport;
    };
    eIOperatorRenderer* createOperatorRenderer();

public:
    eIOpViewport() : /*Object(eGraphics::m_gcontext),*/ m_result()
    {
    }


    virtual const Result & getResult() const
    {
        return m_result;
    }

private:
    virtual void _preExecute()
    {
    }

protected:
    Result      m_result;
};


//!----------------------------------------------------------------------------------------------------------------------
//!
//! A base renderer for viewport operators.
//!
//!----------------------------------------------------------------------------------------------------------------------
class eIOpViewportRenderer : public eIOperatorRenderer
{
public:
    eIOpViewportRenderer()
    {
    }

    ~eIOpViewportRenderer()
    {
    }

    void init()
    {
    }

    void operatorUpdated(eIOperator* op)
    {
        eIOpViewport* viewportOp = ((eIOpViewport*)op);

        //Node* sceneNode = viewportOp->getResult().cameraNode;
        //getRootNode()->AddChild(sceneNode);

        /*if(viewportOp->getResult().camera)
            getViewport()->SetCamera( viewportOp->getResult().camera );

        getViewport()->SetScene( viewportOp->getResult().scene );

        ePRINT("nb node in scene = %d", getViewport()->GetScene()->GetNumChildren(true));*/


        getViewport()->SetCamera(viewportOp->getResult().viewport->GetCamera());
        getViewport()->SetScene(viewportOp->getResult().viewport->GetScene());
        getViewport()->SetRenderPath(viewportOp->getResult().viewport->GetRenderPath());

        ePRINT("nb node in scene = %d", getViewport()->GetScene()->GetNumChildren(true));



    }

};



#endif // IVIEWPORT_HPP
