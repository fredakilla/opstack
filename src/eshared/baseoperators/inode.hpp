#ifndef INODE_HPP
#define INODE_HPP

#include "eshared.hpp"
#include "urhoobjects/uWz4Meshcomponent.hpp"

//!----------------------------------------------------------------------------------------------------------------------
//!
//! Base class for node operators based on Urho3D Nodes
//!
//!----------------------------------------------------------------------------------------------------------------------
class eIOpNode : public eIOperator
{

public:
    struct Result : public eOpResult
    {
        Result() : sceneNode(nullptr)
        {
        }

        SharedPtr<Node> sceneNode;
    };
    eIOperatorRenderer* createOperatorRenderer();

public:
    eIOpNode() : m_result()
    {
        m_result.sceneNode = nullptr;
    }


    virtual const Result & getResult() const
    {
        return m_result;
    }

protected:

    void allocateResultNode()
    {
        m_result.sceneNode = new Node(eGraphics::m_gcontext);
    }

    void freeResultNode()
    {
        if(m_result.sceneNode)
        {
            m_result.sceneNode->RemoveChildren(true,true,false);
            m_result.sceneNode->Remove();
        }
    }

    void reAllocateResultNode()
    {
        freeResultNode();
        allocateResultNode();
    }

    void copyInputNode(eU16 index = 0U)
    {
        /// Copy Node way
        /// - can't reuse this node in others scenes
        const eIOpNode *input = (eIOpNode *)getAboveOp(index);
        eASSERT(input);
        eASSERT(input->getResult().sceneNode);
        eASSERT(m_result.sceneNode);
        m_result.sceneNode->AddChild(input->getResult().sceneNode);
    }

    void cloneInputNode(eU16 index = 0U)
    {
        /// Clone Node way (allowed to reuse this node in others scenes)
        /// - fix works in multiple scenes - is slower at copy ??
        /// - but animation is broken
        const eIOpNode *input = (eIOpNode *)getAboveOp(index);
        eASSERT(input);
        eASSERT(input->getResult().sceneNode);
        eASSERT(m_result.sceneNode);
        Node* tmp = new Node(context_);
        tmp->AddChild(input->getResult().sceneNode);
        m_result.sceneNode->AddChild(input->getResult().sceneNode->Clone());
        tmp->RemoveChild(input->getResult().sceneNode);
        delete tmp;

    }



private:
    virtual void _preExecute()
    {
    }

protected:
    Result      m_result;
};



//!----------------------------------------------------------------------------------------------------------------------
//!
//! A base class for nodes operators
//!
//!----------------------------------------------------------------------------------------------------------------------

class eIOpNodeRenderer : public eIOperatorRenderer
{

public:
    eIOpNodeRenderer()
    {
        SubscribeToEvent(E_POSTRENDERUPDATE, URHO3D_HANDLER(eIOpNodeRenderer, drawDebug));
    }

    ~eIOpNodeRenderer()
    {
    }

    void init()
    {
    }




    void operatorUpdated(eIOperator* op)
    {
       /* //getRootNode()->RemoveChildren(true, true, false);
        Node* sceneNode = ((eINodeOp*)op)->getResult().sceneNode;
        //getRootNode()->AddChild(sceneNode);
        //sceneNode->SetParent(getDefaultScene());
        sceneNode->SetParent(getRootNode());

       ePRINT("nb node in scene = %d", getDefaultScene()->GetNumChildren(true));*/



       m_nodeRoot->RemoveChildren(true, true, false);
       m_nodeRoot->AddChild(((eIOpNode*)op)->getResult().sceneNode);

       ePRINT("nb node in scene = %d", m_nodeRoot->GetNumChildren(true));

    }

    void drawDebug(Urho3D::StringHash eventType, Urho3D::VariantMap& eventData)
    {
        DebugRenderer * dbgRenderer = eGraphics::m_gdebuRenderer;
        if (dbgRenderer)
        {
            // Lights
            PODVector<Light*> lights;
            getDefaultScene()->GetComponents<Light>(lights, true);
            for (unsigned i = 0; i < lights.Size(); ++i)
            {
                lights[i]->DrawDebugGeometry(dbgRenderer,true);
            }

            // StaticModels
            PODVector<StaticModel*> models;
            getDefaultScene()->GetComponents<StaticModel>(models, true);
            for (unsigned i = 0; i < models.Size(); ++i)
            {
                models[i]->DrawDebugGeometry(dbgRenderer,true);
            }

            // Wz4Meshes
            PODVector<uWz4MeshComponent*> meshes;
            getDefaultScene()->GetComponents<uWz4MeshComponent>(meshes, true);
            for (unsigned i = 0; i < meshes.Size(); ++i)
            {
                meshes[i]->DrawDebugGeometry(dbgRenderer,true);
            }

        }
    }
};

#endif // INODE_HPP


