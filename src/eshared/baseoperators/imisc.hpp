#ifndef IMISC_OPS_HPP
#define IMISC_OPS_HPP

#include "eshared.hpp"

//!----------------------------------------------------------------------------------------------------------------------
//!
//! collects sequencer operators and creates a demo
//!
//!----------------------------------------------------------------------------------------------------------------------
class eIOpDemo : public eIOperator
{
public:
    struct Result : public eOpResult
    {
        eDemo demo;
    };
    eIOperatorRenderer* createOperatorRenderer();

public:
    eIOpDemo() :
        m_demo(m_res.demo),
        m_processAll(eFALSE)
    {
    }

    void setProcessAll(eBool processAll)
    {
        m_processAll = processAll;
    }

    eBool getProcessAll() const
    {
        return m_processAll;
    }

    virtual const Result & getResult() const
    {
        return m_res;
    }

protected:
    eDemo & m_demo;
    Result  m_res;
    eBool   m_processAll;
};




//!----------------------------------------------------------------------------------------------------------------------
//!
//! base class for path operator
//!
//!----------------------------------------------------------------------------------------------------------------------
class eIOpPath : public eIOperator
{
public:
    struct Result : public eOpResult
    {
        ePath4 path;
    };

public:
    eIOpPath() : m_path(m_res.path)
    {
    }

    virtual const Result & getResult() const
    {
        return m_res;
    }

    virtual void updatePath() {}

protected:
    ePath4 & m_path;
    Result   m_res;
};


//!----------------------------------------------------------------------------------------------------------------------
//!
//! Base class for load, store and nop operators
//!
//!----------------------------------------------------------------------------------------------------------------------
class eIOpStructure : public eIOperator
{
public:
    virtual const eOpResult & getResult() const
    {
        return _getRealOp()->getResult();
    }

    eIOperatorRenderer* createOperatorRenderer()
    {
        return _getRealOp()->createOperatorRenderer();
    }

protected:
    virtual eIOperator * _getRealOp() const
    {
        return (m_aboveOps.size() > 0 ? m_aboveOps[0] : nullptr);
    }
};


/*
//!----------------------------------------------------------------------------------------------------------------------
//!
//! Generic operator.
//!
//!----------------------------------------------------------------------------------------------------------------------
class eIGenericOp : public eIOperator
{
public:
    struct Result : public eOpResult
    {
        Result(void* &gd) : genericDataPtr(gd)
        {
        }

        (void*) & genericDataPtr;
    };

public:
    eIGenericOp() : m_result(m_genData)
    {
    }

    virtual const Result & getResult() const
    {
        return m_result;
    }

protected:
    void*   m_genData;
    Result  m_result;
};
*/





//!----------------------------------------------------------------------------------------------------------------------
//!
//! A renderer for demo operators.
//!
//!----------------------------------------------------------------------------------------------------------------------
class eIOpDemoRenderer : public eIOperatorRenderer
{
public:
    eIOpDemoRenderer() :
        m_time(0.0f)
    {

        const char renderpath[] =
                "<renderpath>"
                    //"<rendertarget name='mixed' sizedivisor='1 1' format='rgba' /> "
                    //"<rendertarget name='mixed2' sizedivisor='1 1' format='rgba' /> "
                "</renderpath>";


        const String& patchString = renderpath;
        if (!patchString.Empty())
        {
            SharedPtr<XMLFile> patchFile(new XMLFile(context_));
            if (patchFile->FromString(patchString))
            {
                //Renderer* renderer = GetSubsystem<Renderer>();
                //Viewport* viewport = renderer->GetViewport(0);
                getViewport()->SetRenderPath(patchFile);
            }
        }



    }

    ~eIOpDemoRenderer()
    {
    }

    void init()
    {
    }

    void operatorUpdated(eIOperator* op)
    {
        //((eIDemoOp *)node)->getResult().demo.render(time);
        /*         op->getResult().demo.render(m_time);

        eIDemoOp* demoOp = (eIDemoOp*)node;
        eDemo* demo = demoOp->getResult();
        demo->render(0.4f);*/


        ((eIOpDemo *)op)->getResult().demo.render(m_time);
    }


    void setTimeLine(eF32 time)
    {
        m_time = time;
    }
    eF32 m_time;    // timeline
};




#endif // IMISC_OPS_HPP
