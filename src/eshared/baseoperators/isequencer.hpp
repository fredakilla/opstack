#ifndef ISEQUENCER_HPP
#define ISEQUENCER_HPP

#include "eshared.hpp"

//!----------------------------------------------------------------------------------------------------------------------
//! eISequencerOp
//!----------------------------------------------------------------------------------------------------------------------
class eIOpSequencer : public eIOperator
{
public:
    struct Result : public eOpResult
    {
        eSequencer seq;
    };

    eIOperatorRenderer* createOperatorRenderer();

public:
    eIOpSequencer() : m_seq(m_res.seq)
    {
    }

    virtual const Result & getResult() const
    {
        return m_res;
    }

private:
    virtual void _preExecute()
    {
        m_seq.clear();
    }

protected:
    eSequencer & m_seq;
    Result       m_res;
};



//!----------------------------------------------------------------------------------------------------------------------
//!
//! A base renderer for Sequencer operators.
//!
//!----------------------------------------------------------------------------------------------------------------------
class eIOpSequencerRenderer : public eIOperatorRenderer
{
public:
    eIOpSequencerRenderer()
    {
    }

    ~eIOpSequencerRenderer()
    {
    }

    void init()
    {
    }

    void operatorUpdated(eIOperator* op)
    {
        //((eISequencerOp*)op)->getResult().seq.run(0.25f,0,0);

        /*eSeqEntry seq = ((eISequencerOp*)node)->getResult().seq.getEntriesOfTrack(0)[0];

         Node* cameraNode = seq.scene.urhoScene->CreateChild("Camera");
         Camera* camera = cameraNode->CreateComponent<Camera>();

         cameraNode->SetPosition(Vector3(0,2,-10));

         getViewport()->SetScene(seq.scene.urhoScene);
         getViewport()->SetCamera(camera);*/






        eSeqEntry seq = ((eIOpSequencer*)op)->getResult().seq.getEntriesOfTrack(0)[0];
        RenderPath* rp = getViewport()->GetRenderPath();
        rp->RemoveCommands("ClipSequence");
        rp->AddCommand(*seq.urho.renderPathCommand);












        /*RenderPathCommand rpc;
        rpc.type_ = CMD_QUAD;
        rpc.vertexShaderName_ = "CopyFramebufferFlipped";
        rpc.pixelShaderName_ = "CopyFramebufferFlipped";
        rpc.blendMode_ = BlendMode::BLEND_ADD;
        rpc.SetOutput(0, "viewport");
        rpc.SetTextureName(TU_DIFFUSE, "myrt");

        RenderPath* rp = getViewport()->GetRenderPath();
        rp->AddCommand(rpc);*/


        /*const char renderpath[] =
                "<renderpath>"
                "    <command type=\"clear\" color=\"fog\" depth=\"1.0\" stencil=\"0\" />"
                "    <command type=\"scenepass\" pass=\"base\" vertexlights=\"false\" metadata=\"base\" />"
                "    <command type=\"forwardlights\" pass=\"light\" />"
                "    <command type=\"scenepass\" pass=\"postopaque\" />"
                "    <command type=\"scenepass\" pass=\"refract\">"
                "        <texture unit=\"environment\" name=\"viewport\" />"
                "    </command>"
                "    <command type=\"scenepass\" pass=\"alpha\" vertexlights=\"true\" sort=\"backtofront\" metadata=\"base\" />"
                "    <command type=\"scenepass\" pass=\"postalpha\" sort=\"backtofront\" />"

                "<command type=\"quad\" vs=\"CopyFramebufferFlipped\" ps=\"CopyFramebufferFlipped\" blend=\"replace\" output=\"viewport\">"
                  "<texture unit=\"diffuse\" name=\"myrt\" />"
                "</command>"

                "</renderpath>";*/


        /*const String& patchString = renderpath;
        if (!patchString.Empty())
        {
            SharedPtr<XMLFile> patchFile(new XMLFile(context_));
            if (patchFile->FromString(patchString))
            {
                getViewport()->SetRenderPath(patchFile);
            }
        }*/




    }
};


#endif // ISEQUENCER_HPP
