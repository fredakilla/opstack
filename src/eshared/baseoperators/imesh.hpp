#ifndef IMESHOP_HPP
#define IMESHOP_HPP

#include "eshared.hpp"
#include "urhoobjects/uWz4Meshcomponent.hpp"

//!----------------------------------------------------------------------------------------------------------------------
//!
//! Base class for mesh operators based on wz4 mesh library
//!
//!----------------------------------------------------------------------------------------------------------------------
class eIOpMesh : public eIOperator
{
public:
    struct Result : public eOpResult
    {
        wz4::Wz4Mesh* mesh;
    };

    eIOperatorRenderer* createOperatorRenderer();

    eIOpMesh() : m_mesh(m_res.mesh)
    {
        m_res.mesh = nullptr;
    }

    ~eIOpMesh()
    {
        freeResult();
    }

    virtual const Result & getResult() const
    {
        return m_res;
    }

    virtual eU32 getResultSize() const
    {
        if(m_mesh)
        {
            return  m_mesh->Vertices.GetCount() * sizeof(wz4::Wz4MeshVertex) +
                    m_mesh->Faces.GetCount() * sizeof(wz4::Wz4MeshFace) +
                    m_mesh->Clusters.GetCount() * sizeof(wz4::Wz4MeshCluster) +
                    m_mesh->Chunks.GetCount() * sizeof(wz4::Wz4ChunkPhysics);
        }
        else
            return 0;
    }

    virtual void freeResult()
    {
        eSAFE_DELETE(m_mesh);
    }


protected:
    virtual void _preExecute()
    {
        eSAFE_DELETE(m_mesh);
        m_mesh = new wz4::Wz4Mesh();
    }

    void copyInput(eU32 index)
    {
        m_mesh->CopyFrom(((eIOpMesh *)getAboveOp(index))->getResult().mesh);
    }

    void _selectPrimitive(eBool &selected, eInt mode)
    {
        /*eASSERT(mode >= 0 && mode <= 3);

        const eBool todo[4] =
        {
            eTRUE,      // set
            eTRUE,      // add
            eFALSE,     // remove
            !selected   // toggle
        };

        selected = todo[mode];*/
    }

protected:
    wz4::Wz4Mesh*&  m_mesh;
    Result          m_res;
};



//!----------------------------------------------------------------------------------------------------------------------
//!
//! A base renderer for mesh operators
//!
//!----------------------------------------------------------------------------------------------------------------------
class eIOpMeshRenderer : public eIOperatorRenderer
{
public:
    eIOpMeshRenderer();
    ~eIOpMeshRenderer();
    void init();
    void operatorUpdated(eIOperator* op);

private:
    SharedPtr<Node>                 m_node;         // scene node to hold the mesh to render
    SharedPtr<uWz4MeshComponent>     m_staticModel;  // wz4mesh node component
};





#endif // IMESHOP_HPP
