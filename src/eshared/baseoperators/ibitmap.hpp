#ifndef IBITMAP_HPP
#define IBITMAP_HPP

#include "eshared.hpp"

//!----------------------------------------------------------------------------------------------------------------------
//!
//! Base class for bitmap operators based on wz4 bitmap library
//!
//!----------------------------------------------------------------------------------------------------------------------
class eIOpBitmap : public eIOperator
{
public:
    struct Result : public eOpResult
    {
        eU32                width;
        eU32                height;
        wz4::Wz4Bitmap*     bitmap;
    };
    eIOperatorRenderer* createOperatorRenderer();

public:
    eIOpBitmap();
    virtual ~eIOpBitmap();

    virtual const Result &  getResult() const;
    virtual eU32            getResultSize() const;
    virtual void            freeResult();

private:
    virtual void            _preExecute();

protected:
    void                    reallocate(eU32 width, eU32 height);
    void                    allocateBitmap(eU32 width, eU32 height, wz4::Wz4Bitmap *&bitmap) const;
    void                    copyInput(eU32 index);

protected:
    wz4::Wz4Bitmap *&       m_bitmap;
    eU32 &                  m_bmpWidth;
    eU32 &                  m_bmpHeight;
    Result                  m_res;
};



//!----------------------------------------------------------------------------------------------------------------------
//!
//! A base renderer for bitmaps operators
//!
//!----------------------------------------------------------------------------------------------------------------------
class eIOpBitmapRenderer : public eIOperatorRenderer
{
public:

    eIOpBitmapRenderer();
    ~eIOpBitmapRenderer();
    void init();
    void operatorUpdated(eIOperator* op);
    void subscribeToEvents();

private:

    void updateZoomAndPos();
    void createBitmap(int width, int height);
    void updateInfoText();
    void HandleMouseMove(StringHash eventType, VariantMap& eventData);
    void HandleMouseWheel(StringHash eventType, VariantMap& eventData);
    void HandleKeyDown(StringHash eventType, VariantMap& eventData);
    void HandleMousePressed(StringHash eventType, VariantMap& eventData);
    void HandleMouseReleased(StringHash eventType, VariantMap& eventData);

    eU8*                    _textureBitmapData;         // displayed texture data buffer
    eU8*                    _textureBackgroundData;     // background checker data buffer
    SharedPtr<Sprite>       _spriteBackground;          // UI sprite for background
    SharedPtr<Sprite>       _spriteBitmap;              // UI sprite for bitmap
    SharedPtr<Texture2D>    _texBitmap;                 // texture 2D for bitmap
    SharedPtr<Text>         _text;                      // text about bitmap infos
    float                   _zoom;                      // zoom factor
    SharedPtr<Scene>        _scene;                     // scene
    bool                    _beginDrag;

};


#endif // IBITMAP_HPP
