#include "ibitmap.hpp"

//-------------------------------------------------------------------------------------------------------------------------
//!
//! eIBitmapOp
//!
//-------------------------------------------------------------------------------------------------------------------------

eIOpBitmap::eIOpBitmap() :
    m_bmpWidth(m_res.width),
    m_bmpHeight(m_res.height),
    m_bitmap(m_res.bitmap)
{
    m_bmpWidth = 0;
    m_bmpHeight = 0;
    m_bitmap = nullptr;
}

eIOperatorRenderer* eIOpBitmap::createOperatorRenderer()
{
    m_opRenderer = new eIOpBitmapRenderer;
    return m_opRenderer;
}

eIOpBitmap::~eIOpBitmap()
{
    eSAFE_DELETE(m_bitmap);
}

const eIOpBitmap::Result & eIOpBitmap::getResult() const
{
    return m_res;
}

eU32 eIOpBitmap::getResultSize() const
{
    return (m_bitmap ? m_bitmap->Size * sizeof(eU8) * 4 : 0);
}

void eIOpBitmap::freeResult()
{
    eSAFE_DELETE(m_bitmap);
}

void eIOpBitmap::_preExecute()
{
    // bitmap operators always operate on smallest input bitmap operator size

    if (getAboveOpCount() > 0)
    {
        eSize size(eS32_MAX, eS32_MAX);

        for (eU32 i=0; i<getAboveOpCount(); i++)
        {
            const Result &res = ((eIOpBitmap *)getAboveOp(i))->getResult();
            size.minComponents(eSize(res.bitmap->XSize, res.bitmap->YSize));
        }

        reallocate(size.x, size.y);
    }
}

void eIOpBitmap::reallocate(eU32 width, eU32 height)
{
    allocateBitmap(width, height, m_bitmap);
    m_bmpWidth = width;
    m_bmpHeight = height;
}

void eIOpBitmap::allocateBitmap(eU32 width, eU32 height, wz4::Wz4Bitmap *&bitmap) const
{
    eASSERT(eIsPowerOf2(width) && eIsPowerOf2(height));

    if (!bitmap || bitmap->XSize != width || bitmap->YSize != height)
    {
        eSAFE_DELETE(bitmap);
        bitmap = new wz4::Wz4Bitmap();
        bitmap->Init(width ,height);
    }
}

void eIOpBitmap::copyInput(eU32 index)
{
    m_bitmap->CopyFrom(((eIOpBitmap *)getAboveOp(index))->getResult().bitmap);
}





const int TEX_BACKGROUND_SIZE = 128;

//!----------------------------------------------------------------------------------------------------------------------
//!
//! Wz4BitmapOpRenderer
//!
//!----------------------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------------------------------
eIOpBitmapRenderer::eIOpBitmapRenderer()
{
    eWriteToLog("create Wz4BitmapOpRenderer");

    _textureBitmapData = nullptr;
    _textureBackgroundData = nullptr;
    _texBitmap = 0;
    _spriteBitmap = 0;
    _zoom = 1.0f;
    _beginDrag = false;

    HideGrid();

    // add shortcuts
    //getShortcutsManager()->add("Alpha blending",    KEY_A);
    //getShortcutsManager()->add("Bilinear Filter",   KEY_B);
    //getShortcutsManager()->add("Nearest Filter",    KEY_N);
    //getShortcutsManager()->add("Reset",             KEY_R);

    // create background checker

    // create a wz4 checker texture
    wz4::Wz4Bitmap gb;
    gb.Init(TEX_BACKGROUND_SIZE, TEX_BACKGROUND_SIZE);
    gb.Checker(0xFFFFFFFF, 0xFF666666, 1, 1);

    // copy checker texture to texture object
    _textureBackgroundData = new eU8[gb.Size * 4 * sizeof(eU8)];
    gb.CopyTo((eU8*)_textureBackgroundData);

    // create texture
    Texture2D * texture = new Texture2D(context_);
    texture->SetFilterMode(TextureFilterMode::FILTER_NEAREST);
    texture->SetNumLevels(0);
    texture->SetSize(TEX_BACKGROUND_SIZE, TEX_BACKGROUND_SIZE, Graphics::GetRGBAFormat());
    texture->SetData(0, 0, 0, TEX_BACKGROUND_SIZE, TEX_BACKGROUND_SIZE, _textureBackgroundData);

    // create a new sprite, set it to use the texture
    _spriteBackground = new Sprite(context_);
    _spriteBackground->SetBlendMode(BlendMode::BLEND_REPLACE);
    _spriteBackground->SetTexture(texture);
    _spriteBackground->SetPosition(Vector2(0, 0));
    _spriteBackground->SetSize(TEX_BACKGROUND_SIZE, TEX_BACKGROUND_SIZE);
    _spriteBackground->SetScale(25.0f);
    _spriteBackground->SetVisible(false);

    ResourceCache* cache = GetSubsystem<ResourceCache>();

    // create text info
    _text = new Text(context_);
    _text->SetFont(cache->GetResource<Font>("Fonts/BlueHighway.ttf"), 10);
    _text->SetColor(Color(0,0,0));
    _text->SetHorizontalAlignment(HA_LEFT);
    _text->SetVerticalAlignment(VA_TOP);

    // create UI element to contain all sub-elements
    createRootUI();
    getRootUI()->AddChild(_spriteBackground);
    getRootUI()->AddChild(_text);
    getRootUI()->SetName("UI_Wz4BitmapOpRenderer");






    eSAFE_DELETE_ARRAY(_textureBitmapData);


    // all this stuff just for for setting background color ?

   /* _scene = new Scene(context_);
    _scene->CreateComponent<Octree>();

    Node* cameraNode = _scene->CreateChild("Camera");
    Camera* camera = cameraNode->CreateComponent<Camera>();


    getViewport()->SetScene(_scene);
    getViewport()->SetCamera(camera);

    const char renderpath[] =
            "<renderpath>"
            "    <command type=\"clear\" color=\"0.6 0.6 0.6 1.0\" depth=\"1.0\" stencil=\"0\" />"
            "    <command type=\"scenepass\" pass=\"base\" vertexlights=\"false\" metadata=\"base\" />"
            //"    <command type=\"forwardlights\" pass=\"light\" />"
            //"    <command type=\"scenepass\" pass=\"postopaque\" />"
            //"    <command type=\"scenepass\" pass=\"refract\">"
            //"        <texture unit=\"environment\" name=\"viewport\" />"
            //"    </command>"
            //"    <command type=\"scenepass\" pass=\"alpha\" vertexlights=\"true\" sort=\"backtofront\" metadata=\"base\" />"
            //"    <command type=\"scenepass\" pass=\"postalpha\" sort=\"backtofront\" />"
            "</renderpath>";


    const String& patchString = renderpath;
    if (!patchString.Empty())
    {
        SharedPtr<XMLFile> patchFile(new XMLFile(context_));
        if (patchFile->FromString(patchString))
        {
            //Renderer* renderer = GetSubsystem<Renderer>();
            //Viewport* viewport = renderer->GetViewport(0);
            getViewport()->SetRenderPath(patchFile);
        }
    }*/
}


//------------------------------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------------------------------
eIOpBitmapRenderer::~eIOpBitmapRenderer()
{
    eSAFE_DELETE_ARRAY(_textureBitmapData);
    eSAFE_DELETE_ARRAY(_textureBackgroundData);

    // remove all sub-elements from UI
    UIElement* element = getRootUI();
    if(element)
    {
        element->RemoveAllChildren();
    }
}

//------------------------------------------------------------------------------------------------------
// Init event
//------------------------------------------------------------------------------------------------------
void eIOpBitmapRenderer::init()
{

}

//------------------------------------------------------------------------------------------------------
// Called by system when window get mouse.
// Subscribe to all events
//------------------------------------------------------------------------------------------------------
void eIOpBitmapRenderer::subscribeToEvents()
{
    // subscribe to events
    //SubscribeToEvent(E_GENOD_MOUSEBUTTONPRESSED,    URHO3D_HANDLER(Wz4BitmapOpRenderer, HandleMousePressed      ));
    //SubscribeToEvent(E_GENOD_MOUSEBUTTONRELEASED,   URHO3D_HANDLER(Wz4BitmapOpRenderer, HandleMouseReleased     ));
    SubscribeToEvent(E_MOUSEMOVE,                   URHO3D_HANDLER(eIOpBitmapRenderer, HandleMouseMove         ));
    SubscribeToEvent(E_MOUSEMOVE,                   URHO3D_HANDLER(eIOpBitmapRenderer, HandleMouseMove         ));
    SubscribeToEvent(E_KEYDOWN,                     URHO3D_HANDLER(eIOpBitmapRenderer, HandleKeyDown           ));
    SubscribeToEvent(E_MOUSEWHEEL,                  URHO3D_HANDLER(eIOpBitmapRenderer, HandleMouseWheel        ));

    SubscribeToEvent(E_MOUSEBUTTONDOWN,    URHO3D_HANDLER(eIOpBitmapRenderer, HandleMousePressed      ));
    SubscribeToEvent(E_MOUSEBUTTONUP,   URHO3D_HANDLER(eIOpBitmapRenderer, HandleMouseReleased     ));
}


//------------------------------------------------------------------------------------------------------
// Create bitmap or recreate it
//------------------------------------------------------------------------------------------------------
void eIOpBitmapRenderer::createBitmap(int width, int height)
{
    // delete objects if already exixts
    if(_texBitmap)
    {
        //_texBitmap->Release();
        //_texBitmap->ReleaseRef();
    }
    if(_spriteBitmap)
    {
        getRootUI()->RemoveChild(_spriteBitmap);
        //_spriteBitmap->ReleaseRef();
    }

    // Create a new texture
    _texBitmap = new Texture2D(context_);
    _texBitmap->SetMipsToSkip(QUALITY_LOW, 0);
    _texBitmap->SetNumLevels(1);
    _texBitmap->SetFilterMode(FILTER_NEAREST);
    _texBitmap->SetAddressMode(COORD_U, ADDRESS_CLAMP);
    _texBitmap->SetAddressMode(COORD_V, ADDRESS_CLAMP);
    _texBitmap->SetSize(width, height, Graphics::GetRGBAFormat() );
    //_texBitmap->AddRef();

    // Create a new sprite, set it to use the texture
    Vector2 spriteCenter(width/2, height/2);

    _spriteBitmap = new Sprite(context_);
    _spriteBitmap->SetTexture(_texBitmap);
    _spriteBitmap->SetPosition(spriteCenter.x_ + 16, spriteCenter.y_ + 16 + _text->GetHeight());
    _spriteBitmap->SetSize(IntVector2(width, height));
    _spriteBitmap->SetHotSpot(IntVector2(spriteCenter.x_, spriteCenter.y_));
    _spriteBitmap->SetBlendMode(BLEND_ALPHA);
    _spriteBitmap->AddChild(_text);
    _spriteBitmap->SetChildOffset(IntVector2(0, -(_text->GetHeight() + 8) ));
    //_spriteBitmap->AddRef();

    // restore last zoom if exists
    updateZoomAndPos();

    // create new memory block for texture data
    if(_textureBitmapData)
        eSAFE_DELETE_ARRAY(_textureBitmapData);
    _textureBitmapData = new eU8[width * height * 4 * sizeof(eU8)];

    // add sprite to ui
    getRootUI()->AddChild(_spriteBitmap);
}

//------------------------------------------------------------------------------------------------------
// set zoom on bitmap sprite
//------------------------------------------------------------------------------------------------------
void eIOpBitmapRenderer::updateZoomAndPos()
{
    // set zoom limits
    _zoom = eMax(0.03125f, _zoom);
    _zoom = eMin(64.0f, _zoom);

    // zoom sprite
    _spriteBitmap->SetScale(_zoom);

    // adjust text position
    //_text->SetPosition(_spriteBitmap->GetPosition().x_, (_spriteBitmap->GetPosition().y_ + _texBitmap->GetHeight()) * _zoom  + _text->GetHeight() + 2);

    // update text
    updateInfoText();
}

//------------------------------------------------------------------------------------------------------
// update text infos
//------------------------------------------------------------------------------------------------------
void eIOpBitmapRenderer::updateInfoText()
{
    const char* filterNames[] = { "UNKNOWN", "NEAREST", "BILINEAR" };

    int index = 0;
    if(_texBitmap->GetFilterMode() == FILTER_NEAREST)
        index = 1;
    else if(_texBitmap->GetFilterMode() == FILTER_BILINEAR)
        index = 2;
    else
        index = 0;

    char buffer[255];
    sprintf(buffer, "%d x %d. zoom = %.2f. filter = %s",
            _texBitmap->GetWidth(),
            _texBitmap->GetHeight(),
            _zoom,
            filterNames[index]
            );

    _text->SetText(String(buffer));
}

//------------------------------------------------------------------------------------------------------
// key event
//------------------------------------------------------------------------------------------------------
void eIOpBitmapRenderer::HandleKeyDown(StringHash eventType, VariantMap& eventData)
{
    /*
    // get pressed key
    using namespace KeyDown;
    int key = eventData[P_KEY].GetInt();

    // get shortcut in list
    genod::nShortcut * sc  = getShortcutsManager()->get(key);

    // not found, nothing to do
    if(sc == 0)
    {
        nPRINT("Shortcut not found");
        return;
    }

    // do action according shortcut key
    switch(sc->_key)
    {
    case KEY_R:
        {
            Vector2 spriteCenter(_texBitmap->GetWidth()/2, _texBitmap->GetHeight()/2);
            _spriteBitmap->SetPosition(spriteCenter.x_ + 16, spriteCenter.y_ + 16 + _text->GetHeight());
            _zoom = 1.0f;
            updateZoomAndPos();
            _texBitmap->SetFilterMode(TextureFilterMode::FILTER_NEAREST);
            _spriteBackground->SetVisible(false);
        }
        break;

    case KEY_A:
        _spriteBackground->SetVisible(!_spriteBackground->IsVisible());
        break;

    case KEY_B:
         _texBitmap->SetFilterMode(TextureFilterMode::FILTER_BILINEAR);
         updateInfoText();
        break;

    case KEY_N:
        _texBitmap->SetFilterMode(TextureFilterMode::FILTER_NEAREST);
        updateInfoText();
        break;
    }
*/
}

//------------------------------------------------------------------------------------------------------
// mouse wheel event
//------------------------------------------------------------------------------------------------------
void eIOpBitmapRenderer::HandleMouseWheel(StringHash eventType, VariantMap& eventData)
{
    using namespace MouseWheel;
    int wheel = eventData[P_WHEEL].GetInt();

    if(wheel > 0.0f)
        _zoom += _zoom;
    else
        _zoom -= _zoom/2;

    updateZoomAndPos();
}


IntVector2 mousePos;

//------------------------------------------------------------------------------------------------------
// mouse press event
//------------------------------------------------------------------------------------------------------
void eIOpBitmapRenderer::HandleMousePressed(StringHash eventType, VariantMap& eventData)
{
    _beginDrag = true;

    Input * input = GetSubsystem<Input>();
    mousePos = input->GetMousePosition();

    ePRINT("%d %d", mousePos.x_, mousePos.y_);

    //int button = eventData[GenodMouseButtonPressed::P_BUTTON].GetInt();
    //
    //if(button == MOUSEB_LEFT)
    //{
    //    nPRINT("LEFT %d", eventData[GenodMouseButtonPressed::P_X].GetInt());
    //    mousePressedPos = IntVector2(eventData[GenodMouseButtonPressed::P_X].GetInt(), eventData[GenodMouseButtonPressed::P_Y].GetInt());
    //}

}

//------------------------------------------------------------------------------------------------------
// mouse release event
//------------------------------------------------------------------------------------------------------
void eIOpBitmapRenderer::HandleMouseReleased(StringHash eventType, VariantMap& eventData)
{
    _beginDrag = false;



    //int button = eventData[GenodMouseButtonReleased::P_BUTTON].GetInt();
    //
    //if(button == MOUSEB_LEFT)
    //{
    //    nPRINT("RELEASED %d \n", eventData[GenodMouseButtonReleased::P_X].GetInt());
    //    mousePressedPos = IntVector2(eventData[GenodMouseButtonReleased::P_X].GetInt(), eventData[GenodMouseButtonPressed::P_Y].GetInt());
    //}
}

//------------------------------------------------------------------------------------------------------
// mouse move event
//------------------------------------------------------------------------------------------------------
void eIOpBitmapRenderer::HandleMouseMove(StringHash eventType, VariantMap& eventData)
{
    if(_beginDrag)
    {

        Input * input = GetSubsystem<Input>();
        mousePos = input->GetMouseMove();
        int x = mousePos.x_ + _spriteBitmap->GetPosition().x_;
        int y = mousePos.y_ + _spriteBitmap->GetPosition().y_;

        //int x = eventData[MouseMove::P_DX].GetInt()/2 + _spriteBitmap->GetPosition().x_ ;
       // int y = eventData[MouseMove::P_DY].GetInt()/2 + _spriteBitmap->GetPosition().y_;

        _spriteBitmap->SetPosition(x, y);
        //updateZoomAndPos();
    }
}



//------------------------------------------------------------------------------------------------------
// operator has been updated
//------------------------------------------------------------------------------------------------------
void eIOpBitmapRenderer::operatorUpdated(eIOperator* op)
{
    if(op->getResultClass() == eOC_BMP)
    {
        eIOpBitmap * nodeBitmap = (eIOpBitmap*)op;
        if(nodeBitmap)
        {
            // get result
            //op_bitmap::sResult * bitmapResult = &nodeBitmap->getResult();
            if(nodeBitmap->getResult().bitmap)// && !node->_updated)
            {
                // get new texture size
                int tw = nodeBitmap->getResult().bitmap->XSize;
                int th = nodeBitmap->getResult().bitmap->YSize;

                // recreate bitmap if needed
                if( !_textureBitmapData ||
                        !_texBitmap ||
                        _texBitmap->GetWidth() != tw ||
                        _texBitmap->GetHeight() != th )
                {
                    createBitmap(tw,th);
                }

                // copy the operator bitmap result to the texture buffer
                nodeBitmap->getResult().bitmap->CopyTo((eU8*)_textureBitmapData);

                // set texture data
                eASSERT(_texBitmap);
                _texBitmap->SetData(0, 0, 0, tw, th, _textureBitmapData);
            }
        }
    }

   /*
    if(node->_sInfo.type == opswz4::EOT_BITMAP)
    {
        op_bitmap * nodeBitmap = (op_bitmap*)node;
        if(nodeBitmap)
        {
            // get result
            //op_bitmap::sResult * bitmapResult = &nodeBitmap->getResult();
            if(nodeBitmap->getResult() && !node->_updated)
            {
                // get new texture size
                int tw = nodeBitmap->getResult()->XSize;
                int th = nodeBitmap->getResult()->YSize;

                // recreate bitmap if needed
                if( !_textureBitmapData ||
                        !_texBitmap ||
                        _texBitmap->GetWidth() != nodeBitmap->getResult()->XSize ||
                        _texBitmap->GetHeight() != nodeBitmap->getResult()->YSize )
                {
                    createBitmap(tw,th);
                }

                // copy the operator bitmap result to the texture buffer
                nodeBitmap->getResult()->CopyTo((uint8*)_textureBitmapData);

                // set texture data
                nASSERT(_texBitmap);
                _texBitmap->SetData(0, 0, 0, tw, th, _textureBitmapData);
            }
        }
    }
*/

}




