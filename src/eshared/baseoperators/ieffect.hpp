#ifndef IEFFECT_HPP
#define IEFFECT_HPP

#include "eshared.hpp"

//!----------------------------------------------------------------------------------------------------------------------
//!
//! Base class for effect operators based on RenderPath commands
//!
//!----------------------------------------------------------------------------------------------------------------------
class eIOpEffect : public eIOperator
{
public:
    struct Result : public eOpResult
    {
        RenderPathCommand rpc;
        SharedPtr<RenderPath> renderPath;
    };

public:
    eIOpEffect()
    {
    }

    virtual const Result & getResult() const
    {
        return m_res;
    }

private:
    virtual void _preExecute()
    {

    }

protected:


protected:
    Result      m_res;
};

#endif // IEFFECT_HPP
