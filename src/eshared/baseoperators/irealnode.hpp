#ifndef IREALNODE_HPP
#define IREALNODE_HPP

//!----------------------------------------------------------------------------------------------------------------------
//!
//! Base class for realNode operators
//!
//!----------------------------------------------------------------------------------------------------------------------
class eIOpRealNode : public eIOperator
{
public:
    struct Result : public eOpResult
    {
        Result() : realNode(nullptr)
        {
        }

        SharedPtr<Node> realNode;
    };
    //eIOperatorRenderer* createOperatorRenderer() {}

public:
    eIOpRealNode()
    {
    }

    virtual const Result & getResult() const
    {
        return m_result;
    }

    void allocateResult()
    {
        m_result.realNode = new Node(eGraphics::m_gcontext);
    }

    void freeResult()
    {
        if(m_result.realNode)
        {
            //m_result.node->RemoveChildren(true,true,false);
            m_result.realNode->RemoveAllComponents();
            m_result.realNode->Remove();
        }
    }

    void reAllocateResult()
    {
        freeResult();
        allocateResult();
    }


private:
    virtual void _preExecute()
    {
       //allocateResultNode();

        /*if(getAboveOpCount() == 0)
            reAllocateResultNode();
        else
        {
            const eIComponentOp *input = (eIComponentOp *)getAboveOp(0);
            eASSERT(input->getResult().node);
            m_result.node = input->getResult().node;
            m_result.node->AddRef();
        }*/
    }

protected:
    Result      m_result;
};


#endif // IREALNODE_HPP
