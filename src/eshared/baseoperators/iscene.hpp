#ifndef ISCENE_HPP
#define ISCENE_HPP

#include "eshared.hpp"

//!----------------------------------------------------------------------------------------------------------------------
//!
//! operator between model and effect operators
//!
//!----------------------------------------------------------------------------------------------------------------------
class eIOpScene : public eIOperator
{
public:
    struct Result : public eOpResult
    {
        //@@eScene scene;
        SharedPtr<Scene> scene;

    };
    eIOperatorRenderer* createOperatorRenderer();

public:
    eIOpScene()
    {
    }

    virtual const Result & getResult() const
    {
        return m_result;
    }

protected:
    //@@eScene & m_scene;
    //SharedPtr<Scene> & m_scene;
    Result   m_result;
};



//!----------------------------------------------------------------------------------------------------------------------
//!
//! A renderer for scene operators.
//!
//!----------------------------------------------------------------------------------------------------------------------
class eIOpSceneRenderer : public eIOperatorRenderer
{
public:
    eIOpSceneRenderer()
    {
    }

    ~eIOpSceneRenderer()
    {
    }

    void init()
    {
    }

    void operatorUpdated(eIOperator* op)
    {
        Scene* scene = ((eIOpScene*)op)->getResult().scene;


        getViewport()->SetScene(scene);

        Node* cameraNode = scene->CreateChild("Camera");
        Camera* camera = cameraNode->CreateComponent<Camera>();

        cameraNode->SetPosition(Vector3(0,2,-10));

        getViewport()->SetScene(scene);
        getViewport()->SetCamera(camera);
    }

};



#endif // ISCENE_HPP
