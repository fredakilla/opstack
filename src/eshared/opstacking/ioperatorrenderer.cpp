

#include "urhoobjects/uCameraController.hpp"


//*********************************************************************************************************************
//
// nIOperatorRenderer
//
//*********************************************************************************************************************

eIOperatorRenderer::eIOperatorRenderer() :
    Object(eGraphics::m_gcontext)
{
    _ready = false;
    _rootUI = nullptr;
    _viewport = new Viewport(context_);    

    // create scene
    m_scene = new Scene(context_);
    m_scene->CreateComponent<Octree>();
    m_scene->AddComponent(eGraphics::m_gdebuRenderer, 0, LOCAL);
    m_scene->SetUpdateEnabled(false);

    // create default camera
    m_nodeCamera = m_scene->CreateChild("DefaultCamera");
    m_nodeCamera->SetPosition(Vector3(0,2,-10));
    Camera* camera = m_nodeCamera->CreateComponent<Camera>();

    // create default controller component
    m_nodeCamera->CreateComponent<uCameraController>();

    // attach default light to camera
    Node* cameraLightNode = m_nodeCamera->CreateChild("CameraLight");
    cameraLightNode->SetDirection(m_nodeCamera->GetDirection());
    Light* light = cameraLightNode->CreateComponent<Light>();
    light->SetLightType(LIGHT_DIRECTIONAL);
    light->SetColor(Color::WHITE);
    light->SetCastShadows(false);


    // create the root scene node (that will hold all childs nodes)
    m_nodeRoot = m_scene->CreateChild("RootNode");
    m_nodeRoot->AddRef();








    _viewport->SetScene(m_scene);
    _viewport->SetCamera(camera);

    const char renderpath[] =
            "<renderpath>"
            "    <command type=\"clear\" color=\"0.1 0.1 0.2 1.0\" depth=\"1.0\" stencil=\"0\" />"
            "    <command type=\"scenepass\" pass=\"base\" vertexlights=\"false\" metadata=\"base\" />"
            "    <command type=\"forwardlights\" pass=\"light\" />"
            "    <command type=\"scenepass\" pass=\"postopaque\" />"
            "    <command type=\"scenepass\" pass=\"refract\">"
            "        <texture unit=\"environment\" name=\"viewport\" />"
            "    </command>"
            "    <command type=\"scenepass\" pass=\"alpha\" vertexlights=\"true\" sort=\"backtofront\" metadata=\"base\" />"
            "    <command type=\"scenepass\" pass=\"postalpha\" sort=\"backtofront\" />"
            "</renderpath>";


    const String& patchString = renderpath;
    if (!patchString.Empty())
    {
        SharedPtr<XMLFile> patchFile(new XMLFile(context_));
        if (patchFile->FromString(patchString))
        {
            //Renderer* renderer = GetSubsystem<Renderer>();
            //Viewport* viewport = renderer->GetViewport(0);
            _viewport->SetRenderPath(patchFile);
        }
    }

    //_viewport->GetRenderPath()->AddCommand();

    //ResourceCache* cache = GetSubsystem<ResourceCache>();
    // _viewport->GetRenderPath()->Append(cache->GetResource<XMLFile>("PostProcess/GammaCorrection.xml"));



    gridColor = Color(0.5f, 0.5f, 0.5f);
    gridSubdivisionColor = Color(0.2f, 0.2f, 0.2f);
    gridXColor = Color(1.0f, 0.1f, 0.1f);
    gridYColor = Color(0.1f, 1.0f, 0.1f);
    gridZColor = Color(0.1f, 0.5f, 1.0f);
    showGrid_ = true;
    grid2DMode_ = false;
    CreateGrid();


}

eIOperatorRenderer::~eIOperatorRenderer()
{
   // eSAFE_RELEASE_REF(_rootUI);
   // eSAFE_RELEASE_REF(_viewport);

    m_nodeRoot->ReleaseRef();
}

UIElement* eIOperatorRenderer::createRootUI()
{
    _rootUI = new UIElement(context_);
    _rootUI->SetName("nIOperatorRenderer");
    return _rootUI;
}


UIElement* eIOperatorRenderer::getRootUI()
{
    return _rootUI;
}



void eIOperatorRenderer::HideGrid()
{
    if (grid_ != NULL)
        grid_->SetEnabled(false);
}

void eIOperatorRenderer::ShowGrid()
{
    if (grid_ != NULL)
    {
        grid_->SetEnabled(true);

       // EditorData* editorData_ = GetSubsystem<EditorData>();
        if (getViewport()->GetScene()->GetComponent<Octree>() != NULL)
            getViewport()->GetScene()->GetComponent<Octree>()->AddManualDrawable(grid_);
    }
}

void eIOperatorRenderer::CreateGrid()
{
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    if (!gridNode_)
    {
        gridNode_ = new Node(context_);
        grid_ = gridNode_->CreateComponent<CustomGeometry>();
        grid_->SetNumGeometries(1);
        grid_->SetMaterial(cache->GetResource<Material>("Materials/VColUnlit.xml"));
        grid_->SetViewMask(0x80000000); // Editor raycasts use viewmask 0x7fffffff
        grid_->SetOccludee(false);
    }
    UpdateGrid();
}

void eIOperatorRenderer::UpdateGrid(bool updateGridGeometry /*= true*/)
{
    showGrid_ ? ShowGrid() : HideGrid();
    gridNode_->SetScale(Vector3(8.0f, 8.0f, 8.0f));

    if (!updateGridGeometry)
        return;

    unsigned int size = (unsigned int)(floor(8.0f / 2.0f) * 2.0f);
    float halfSizeScaled = size / 2.0f;
    float scale = 1.0f;
    unsigned int subdivisionSize = (unsigned int)(pow(2.0f, 3.0f));

    if (subdivisionSize > 0)
    {
        size *= subdivisionSize;
        scale /= subdivisionSize;
    }

    unsigned int halfSize = size / 2;

    grid_->BeginGeometry(0, LINE_LIST);
    float lineOffset = -halfSizeScaled;
    for (unsigned int i = 0; i <= size; ++i)
    {
        bool lineCenter = i == halfSize;
        bool lineSubdiv = !Equals(float(i% subdivisionSize), 0.0f);

        if (!grid2DMode_)
        {
            grid_->DefineVertex(Vector3(lineOffset, 0.0, halfSizeScaled));
            grid_->DefineColor(lineCenter ? gridZColor : (lineSubdiv ? gridSubdivisionColor : gridColor));
            grid_->DefineVertex(Vector3(lineOffset, 0.0, -halfSizeScaled));
            grid_->DefineColor(lineCenter ? gridZColor : (lineSubdiv ? gridSubdivisionColor : gridColor));

            grid_->DefineVertex(Vector3(-halfSizeScaled, 0.0, lineOffset));
            grid_->DefineColor(lineCenter ? gridXColor : (lineSubdiv ? gridSubdivisionColor : gridColor));
            grid_->DefineVertex(Vector3(halfSizeScaled, 0.0, lineOffset));
            grid_->DefineColor(lineCenter ? gridXColor : (lineSubdiv ? gridSubdivisionColor : gridColor));
        }
        else
        {
            grid_->DefineVertex(Vector3(lineOffset, halfSizeScaled, 0.0));
            grid_->DefineColor(lineCenter ? gridYColor : (lineSubdiv ? gridSubdivisionColor : gridColor));
            grid_->DefineVertex(Vector3(lineOffset, -halfSizeScaled, 0.0));
            grid_->DefineColor(lineCenter ? gridYColor : (lineSubdiv ? gridSubdivisionColor : gridColor));

            grid_->DefineVertex(Vector3(-halfSizeScaled, lineOffset, 0.0));
            grid_->DefineColor(lineCenter ? gridXColor : (lineSubdiv ? gridSubdivisionColor : gridColor));
            grid_->DefineVertex(Vector3(halfSizeScaled, lineOffset, 0.0));
            grid_->DefineColor(lineCenter ? gridXColor : (lineSubdiv ? gridSubdivisionColor : gridColor));
        }

        lineOffset += scale;
    }
    grid_->Commit();
}
