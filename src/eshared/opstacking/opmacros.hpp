/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   This file is part of
 *       ______        _                             __ __
 *      / ____/____   (_)____ _ ____ ___   ____ _   / // /
 *     / __/  / __ \ / // __ `// __ `__ \ / __ `/  / // /_
 *    / /___ / / / // // /_/ // / / / / // /_/ /  /__  __/
 *   /_____//_/ /_//_/ \__, //_/ /_/ /_/ \__,_/     /_/.   
 *                    /____/                              
 *
 *   Copyright � 2003-2012 Brain Control, all rights reserved.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef OP_MACROS_HPP
#define OP_MACROS_HPP

#ifdef eEDITOR
    #define eDEF_OPERATOR_SOURCECODE_INTERNAL(CLASSNAME)                    \
        class CLASSNAME {                                                   \
		public:																\
                CLASSNAME() {                                               \
                    eDemoData::m_operatorSourceFiles.append(__FILE__);      \
				}															\
			};																\
			CLASSNAME temp;

	#define GLUE_OPSOURCE_NAME(A,B) eDEF_OPERATOR_SOURCECODE_INTERNAL(A##B)
	#define GLUE_OPSOURCE_NAME0(A,B) GLUE_OPSOURCE_NAME(A,B)
	#define eDEF_OPERATOR_SOURCECODE(A)	GLUE_OPSOURCE_NAME0(OPSOURCEFILE,A)

#else
	#define eDEF_OPERATOR_SOURCECODE(A)
#endif

// create an operator type as a compile-time
// hash based on the category and name
#define eOP_TYPE(category, name) (eHashStr(category)^eHashStr(name))

// use these macros if many inputs have to
// be declared (mostly used for merge operators)
#define eOP_4INPUTS(opClass)  opClass, opClass, opClass, opClass
#define eOP_8INPUTS(opClass)  eOP_4INPUTS(opClass), eOP_4INPUTS(opClass)
#define eOP_16INPUTS(opClass) eOP_8INPUTS(opClass), eOP_8INPUTS(opClass)
#define eOP_32INPUTS(opClass) eOP_16INPUTS(opClass), eOP_16INPUTS(opClass)

#ifdef eEDITOR
#define eOP_REGISTER_EFFECT_VAR(opclass, var)																	\
	eEngine::_addEffectVar(#opclass, var)
#else
#define eOP_REGISTER_EFFECT_VAR(opclass, var)                                                              
#endif


#define eOP_INPUTS(...)																					\
	{ __VA_ARGS__ }

#ifdef eEDITOR
#define eOP_DEF(classT, baseClassT, name, category, color, shortcut, output, minAbove, maxAbove, ...)   \
    static eOpMetaInfos classT##MetaInfos =                                                             \
    {                                                                                                   \
        #classT, name, category, color, shortcut, minAbove, maxAbove, __VA_ARGS__,                      \
        output, eOP_TYPE(category, name), nullptr                                                       \
    };                                                                                                  \
                                                                                                        \
    class classT : public baseClassT                                                                    \
    {                                                                                                   \
    public:                                                                                             \
        classT()                                                                                        \
        {                                                                                               \
            m_metaInfos = &classT##MetaInfos;                                                           \
            m_width = eClamp<eU32>(1, (eU32)maxAbove, 2)*4;                                             \
			_setupParams();																				\
            _initialize();                                                                              \
        }                                                                                               \
    private:
#else

#define eOP_DEF(classT, baseClassT, name, category, color, shortcut, output, minAbove, maxAbove, inputs)   \
    static eOpMetaInfos classT##MetaInfos =                                                             \
    {                                                                                                   \
		output, ePLAYER_OPTYPE_##classT																	\
    };                                                                                                  \
                                                                                                        \
    class classT : public baseClassT                                                                    \
    {                                                                                                   \
    public:                                                                                             \
        classT()                                                                                        \
        {                                                                                               \
            m_metaInfos = &classT##MetaInfos;                                                           \
			_setupParams();																				\
            _initialize();                                                                              \
        }
#endif

#ifdef eEDITOR
#define eOP_END(classT)                                                                                 \
        virtual ~classT()                                                                               \
        {                                                                                               \
            _deinitialize();                                                                            \
        }                                                                                               \
                                                                                                        \
        static eIOperator * classT##CreateInstance()                                                    \
        {                                                                                               \
            return new classT;                                                                          \
        }                                                                                               \
                                                                                                        \
        static const struct RegisterExecFunc                                                            \
        {                                                                                               \
            RegisterExecFunc()                                                                          \
            {                                                                                           \
                classT##MetaInfos.createOp = classT##CreateInstance;                                    \
                getAllMetaInfos().append(&classT##MetaInfos);                                           \
            }                                                                                           \
        }                                                                                               \
        regExecFunc;                                                                                    \
    };                                                                                                  \
                                                                                                        \
    const classT::RegisterExecFunc classT::regExecFunc;											        \

#else
#define eOP_END(classT)                                                                                 \
        virtual ~classT()                                                                               \
        {                                                                                               \
            _deinitialize();                                                                            \
        }                                                                                               \
                                                                                                        \
    };                                                                                                  \

#endif

#define eOP_EXEC                                                                                        \
   public:                                                                                              \
       void _callExecute2

#define eOP_INIT                                                                                        \
   private:                                                                                             \
       eParameter* _dummyParaLabel;                                                                     \
       void _initialize

#define eOP_DEINIT                                                                                      \
    private:                                                                                            \
        void _deinitialize

#ifdef eEDITOR

#define eOP_DEBUGDRAW                                                                                   \
    public:                                                                                             \
        virtual void drawDebug

#define eOP_INTERACT                                                                                    \
    public:                                                                                             \
        virtual eBool doEditorInteraction
#else
#define eOP_INTERACT																					\
    public:                                                                                             \
        eBool __UNUSED_INTERACTION_FUNCTION
#endif

#define eOP_VAR(var)                                                                                    \
    private:                                                                                            \
        var

#define eOP_DEF_BMP(classT, name, shortcut, minAbove, maxAbove, inputs)                                \
    eOP_DEF(classT, eIOpBitmap, name, "Bitmap", eColor(170, 85, 136),                                  \
            shortcut, eOC_BMP, minAbove, maxAbove, inputs)

#define eOP_DEF_MESH(classT, name, shortcut, minAbove, maxAbove, inputs)                                \
    eOP_DEF(classT, eIOpMesh, name, "Mesh", eColor(116, 160, 173),                                      \
            shortcut, eOC_MESH, minAbove, maxAbove, inputs)

#define eOP_DEF_COMPONENT(classT, name, shortcut, minAbove, maxAbove, inputs)                           \
    eOP_DEF(classT, eIOpComponent, name, "Component", eColor(150, 215, 128),                            \
            shortcut, eOC_COMPONENT, minAbove, maxAbove, inputs)

#define eOP_DEF_NODE(classT, name, shortcut, minAbove, maxAbove, inputs)                                \
    eOP_DEF(classT, eIOpNode, name, "Node", eColor(215, 215, 0),                                        \
            shortcut, eOC_NODE, minAbove, maxAbove, inputs)

#define eOP_DEF_SCENE(classT, name, shortcut, minAbove, maxAbove, inputs)                               \
    eOP_DEF(classT, eIOpScene, name, "Scene", eColor(215, 140, 50),                                      \
            shortcut, eOC_SCENE, minAbove, maxAbove, inputs)

#define eOP_DEF_FX(classT, name, shortcut, minAbove, maxAbove, inputs)                                  \
    eOP_DEF(classT, eIOpEffect, name, "Effect", eColor(128, 0, 128),                                    \
            shortcut, eOC_FX, minAbove, maxAbove, inputs)

#define eOP_DEF_VIEWPORT(classT, name, shortcut, minAbove, maxAbove, inputs)                            \
    eOP_DEF(classT, eIOpViewport, name, "Viewport", eColor(50, 140, 50),                                \
            shortcut, eOC_POV, minAbove, maxAbove, inputs)

#define eOP_DEF_SEQ(classT, name, shortcut, minAbove, maxAbove, inputs)                                 \
    eOP_DEF(classT, eIOpSequencer, name, "Sequencer", eColor(255, 128, 0),                              \
            shortcut, eOC_SEQ, minAbove, maxAbove, inputs)

#define eOP_DEF_REALNODE(classT, name, shortcut, minAbove, maxAbove, inputs)                            \
    eOP_DEF(classT, eIOpRealNode, name, "RealNode", eColor(145, 215, 0),                                \
            shortcut, eOC_REALNODE, minAbove, maxAbove, inputs)


#define eOP_PAR_RGB(VARNAME, name, r, g, b)                                                             \
    eOP_PARAM2_ADD_RGB(VARNAME, name, r, g, b)
#define eOP_PAR_RGBA( VARNAME, name, r, g, b, a)														\
    eOP_PARAM2_ADD_RGBA(VARNAME, name, r, g, b, a)
#define eOP_PAR_PATH(VARNAME, name)																		\
    eOP_PARAM2_ADD_PATH(VARNAME, name)
#define eOP_PAR_BOOL(VARNAME, name, state)																\
    eOP_PARAM2_ADD_BOOL(VARNAME, name, state)
#define eOP_PAR_FILE(VARNAME, name, file)																\
    eOP_PARAM2_ADD_FILE(VARNAME, name, file)
#define eOP_PAR_STRING(VARNAME, name, str)																\
    eOP_PARAM2_ADD_STRING(VARNAME, name, str)
#define eOP_PAR_TEXT(VARNAME, name, str)																\
    eOP_PARAM2_ADD_TEXT(VARNAME, name, str)
#define eOP_PAR_FLAGS(VARNAME, name, descr, index)														\
    eOP_PARAM2_ADD_FLAGS(VARNAME, name, descr, index)
#define eOP_PAR_ENUM(VARNAME, name, descr, index)														\
    eOP_PARAM2_ADD_ENUM(VARNAME, name, descr, index)
#define eOP_PAR_INT(VARNAME, name, min, max, x)															\
    eOP_PARAM2_ADD_INT(VARNAME, name, min, max, x)
#define eOP_PAR_IXY(VARNAME, name, min, max, x, y)														\
    eOP_PARAM2_ADD_IXY(VARNAME, name, min, max, x, y)
#define eOP_PAR_IXYZ(VARNAME, name, min, max, x, y, z)													\
    eOP_PARAM2_ADD_IXYZ(VARNAME, name, min, max, x, y,z)
#define eOP_PAR_IXYXY(VARNAME, name, min, max, x, y, z, w)												\
    eOP_PARAM2_ADD_IXYXY(VARNAME, name, min, max, x, y, z, w)
#define eOP_PAR_FLOAT(VARNAME, name, min, max, x)														\
    eOP_PARAM2_ADD_FLOAT(VARNAME, name, min, max, x)
#define eOP_PAR_FXY(VARNAME, name, min, max, x, y)														\
    eOP_PARAM2_ADD_FXY(VARNAME, name, min, max, x, y)
#define eOP_PAR_FXYZ(VARNAME, name, min, max, x, y, z)													\
    eOP_PARAM2_ADD_FXYZ(VARNAME, name, min, max, x, y, z)
#define eOP_PAR_FXYZW(VARNAME, name, min, max, x, y, z, w)												\
    eOP_PARAM2_ADD_FXYZW(VARNAME, name, min, max, x, y, z, w)
#define eOP_PAR_LINK_MATERIAL(VARNAME, name, allowedLinks, requiredLink)								\
    eOP_PARAM2_ADD_LINK(const eIMaterialOp*, VARNAME, name, allowedLinks, requiredLink)
#define eOP_PAR_LINK_PATH(VARNAME, name, allowedLinks, requiredLink)									\
    eOP_PARAM2_ADD_LINK(const eIPathOp*, VARNAME, name, allowedLinks, requiredLink)
#define eOP_PAR_LINK_MESH(VARNAME, name, allowedLinks, requiredLink)									\
    eOP_PARAM2_ADD_LINK(const eIMeshOp*, VARNAME, name, allowedLinks, requiredLink)
#define eOP_PAR_LINK_MODEL(VARNAME, name, allowedLinks, requiredLink)									\
    eOP_PARAM2_ADD_LINK(const eIModelOp*, VARNAME, name, allowedLinks, requiredLink)
#define eOP_PAR_LINK_BMP(VARNAME, name, allowedLinks, requiredLink)										\
    eOP_PARAM2_ADD_LINK(const eIBitmapOp*, VARNAME, name, allowedLinks, requiredLink)
#define eOP_PAR_LINK_POV(VARNAME, name, allowedLinks, requiredLink)										\
    eOP_PARAM2_ADD_LINK(const eIPovOp*, VARNAME, name, allowedLinks, requiredLink)
#define eOP_PAR_LINK_R2T(VARNAME, name, allowedLinks, requiredLink)										\
    eOP_PARAM2_ADD_LINK(const eIRenderToTextureOp*, VARNAME, name, allowedLinks, requiredLink)
#define eOP_PAR_LINK_DEMO(VARNAME, name, allowedLinks, requiredLink)									\
    eOP_PARAM2_ADD_LINK(const eDemoOp*, VARNAME, name, allowedLinks, requiredLink)
#define eOP_PAR_LINK(VARNAME, name, allowedLinks, requiredLink)											\
    eOP_PARAM2_ADD_LINK(const eIOperator*, VARNAME, name, allowedLinks, requiredLink)
#ifdef ePLAYER
#define eOP_PAR_LABEL(VARNAME, name, caption)															\
    eOP_PAR_GLUE1(__VA_ARGS__)
#else
#define eOP_PAR_LABEL(VARNAME, name, caption)															\
    eOP_PARAM2_ADD_LABEL(VARNAME, name, caption)
#define eOP_PAR_LABEL2(caption)                                                                         \
    eOP_PARAM2_ADD_LABEL(_dummyParaLabel, "_dummyParaLabel", caption)
#endif





#define eOP_PARAM2_ADD_INT(varname, name, min, max, value)                                                        \
    eOP_PARAM2_ADD(ePT_INT, const eU32, const eU32&, varname, name, integer, min, max, value)

#define eOP_PARAM2_ADD_FLOAT(varname, name, min, max, value)                                                      \
    eOP_PARAM2_ADD(ePT_FLOAT, const eF32, const eF32&, varname, name, flt, min, max, value)

#define eOP_PARAM2_ADD_FXY(varname, name, min, max, x, y)                                                         \
    eOP_PARAM2_ADD(ePT_FXY, const eVector2&, const eVector2&, varname, name, fxy, min, max, eVector2(x, y))

#define eOP_PARAM2_ADD_FXYZ(varname, name, min, max, x, y, z)                                                     \
    eOP_PARAM2_ADD(ePT_FXYZ, const eVector3&, const eVector3&, varname, name, fxyz, min, max, eVector3(x, y, z))

#define eOP_PARAM2_ADD_FXYZW(varname, name, min, max, x, y, z, w)                                                 \
    eOP_PARAM2_ADD(ePT_FXYZW, const eVector4&, const eVector4&, varname, name, fxyzw, min, max, eVector4(x, y, z, w))

#define eOP_PARAM2_ADD_RGB(varname, name, r, g, b)                                                                \
    eOP_PARAM2_ADD(ePT_RGB, const eColor&, const eColor&, varname, name, color, 0, 255, eColor(r, g, b, 255))

#define eOP_PARAM2_ADD_RGBA(varname, name, r, g, b, a)                                                            \
    eOP_PARAM2_ADD(ePT_RGBA, const eColor&, const eColor&, varname, name, color, 0, 255, eColor(r, g, b, a))

#define eOP_PARAM2_ADD_IXY(varname, name, min, max, x, y)                                                         \
    eOP_PARAM2_ADD(ePT_IXY, const ePoint&, const ePoint&, varname, name, ixy, min, max, ePoint(x, y))

#define eOP_PARAM2_ADD_IXYZ(varname, name, min, max, x, y, z)                                                     \
    eOP_PARAM2_ADD(ePT_IXYZ, const eIXYZ&, const eIXYZ&, varname, name, ixyxy, min, max, eRect(x, y, z, 0))

#define eOP_PARAM2_ADD_IXYXY(varname, name, min, max, x0, y0, x1, y1)                                             \
    eOP_PARAM2_ADD(ePT_IXYXY, const eIXYXY&, const eIXYXY&, varname, name, ixyxy, min, max, eRect(x0, y0, x1, y1))

#define eOP_PARAM2_ADD_BOOL(varname, name, state)                                                                 \
    eOP_PARAM2_ADD(ePT_BOOL, const eBool, const eBool&, varname, name, boolean, 0, 1, state)

#define eOP_PARAM2_ADD_STRING(varname, name, str)                                                                 \
    eOP_PARAM2_ADD(ePT_STR, const eChar*, const eChar*, varname, name, string, 0, 0, str)

#define eOP_PARAM2_ADD_TEXT(varname, name, text)                                                                  \
    eOP_PARAM2_ADD(ePT_TEXT, const eChar*, const eChar*, varname, name, string, 0, 0, text)

#define eOP_PARAM2_ADD_FILE(varname, name, file)                                                                  \
    eOP_PARAM2_ADD(ePT_FILE, const eChar*, const eChar*, varname, name, string, 0, 0, file)

#define eOP_PARAM2_ADD_PATH(varname, name)                                                                        \
    eOP_PARAM2_ADD(ePT_PATH, const ePath4&, const ePath4&, varname, name, path, 0, 0, ePath4())






// nulls PODs but not string and path as they have ctors
#define eOP_PARAM2_ADD(type, allocator, caster, varname, name, var, min, max, value)                    \
{                                                                                                       \
    eParamValue defVal;                                                                                 \
    eMemSet(&defVal, 0, 4*4);                                                                           \
    defVal.var = value;                                                                                 \
    varname = new eParameter(type, #allocator, #caster, #varname, name, min, max, defVal, this);        \
    m_params.append(varname); \
}

#define eOP_PARAM2_ADD_LINK(caster, varname, name, allowedLinks, requiredLink)                          \
    eOP_PARAM2_ADD(ePT_LINK, caster, caster, varname, name, linkedOpId, 0, 0, eNOID);                   \
    m_params.last()->setAllowedLinks(allowedLinks);                                                     \
    m_params.last()->setRequiredLink(requiredLink);

#define eOP_PARAM2_ADD_ENUM(varname, name, descr, index)                                                \
    eOP_PARAM2_ADD(ePT_ENUM, const eInt, const eInt&, varname, name, enumSel, 0, 255, index);           \
    m_params.last()->setDescription(descr);

#define eOP_PARAM2_ADD_FLAGS(varname, name, descr, index)                                               \
    eOP_PARAM2_ADD(ePT_FLAGS, const eInt, const eInt&, varname, name, flags, 0, 255, index);            \
    m_params.last()->setDescription(descr);

#define eOP_PARAM2_ADD_LABEL(varname, name, caption)                                                    \
    eOP_PARAM2_ADD(ePT_LABEL, NOTYPE, NOTYPE, varname, name, string, 0, 0, caption)


#endif // OP_MACROS_HPP
