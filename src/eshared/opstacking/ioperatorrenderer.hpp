#ifndef IOPERATORRENDERER_HPP
#define IOPERATORRENDERER_HPP

#include "eshared.hpp"

//--------------------------------------------------------------------------------------------------
// A renderer interface for operators.
// Used to render operator result.
// Each operator base type has its own renderer.
//--------------------------------------------------------------------------------------------------
class eIOperatorRenderer : public Object
{
    URHO3D_OBJECT(eIOperatorRenderer, Object)

public:
    eIOperatorRenderer();
    virtual ~eIOperatorRenderer();

    virtual void init() = 0;
    virtual void operatorUpdated(eIOperator* op) = 0;

    /// Subscribe to all Urho3D events. Defines here which events to subscribe. Automatically called by system.
    virtual void subscribeToEvents() {}

    /// Unsubscribe to all Urho3D events. Automatically called by system.
    virtual void unSubscribeToEvents() { UnsubscribeFromAllEvents(); }

    /// View was resized.
    virtual void resize(int width, int height) { _viewport->SetRect(IntRect(0,0, width, height)); }

    /// Is engine ready to render.
    bool isReady() { return _ready; }

    /// Set engine ready to render.
    void setReady(bool ready) { _ready = ready; }

    /// Get shortcuts manager.
    //nShortcutManager * getShortcutsManager() { return &_shortcuts; }

    /// Get viewport pointer.
    Viewport*   getViewport() { return _viewport; }

    /// Create a root UI element.
    UIElement*  createRootUI();

    /// Get root UI element pointer.
    UIElement*  getRootUI();






    Node* getDefaultCamera() { return m_nodeCamera; }
    Scene* getDefaultScene() { return m_scene; }
    Node* getRootNode() { return m_nodeRoot; }

    void clearScene()
    {
        //m_scene->Clear();
        //m_scene->CreateComponent<Octree>();

        //m_scene->RemoveAllChildren();
        //m_scene->AddChild(m_cameraNode);

        //m_nodeRoot->RemoveAllChildren();

        //m_nodeRoot->RemoveChildren(false, false, false);


        m_nodeRoot->RemoveChildren(true, true, false);
    }



    // grid
    void HideGrid();
    void ShowGrid();
    void CreateGrid();
    void UpdateGrid(bool updateGridGeometry = true);

    SharedPtr<Node>				gridNode_;
    SharedPtr<CustomGeometry>	grid_;
    bool	showGrid_;
    bool	grid2DMode_;
    Color gridColor;
    Color gridSubdivisionColor;
    Color gridXColor;
    Color gridYColor;
    Color gridZColor;









private:
    bool                                   _ready;         // engine was successfully initialized and is ready to render
    SharedPtr<Viewport>     _viewport;      // viewport to pass to final renderer
    SharedPtr<UIElement>    _rootUI;        // root UI element to pass to final renderer
    //nShortcutManager                  _shortcuts;     // associated shortcuts

protected:
    SharedPtr<Scene>        m_scene;
    SharedPtr<Node>         m_nodeCamera;
    SharedPtr<Node>         m_nodeRoot;


};

#endif // IOPERATORRENDERER_HPP
