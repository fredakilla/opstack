/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   This file is part of
 *       ______        _                             __ __
 *      / ____/____   (_)____ _ ____ ___   ____ _   / // /
 *     / __/  / __ \ / // __ `// __ `__ \ / __ `/  / // /_
 *    / /___ / / / // // /_/ // / / / / // /_/ /  /__  __/
 *   /_____//_/ /_//_/ \__, //_/ /_/ /_/ \__,_/     /_/.   
 *                    /____/                              
 *
 *   Copyright � 2003-2012 Brain Control, all rights reserved.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef GRAPHICS_HPP
#define GRAPHICS_HPP

#include <Urho3D/Urho3DAll.h>



class eGraphics : public Object
{
    URHO3D_OBJECT(eGraphics, Object)

public:
    eGraphics();
    ~eGraphics();

// ---- new urho -----------------------------------------------------------------
// ------------------------------------------------------------------------------

private:
    Engine*           m_engine;            // Urho3D engine

public:
    static void                 createContext();
    static void                 freeContext();
    static Context*             m_gcontext;             // Urho3D global context
    static DebugRenderer*       m_gdebuRenderer;        // Urho3D global shared DebugRenderer

    static eF32 m_time;
    static void setTime(eF32 time) { m_time = time; }
    static eF32 getTime() { return m_time; }

// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------

    void                        initialize();
    void                        shutdown();
    void                        openWindow(eU32 width, eU32 height, eInt windowFlags=0, ePtr hwnd=nullptr);
    void                        beginFrame();
    void                        endFrame();

    eU32                        getResolutionCount() const;
    const eSize &               getResolution(eU32 index) const;
#ifdef eEDITOR
    const eRenderStats &        getRenderStats() const;
    const eEngineStats &        getEngineStats() const;
#endif
    eBool                       getFullScreen() const;
    eU32                        getWndWidth() const;
    eU32                        getWndHeight() const;
    eSize                       getWndSize() const;

private:

#ifdef eEDITOR
    eRenderStats                m_renderStats;
    eEngineStats                m_engineStats;
#endif

    ePtr                        m_hwnd;
    eBool                       m_ownWindow;
    eBool                       m_fullScreen;
    eBool                       m_vsync;
    eU32                        m_wndWidth;
    eU32                        m_wndHeight;
};

#endif // GRAPHICS_HPP
