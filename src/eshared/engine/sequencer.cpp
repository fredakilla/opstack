/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   This file is part of
 *       ______        _                             __ __
 *      / ____/____   (_)____ _ ____ ___   ____ _   / // /
 *     / __/  / __ \ / // __ `// __ `__ \ / __ `/  / // /_
 *    / /___ / / / // // /_/ // / / / / // /_/ /  /__  __/
 *   /_____//_/ /_//_/ \__, //_/ /_/ /_/ \__,_/     /_/.   
 *                    /____/                              
 *
 *   Copyright � 2003-2012 Brain Control, all rights reserved.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "../eshared.hpp"
#include "device.hpp"

eSequencer::eSequencer() :
    Object(eGraphics::m_gcontext),
    m_endTime(0.0f),
    m_aspectRatio(4.0f/3.0f)
{
    m_viewport = new Viewport(context_);
    m_viewport->SetRect(IntRect(0,0, 800, 600));
}

eSequencer::~eSequencer()
{
}

eSeqResult eSequencer::run(eF32 time) const
{
    eASSERT(time >= 0.0f);

    // are there still entries to play?
    // used for indicating player to exit
    if (time > m_endTime)
        return eSEQ_FINISHED;




    Renderer* renderer = GetSubsystem<Renderer>();
    RenderPath* rp = renderer->GetViewport(0)->GetRenderPath();
    rp->RemoveCommands("ClipSequence");
    //rp->RemoveCommands("Result");




    for (eU32 i=0; i<MAX_TRACKS; i++)
    {
        const eSeqEntry *entry = _getEntryForTrack(time, i);
        if (entry)
        {
            _renderEntry(*entry, time);

        }
    }




   /* RenderPathCommand rpc;
    rpc.type_ = CMD_QUAD;
    rpc.tag_ = String("Result");
    rpc.vertexShaderName_ = "CopyFramebuffer";
    rpc.pixelShaderName_ = "CopyFramebuffer";
    rpc.blendMode_ = BlendMode::BLEND_REPLACE;
    rpc.SetOutput(0, "viewport" );
    rpc.SetTextureName(TU_DIFFUSE, "mixed");
    rp->AddCommand(rpc);*/

    return eSEQ_PLAYING;
}

void eSequencer::addEntry(const eSeqEntry &entry, eU32 track)
{
    eASSERT(track < MAX_TRACKS);
    m_entries[track].append(entry);
    m_endTime = eMax(m_endTime, entry.startTime+entry.duration);
}

void eSequencer::merge(const eSequencer &seq)
{
    for (eU32 i=0; i<MAX_TRACKS; i++)
        for (eU32 j=0; j<seq.m_entries[i].size(); j++)
            addEntry(seq.m_entries[i][j], i);
}

void eSequencer::clear()
{
    for (eU32 i=0; i<MAX_TRACKS; i++)
        m_entries[i].clear();

    m_endTime = 0.0f;
}

void eSequencer::setAspectRatio(eF32 aspectRatio)
{
    eASSERT(aspectRatio > 0.0f);
    m_aspectRatio = aspectRatio;
}

const eArray<eSeqEntry> & eSequencer::getEntriesOfTrack(eU32 track) const
{
    eASSERT(track < MAX_TRACKS);
    return m_entries[track];
}

eF32 eSequencer::getAspectRatio() const
{
    return m_aspectRatio;
}

eF32 eSequencer::getEndTime() const
{
    return m_endTime;
}

const eSeqEntry * eSequencer::_getEntryForTrack(eF32 time, eU32 track) const
{
    eASSERT(track < MAX_TRACKS);
    eASSERT(time >= 0.0f);

    for (eU32 i=0; i<m_entries[track].size(); i++)
    {
        const eSeqEntry *entry = &m_entries[track][i];
        if (entry->startTime <= time && entry->startTime+entry->duration > time)
            return entry;
    }

    return nullptr;
}

void eSequencer::_renderEntry(const eSeqEntry &entry, eF32 time) const
{
    if (entry.type == eSET_SCENE)
    {
        Renderer* renderer = GetSubsystem<Renderer>();


        /*Node* cameraNode = entry.scene.urhoScene->CreateChild("Camera");
        Camera* camera = cameraNode->CreateComponent<Camera>();

        cameraNode->SetPosition(Vector3(0,2,-10));

        m_viewport->SetScene(entry.scene.urhoScene);
        m_viewport->SetCamera(camera);*/


        //renderer->SetViewport(0, entry.scene.urhoViewport);

    }



    if (entry.type == eSET_URHOTEST)
    {
        Renderer* renderer = GetSubsystem<Renderer>();
        RenderPath* rp = renderer->GetViewport(0)->GetRenderPath();
        //rp->RemoveCommands("ClipSequence");
        rp->AddCommand(*entry.urho.renderPathCommand);
    }

}
