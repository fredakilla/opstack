/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   This file is part of
 *       ______        _                             __ __
 *      / ____/____   (_)____ _ ____ ___   ____ _   / // /
 *     / __/  / __ \ / // __ `// __ `__ \ / __ `/  / // /_
 *    / /___ / / / // // /_/ // / / / / // /_/ /  /__  __/
 *   /_____//_/ /_//_/ \__, //_/ /_/ /_/ \__,_/     /_/.   
 *                    /____/                              
 *
 *   Copyright � 2003-2012 Brain Control, all rights reserved.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#define NOMINMAX
#include <locale.h>

#include "../eshared.hpp"

// for registering new urho3d objects
#include "urhoobjects/uWz4Meshcomponent.hpp"
#include "urhoobjects/uAnimatorNodePRS.hpp"
#include "urhoobjects/uCameraController.hpp"

Context* eGraphics::m_gcontext = nullptr;
DebugRenderer* eGraphics::m_gdebuRenderer;
eF32 eGraphics::m_time = 0.0f;

eGraphics::eGraphics() :
    Object(m_gcontext),
    m_hwnd(nullptr),
    m_ownWindow(eFALSE),
    m_fullScreen(eFALSE),
    m_vsync(eTRUE),
    m_wndWidth(800),
    m_wndHeight(600)
{
}

eGraphics::~eGraphics()
{
    shutdown();
}

void eGraphics::createContext()
{
    eASSERT(!m_gcontext);
    m_gcontext = new Context();
    eASSERT(m_gcontext);
}

void eGraphics::freeContext()
{
    eDelete(m_gcontext);
}

void eGraphics::initialize()
{
    setlocale(LC_NUMERIC, "C");
}

void eGraphics::shutdown()
{
    Script* scriptContext = GetSubsystem<Script>();
    scriptContext->ReleaseRef();

    //Engine* engine = GetSubsystem<Engine>();
    m_engine->Exit();
    delete m_engine;
}


void __registerCustomLibrary(Context* context)
{
    uWz4MeshComponent::RegisterObject(context);
    context->RegisterFactory<uAnimatorNodePRS>();
    context->RegisterFactory<uCameraController>();
}

void eGraphics::openWindow(eU32 width, eU32 height, eInt windowFlags, ePtr hwnd)
{
    m_wndWidth = width;
    m_wndHeight = height;
    m_fullScreen = (windowFlags&eWF_FULLSCREEN);
    m_vsync = (windowFlags&eWF_VSYNC);

    if (hwnd)
    {
        m_hwnd = hwnd;
        m_ownWindow = eFALSE;
    }
    else
    {
        m_hwnd = 0;
        m_ownWindow = eTRUE;
    }

    VariantMap engineParameters;
    engineParameters["LogName"]             = "urho3d.log";
    engineParameters["WorkerThreads"]       = false;
    engineParameters["Headless"]            = false;
    engineParameters["VSync"]               = (bool)m_vsync;

    if(hwnd)
    {
        engineParameters["ExternalWindow"]  = m_hwnd;
    }
    else
    {
        engineParameters["FullScreen"]      = m_fullScreen;
        engineParameters["WindowWidth"]     = m_wndWidth;
        engineParameters["WindowHeight"]    = m_wndHeight;
        engineParameters["WindowResizable"] = true;
    }

    m_engine = new Engine(m_gcontext);
    m_engine->Initialize(engineParameters);
    m_engine->SetMaxFps(20000);

    // enable auto reload resources files
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    cache->SetAutoReloadResources(true);

    /*if (m_fullScreen)
        ShowCursor(FALSE);*/

    context_->RegisterSubsystem(new Script(context_));

    // At this point Urho3D objects are registered, we can now register our own derivates objects
    __registerCustomLibrary(context_);

    // create a global debug renderer shared between all op renderer
    m_gdebuRenderer = new DebugRenderer(context_);

    // gl point size for GL_POINTS based geometries rendering
    glPointSize(5);
}

void eGraphics::beginFrame()
{
}

void eGraphics::endFrame()
{
    m_engine->RunFrame();
}

#ifdef eEDITOR
const eRenderStats & eGraphics::getRenderStats() const
{
    return m_renderStats;
}

const eEngineStats & eGraphics::getEngineStats() const
{
    return m_engineStats;
}
#endif

eBool eGraphics::getFullScreen() const
{
    return m_fullScreen;
}

eU32 eGraphics::getWndWidth() const
{
    return m_wndWidth;
}

eU32 eGraphics::getWndHeight() const
{
    return m_wndHeight;
}

eSize eGraphics::getWndSize() const
{
    return eSize(m_wndWidth, m_wndHeight);
}
