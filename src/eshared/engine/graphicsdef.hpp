/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   This file is part of
 *       ______        _                             __ __
 *      / ____/____   (_)____ _ ____ ___   ____ _   / // /
 *     / __/  / __ \ / // __ `// __ `__ \ / __ `/  / // /_
 *    / /___ / / / // // /_/ // / / / / // /_/ /  /__  __/
 *   /_____//_/ /_//_/ \__, //_/ /_/ /_/ \__,_/     /_/.   
 *                    /____/                              
 *
 *   Copyright � 2003-2012 Brain Control, all rights reserved.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef GRAPHICSDEF_HPP
#define GRAPHICSDEF_HPP

enum eWindowFlags
{
    eWF_FULLSCREEN = 0x01,
    eWF_VSYNC      = 0x02,
};

enum eMouseButton
{
    eMB_NONE,
    eMB_LEFT,
    eMB_MIDDLE,
    eMB_RIGHT
};

struct eRenderStats
{
    eU32        batches;
    eU32        instances;
    eU32        triangles;
    eU32        vertices;
    eU32        lines;
};

struct eEngineStats
{
    eU32        numTex2d;
    eU32        numTex3d;
    eU32        numTexCube;
    eU32        numGeos;
    eU32        numGeoBuffs;
    eU32        numShaders;
    eU32        numStates;

    eU32        availGpuMem;
    eU32        usedGpuMem;
    eU32        usedGpuMemTex;
    eU32        usedGpuMemGeo;
};

struct eUserInput
{
    ePoint      mousePos;
    ePoint      mouseDelta;
    eInt        mouseBtns;
};

#endif // GRAPHICSDEF_HPP
