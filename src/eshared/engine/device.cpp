/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   This file is part of
 *       ______        _                             __ __
 *      / ____/____   (_)____ _ ____ ___   ____ _   / // /
 *     / __/  / __ \ / // __ `// __ `__ \ / __ `/  / // /_
 *    / /___ / / / // // /_/ // / / / / // /_/ /  /__  __/
 *   /_____//_/ /_//_/ \__, //_/ /_/ /_/ \__,_/     /_/.   
 *                    /____/                              
 *
 *   Copyright � 2003-2012 Brain Control, all rights reserved.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "../system/system.hpp"
#include "../math/math.hpp"
#include "device.hpp"

eGraphics* eGfx;
eBool eDevice::m_forceNoVSync = eFALSE;

eDevice::eDevice(eInt windowFlags, const eSize &wndSize, ePtr hwnd)
{
    eGfx = new eGraphics;
    eGfx->initialize();
    openWindow(windowFlags, wndSize, hwnd);
}

eDevice::~eDevice()
{
#ifndef eCFG_NO_CLEANUP
    //eDelete(eGfx);
#endif
}

void eDevice::openWindow(eInt windowFlags, const eSize &wndSize, ePtr hwnd)
{
    windowFlags = (m_forceNoVSync ? windowFlags&(~eWF_VSYNC) : windowFlags);
    eGfx->openWindow(wndSize.width, wndSize.height, windowFlags, hwnd);
}

void eDevice::forceNoVSync(eBool forceNoVSync)
{
    m_forceNoVSync = forceNoVSync;
}

