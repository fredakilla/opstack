@echo off


for /r %%i in (*.hlsl) do (
echo Processing %%~ni.hpp
del %%~ni.hpp
REM %BIN% --hlsl --preserve-all-globals -o %%~ni.hpp %%~nxi
)

for /r %%i in (*.hlsli) do (
echo Processing %%~ni.hpp
del %%~ni.hpp
REM %BIN% --hlsl --preserve-all-globals -o %%~ni.hpp %%~nxi
)
