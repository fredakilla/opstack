/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   This file is part of
 *       ______        _                             __ __
 *      / ____/____   (_)____ _ ____ ___   ____ _   / // /
 *     / __/  / __ \ / // __ `// __ `__ \ / __ `/  / // /_
 *    / /___ / / / // // /_/ // / / / / // /_/ /  /__  __/
 *   /_____//_/ /_//_/ \__, //_/ /_/ /_/ \__,_/     /_/.   
 *                    /____/                              
 *
 *   Copyright � 2003-2012 Brain Control, all rights reserved.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef DEVICE_HPP
#define DEVICE_HPP

#include "graphicsdef.hpp"
#include "graphics.hpp"

extern eGraphics * eGfx;

#include "path.hpp"
#include "sequencer.hpp"
#include "demo.hpp"

class eDevice
{
public:
    eDevice(eInt windowFlags, const eSize &wndSize, ePtr hwnd);
    ~eDevice();

    void            openWindow(eInt windowFlags, const eSize &wndSize, ePtr hwnd);
    static void     forceNoVSync(eBool vsync);

private:
    static eBool    m_forceNoVSync;
};

#endif // DEVICE_HPP
