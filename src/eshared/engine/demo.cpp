/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   This file is part of
 *       ______        _                             __ __
 *      / ____/____   (_)____ _ ____ ___   ____ _   / // /
 *     / __/  / __ \ / // __ `// __ `__ \ / __ `/  / // /_
 *    / /___ / / / // // /_/ // / / / / // /_/ /  /__  __/
 *   /_____//_/ /_//_/ \__, //_/ /_/ /_/ \__,_/     /_/.   
 *                    /____/                              
 *
 *   Copyright � 2003-2012 Brain Control, all rights reserved.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "../eshared.hpp"

const eF32 eDemo::MAX_RUNNING_TIME_MINS = 5.0f;

eDemo::eDemo() :
    m_seq(nullptr)
{
}

eDemo::~eDemo()
{
}

eSeqResult eDemo::render(eF32 time) const
{
    eASSERT(time >= 0.0f);

    if (!m_seq)
        return eSEQ_FINISHED;

    //@@_setupTarget(m_seq->getAspectRatio());

    // clear background
    //@@eRenderState &rs = eGfx->freshRenderState();
    //@@rs.targets[0] = eGraphics::TARGET_SCREEN;
    //@@eGfx->clear(eCM_ALL, eCOL_BLACK);

    // render frame on target
    //@@eGfx->pushRenderState();
    const eSeqResult res = m_seq->run(time);
    //@@eGfx->popRenderState();

    // copy to screen
    //@@rs.targets[0] = eGraphics::TARGET_SCREEN;
    //@@rs.depthTarget = nullptr;
    //@@eRenderer->renderQuad(m_renderRect, eGfx->getWndSize(), m_target);
    return res;
}

void eDemo::setSequencer(const eSequencer *seq)
{
    m_seq = seq;
}

const eSequencer * eDemo::getSequencer() const
{
    return m_seq;
}


