/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   This file is part of
 *       ______        _                             __ __
 *      / ____/____   (_)____ _ ____ ___   ____ _   / // /
 *     / __/  / __ \ / // __ `// __ `__ \ / __ `/  / // /_
 *    / /___ / / / // // /_/ // / / / / // /_/ /  /__  __/
 *   /_____//_/ /_//_/ \__, //_/ /_/ /_/ \__,_/     /_/.   
 *                    /____/                              
 *
 *   Copyright � 2003-2012 Brain Control, all rights reserved.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef ESHARED_HPP
#define ESHARED_HPP

#include "system/system.hpp"
#include "math/math.hpp"
#include "engine/device.hpp"
#include "opstacking/opstacking.hpp"
#include "packing/packing.hpp"
#include "wz4Lib.hpp"

// include this one last to avoid errors about the new operator redefinition
#include "system/memorytrack.hpp"

#endif // ESHARED_HPP
