#!/bin/bash

BUILD=$1
NUMPROC=$(grep -c ^processor /proc/cpuinfo)

# check arguments

display_usage() 
{
	echo -e "\nUsage:\n$0 [Debug|Release] \n" 
}

if [ $# -ne 1 ]; then
	display_usage
	exit 1
else
	if [ "$1" == "Debug" ] || [ "$1" == "Release" ]; then
		BUILD=$1
	else
		display_usage	
		exit 1
	fi
fi


##########################################################################
# Build Urho3D library
##########################################################################

URHO3DLIB=build/Urho3D/lib/libUrho3D.a

#if [ ! -f $URHO3DLIB ]; then

	# cmake urho3d
	mkdir -p build/Urho3D
	cd build/Urho3D
	cmake -DCMAKE_BUILD_TYPE=$BUILD -DURHO3D_SAMPLES=OFF ../../3rdparty/Urho3D
	
	# make urho3d
	make -j$NUMPROC
	
	cd -
#fi

# copy urho3d data to binary folder
mkdir -p binary
cp -R 3rdparty/Urho3D/bin/Data binary/Data
cp -R 3rdparty/Urho3D/bin/CoreData binary/CoreData


# Build opstack

#rm -f binary/estudio4 binary/libwz4lib.a
#mkdir -p build
#cd build > /dev/null
#$QMAKE ../src/opstack.pro -r -spec linux-g++ CONFIG+=$BUILD
#make -j$NUMPROC
#cd - > /dev/null

